public function getRetentionFactor(App_Food $ingredient, $componentId, $returnFallbackWhenNeeded=true) {
		$db = $this->_getDbAdapter();

		$foodCode = $ingredient->getCode();
		$methods = $this->getPreparationMethods();
		$groupId = $ingredient->getFoodGroupId();
		$info = $this->_ingredientInfo[$ingredient->getCode()];

		$specifications = array();

		$parts = array();
		
		if ($this->isDrained()) {
			$parts[] = 'E';
			$parts[] = 'A';
		} else {
			$parts[] = 'T';
			$parts[] = 'A';
		}

		if ($info['WHOLE_WHEAT']) {
			$specifications['W'] = 'G';
		}
		if ($info['SOAK']) {
			$specifications['S'] = 'S';
		}
		
		if (sizeof(array_intersect($this->getIngredients()->getKeys(), self::getBreadCrumbFoodCodes()))>0) {
			$specifications['B'] = 'B';
		}

		// current factor part accuracy
		$retFactor = null;
		
		// first try to find specific retention factor
		foreach ($methods as $method) {
			$factors = $this->_getSpecificRetentionFactors($method, $groupId, $componentId);

			foreach ($parts as $key=>$part) {
				if (isset($factors[$part])) {
					foreach ($factors[$part] as $factor) {
						if ($factor['spec']==$specifications) {
							$retFactor = $factor['factor'];
							break 3; // factor found
						}
					}
				}
			}
		}

		// ... if none found, resort to more generic factors
		if (null === $retFactor && sizeof($methods)>0) {
			$family = null;

			/*
			 *	IMPORTANT
			 *
			 *	Conditions here should be synchronized with those in recaching
			 *	retention factors
			 * 
			 *	@see Codelist_Model_Item::recacheRetFact
			 */

			// Family II
			if ($groupId>=43 && $groupId<=66 &&
				sizeof(array_intersect($methods, array('M016', 'M005', 'M015', 'M007')))==0) {

				$family = 'II';

			// Family III
			} else if ($groupId>=8 && $groupId<=18 &&
				sizeof(array_intersect($methods, array('M021', 'M002', 'M112', 'M212', 'M003', 'M013', 'M103', 'M203')))==0) {

				$family = 'III';

			// Family IV
			} else if ($groupId>=30 && $groupId<=41) {

				$family = 'IV';

			// Family V
			} else if ($groupId>=43 && $groupId<=66 &&
				sizeof(array_intersect($methods, array('M016', 'M005', 'M015', 'M007')))>0) {

				$family = 'V';

			// Family VI
			} else if ($groupId>=8 && $groupId<=18 &&
				sizeof(array_intersect($methods, array('M021', 'M002', 'M112', 'M212', 'M003', 'M013', 'M103', 'M203')))>0) {

				$family = 'VI';	
			}

			if (null!==$family) {
				$retFactor = $this->_getGenericRetentionFactors($family, $componentId);
			}
		}

		// last resort - if there were any method is thermical
		// then factor it is fixed unless it can be applied from copyRetentionFactors
		if (null===$retFactor && $returnFallbackWhenNeeded) {
			foreach ($this->_getCopyRetentionFactorForComponent($componentId) as $srcCode) {
				$retFactor = $this->getRetentionFactor($ingredient, $srcCode, false);
				if (null!==$retFactor) {
					break;
				}
			}

			// still undefined
			if (null===$retFactor) {
				if (in_array('M000', $methods)) {
					$retFactor = 0.8;
				} else {
					$retFactor = 1;
				}
			}
		}

		return $retFactor;
		


	}
	
    protected function _getGenericRetentionFactors($family, $componentId) {
		$cache = self::getDefaultCacheProvider();
		$cacheKey = self::CACHE_PREFIX_GENERIC_RETENTION_FACTOR.$family;

		if ($cache===null || false===($factors = $cache->load($cacheKey))) {
			$db = $this->_getDbAdapter();

			$select = $db->select()
				->from('fir_retentionfactors', array('COMPID', 'RETFACT'))
				->where("FOODID=?", '')
				->where("PREPMET=?", $family)
			;
			
			$factors = $db->fetchPairs($select);

			if ($cache!==null) {
				$cache->save($factors, $cacheKey);
			}
		}

		if (isset($factors[$componentId])) {
			return $factors[$componentId];
		} else {
			return null;
		}
	}

    protected function _getSpecificRetentionFactors($prepMethod, $groupId, $componentId) {
		$cache = self::getDefaultCacheProvider();

		$cacheKey = self::CACHE_PREFIX_SPECIFIC_RETENTION_FACTOR."{$prepMethod}_{$groupId}_".self::inflectCacheName($componentId);

		if ($cache===null || false===($factors = $cache->load($cacheKey))) {
			$db = $this->_getDbAdapter();

			$select = $db->select()
				->from('fir_retentionfactors', array('TMIN', 'TMAX', 'BREADED', 'PART', 'WHOLE', 'SOAK', 'RETFACT'))
				->where("FOODID=?", $groupId)
				->where("COMPID=?", $componentId)
				->where("PREPMET=?", $prepMethod)
			;

			$factorsStruct = array();
			
			foreach ($db->fetchAll($select) as $row) {
				$part = !empty($row['PART'])?$row['PART']:'A';

				if (!isset($factorsStruct[$part])) {
					$factorsStruct[$part] = array();
				}

				$attributes = array('WHOLE', 'SOAK', 'BREADED');
				$specification = array();
				foreach ($attributes as $attrib) {
					if (!empty($row[$attrib])) {
						$specification[substr($attrib, 0, 1)] = $row[$attrib];
					}
				}

				$key = implode('_', $specification);
				$temperature = max($row['TMIN'], $row['TMAX']);

				if (isset($factorsStruct[$part][$key]) && $factorsStruct[$part][$key]['temp']>$temperature) {
					continue;
				}

				$factorsStruct[$part][$key] = array(
					'temp'=>$temperature,
					'spec'=>$specification,
					'factor'=>$row['RETFACT'],
				);
			}


			$factors = array();
			foreach ($factorsStruct  as $part=>$items) {
				$factors[$part] = array_values($items);
			}

			if ($cache!==null) {
				$cache->save($factors, $cacheKey);
			}
		}
		return $factors;
	}