/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ijs.opkp.helper;

import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.components.InfiniteProgress;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.Externalizable;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.io.Util;
import com.codename1.processing.Result;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;

import com.ijs.opkp.model.SettingsModel;
import com.ijs.opkp.model.UserModel;
import com.ijs.opkp.model.FoodModel;
import com.ijs.opkp.model.FoodPlateModel;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;



/**
 *
 * net@author simon
 */
public class API {

    public static final String API_URL = "http://opkp.si/sl_SI/api/";
    public static final String API_RES = "http://opkp.si/sl_SI/";
    /*public static final String API_URL = "http://localhost/opkp_stable/public/sl_SI/api/";
	public static final String API_RES = "http://localhost/opkp_stable/public/";*/

    public static final int REQUEST_TIMEOUT = 15000;
    private ArrayList<Request> pendingRequests = new ArrayList<Request>();

    public static final String API_KEY = "dd19272fbaa4135abaaabc3efa128bcb74fe942a";
    public static final String API_LOGIN = "authenticate/access-token";
    public static final String API_REGISTER = "authenticate/register";
    public static final String API_PROFILE = "user/profile";
    public static final String API_GUIDELINES = "user/trustee-members";
    public static final String API_FOOD_SEARCH = "food/search";
    public static final String API_FOOD_ADD = "food/scanned-food";
    public static final String API_FOOD_DOWNLOAD = "food/download";
    public static final String API_DIARY = "diary";
    public static final String API_DIARY_MEAL = "diary/add";
    public static final String API_OFFLINE_SYNC ="offlineRequests";

    protected static API instance = null;
    public Dialog _loadingDlg = null;
	public int _loading = 0;
    protected API() {
    }

    public static API getInstance() {
        if (instance == null) {
            instance = new API();
            NetworkManager.getInstance().setTimeout(REQUEST_TIMEOUT);
            NetworkManager.getInstance().addErrorListener(new ActionListener() {
                public void actionPerformed(ActionEvent aevt) {
                    final NetworkEvent evt = (NetworkEvent) aevt;

                    instance._hideLoading();
                    Log.p("API:" + evt.getError().getMessage());
                    evt.consume();
                    Display.getInstance().callSerially(new Runnable() {
                        public void run() {
                            Dialog.show("Napaka", "Prišlo je do napake pri povezavi na strežnik. Preverite internetno povezavo.", Dialog.TYPE_ERROR, null, "OK", null);
                            API.Callback cb = null;
                            if (evt.getSource() instanceof API.Request) {
                                cb = ((API.Request) evt.getSource()).callback;
                            } else if (evt.getSource() instanceof API.FileRequest) {
                                cb = ((API.FileRequest) evt.getSource()).callback;
                            }

                            if (cb != null) {
                                cb.networkError();
                                cb.complete(null);
                            }
                        }
                    });
                }
            });
        }
        return instance;
    }

	protected void _showLoading() {
		Display.getInstance().callSerially(new Runnable() {
			public void run() {
				_loading++;
				if (_loadingDlg==null) {
					_loadingDlg = (new InfiniteProgress()).showInifiniteBlocking();
				}
			}
		});
	}
	
	protected void _hideLoading() {
		Display.getInstance().callSerially(new Runnable() {
			public void run() {
				_loading--;
				if (_loading==0 && _loadingDlg!=null) {
					_loadingDlg.dispose();
					_loadingDlg = null;
				}
			}
		});
	}
    public void getGuideLines(API.Callback cb){
        API.GuideLinesResponse rsp = new API.GuideLinesResponse();
        API.Request request = createRequest(cb, rsp);
        request.setUrl(API_URL + API_GUIDELINES);
        request.setPost(false);
        request.addArgument("guidelines", "1");
        NetworkManager.getInstance().addToQueue(request);
    }
    
    public void login(String username, String password, API.Callback cb) {
        API.LoginResponse rsp = new API.LoginResponse();
        API.Request request = createRequest(cb, rsp);

        request.setUrl(API_URL + API_LOGIN);
        request.setPost(true);
        request.addArgument("api_key", API_KEY);
        request.addArgument("username", username);
        request.addArgument("password", password);
        request.addArgument("active-check", "0");

        NetworkManager.getInstance().addToQueue(request);
    }

    public void profile(API.Callback cb) {
        API.MapResponse rsp = new API.MapResponse();
        API.Request request = createRequest(cb, rsp);

        request.setUrl(API_URL + API_PROFILE);
        request.setPost(false);

        NetworkManager.getInstance().addToQueue(request);
    }

    public void register(String username, String email, String password, Callback cb) {
        API.RegisterResponse rsp = new API.RegisterResponse();
        API.Request request = createRequest(cb, rsp);

        request.setUrl(API_URL + API_REGISTER);
        request.setPost(true);
        request.addArgument("api_key", API_KEY);
        request.addArgument("username", username);
        request.addArgument("email", email);
        request.addArgument("password", password);

        NetworkManager.getInstance().addToQueue(request);
    }

    public void addDiaryItem(HashMap ht, Callback cb) {
        System.out.println(ht.containsKey("foodItems"));
       
        if (!ht.containsKey("foodItems")) {
            return;
        }
        
        System.out.println("AddingMeals");

        API.Response rsp = new API.Response();
        API.Request request = createRequest(cb, rsp);

        request.setPost(true);
      
            request.addArgument("entries[0][time]", (String) ht.get("time"));
            request.addArgument("entries[0][code]", FoodPlateModel.getMealType((String) ht.get("time")));

            HashMap<String,HashMap> htFoodItems = (HashMap) ht.get("foodItems");
            
            int countItems = 0;
           for (Entry<String, HashMap> item : htFoodItems.entrySet()) {
            String key = item.getKey();
            HashMap htItem = item.getValue();

                System.out.println("class: FOOD, code: "+key+" weight: "+ String.valueOf((Double) htItem.get("weight"))+" unit: 0");
                request.addArgument("entries[0][items][" + countItems + "][class]", "FOOD");
                request.addArgument("entries[0][items][" + countItems + "][code]", key);
                request.addArgument("entries[0][items][" + countItems + "][weight]", String.valueOf((Double) htItem.get("weight")));
                request.addArgument("entries[0][items][" + countItems + "][unit]", "0"); //grams
                countItems++;
            }
            /*request.addArgument("measurements[GLUCOSE_LEVEL][0]", "1");*/
            request.addArgument("measurements[INSULIN_SHOT][0]", "0.3");
            request.setUrl(API_URL + API_DIARY + "?date=" + (String) ht.get("date"));

        /*    if (!isConnectionAvaible()) {
                Dialog.show("eDietetik2", "Obrok se bo shranil v interni dnevnik, ker povezava ni na voljo. Sinhronizacija internega dnevnika poteka samodejno.", null, "Nazaj");
                pendingRequests.add(request);

                Store.getInstance().setPendingRequests();

            } else {*/

                NetworkManager.getInstance().addToQueue(request);
            //}
        }
    public void addMeals(HashMap ht, Callback cb) {
        if (!ht.containsKey("foodItems")) {
            return;
        }

        API.Response rsp = new API.Response();
        API.Request request = createRequest(cb, rsp);

        request.setUrl(API_URL + API_DIARY_MEAL);
        request.setPost(true);
        request.addArgument("date", (String) ht.get("date")); // change for all diary
        request.addArgument("time", (String) ht.get("time")); // change for all diary
        request.addArgument("code", FoodPlateModel.getMealType((String) ht.get("time"))); // change for all diary
        HashMap<String,HashMap> htFoodItems = (HashMap) ht.get("foodItems");

        int count = 0;
        for (Entry<String, HashMap> item : htFoodItems.entrySet()) {
            String key = item.getKey();
            HashMap htItem = item.getValue();

            request.addArgument("to_insert[" + count + "][class]", "FOOD");
            request.addArgument("to_insert[" + count + "][weight]", String.valueOf((Double) htItem.get("weight")));
            request.addArgument("to_insert[" + count + "][code]", key);
            request.addArgument("to_insert[" + count + "][unit]", "0"); //grams
            count++;
        }

        NetworkManager.getInstance().addToQueue(request);
    }

    public void addFood(HashMap food, Callback cb) {

       API.ResultResponse rsp = new API.ResultResponse();
               byte[] photoBytes  = (byte[])food.get("photo");
		ConnectionRequest request;
		if (photoBytes!=null) {
			request = createFileRequest(cb, rsp);
		} else {
			request = createRequest(cb, rsp);
		}
		
		request.setUrl(API_URL+API_FOOD_ADD);
		request.setPost(true);
		request.addArgument("code", (String) food.get("code"));
		request.addArgument("name", (String) food.get("name"));
		request.addArgument("producer", (String) food.get("producer"));
		request.addArgument("country", (String) food.get("country"));

                request.addArgument("price", (String)food.get("price"));
                request.addArgument("weight", (String)food.get("weight"));
                ((API.FileRequest)request).addData("photo", photoBytes, "application/octet-stream");

     /*   if (ht.get("gs1") != null)
            request.addArgument("gs1", (String) ht.get("gs1"));
        if (ht.get("producer") != null)
            request.addArgument("producer", (String) ht.get("producer"));
        if (ht.get("country") != null)
            request.addArgument("country", (String) ht.get("country"));
        */
         HashMap components = (HashMap) food.get("components");
        if (components != null && !components.isEmpty()) {
            Iterator it = components.entrySet().iterator();
            int countItems = 0;
            while (it.hasNext()) {
                Map.Entry htItem = (Map.Entry) it.next();
                request.addArgument("components[" + countItems + "][code]", (String) htItem.getKey());
                request.addArgument("components[" + countItems + "][weight]", String.valueOf((Double) htItem.getValue()));
                //request.addArgument("components[" + countItems + "][unit]", (String) htItem.get("unit"));
                //TODO: delete this line! Check default units for conversion in "add new food stateMachine conversion"!
                //request.addArgument("components[" + countItems + "][unit]", "g");
                countItems++;
            }
        }
        Dialog.show("eDietetik2", request.toString(), null,"Nazaj");
        NetworkManager.getInstance().addToQueue(request);
    }
    public static class AddFoodsResponse extends Response {


        public AddFoodsResponse() {
        }

        @Override
        public void fill(JSONObject table) {
            super.fill(table);
         
        }
    }

    public void downloadFood(Callback cb) {
        System.out.println("### Download Food");
        API.GetFoodsResponse rsp = new API.GetFoodsResponse();
        API.Request request = createRequest(cb, rsp);
      //  _showLoading();

        request.setUrl(API_URL + API_FOOD_DOWNLOAD);
        request.setPost(false);
      //request.addArgument("components", ""); //ENERA
        request.addArgument("fields",  "CODE,NAME,IS_RECIPE,GROUP_ID,MAIN_GROUP_ID,UNITS,HOUSEHOLD_MEASURES,COMPONENTS"); //BARCODE
        request.addArgument("all_components", "1");
        request.addArgument("allowed_recipe_authors", "LIBRA,EFSA");

        NetworkManager.getInstance().addToQueue(request);
    }
    
    
       public void getFoodByBarcode(String barcode,Callback cb) {
        System.out.println("### Download Food");
        API.GetFoodByBarcodeResponse rsp = new API.GetFoodByBarcodeResponse();
        API.Request request = createRequest(cb, rsp);

        request.setUrl(API_URL + API_FOOD_SEARCH);
        request.setPost(false);
        request.addArgument("code", barcode);
    //    request.addArgument("components", "ENERA, CHOT, PROT,FAT, PHE, GLUTN,PHE,NA,PROBIO,FASAT,SUGAR,FIBT,ASPM,CO2E,LA-5,BB-12,DN-173-010,DN-114-001,LGG,L.GASSERI,B.INFANTIS,E.FAECIUM"); //ENERA
        request.addArgument("fields", "CODE,NAME,IS_RECIPE,GROUP_ID, MAIN_GROUP_ID,UNITS,HOUSEHOLD_MEASURES,COMPONENTS"); //BARCODE
        request.addArgument("all_components", "1");
        request.addArgument("allowed_recipe_authors", "LIBRA,EFSA");

        NetworkManager.getInstance().addToQueue(request);
    }

    public static abstract class Callback {

        public void success(API.Response response) {
        }

        public void complete(Result result) {
        }

        public void networkError() {
        }

        public void error(String reason, Result result) {
        }

        public void error(String reason, JSONObject result) {
        }

    }

    public static class Response implements Externalizable{

        void fill(Result res) {
        }

        void fill(JSONObject res) {
        }

        void fill(InputStream is) {
        }

        @Override
        public int getVersion() {
           return 1;
        }

        @Override
        public void externalize(DataOutputStream out) throws IOException {
            
        }

        @Override
        public void internalize(int version, DataInputStream in) throws IOException {
        }

        @Override
        public String getObjectId() {
            return "Response";
        }
    }

    public static class ResultResponse extends Response {

        public Result res;

        @Override
        void fill(Result res) {
            this.res = res;
        }
    }

    public static class LoginResponse extends Response {

        public String token;

        void fill(Result res) {
            token = res.getAsString("/access_token");
        }
    }

    public static class MapResponse extends Response {

        public JSONObject map;

        @Override
        void fill(JSONObject map) {
            this.map = map;
        }

    }

    public static class RegisterResponse extends Response {

        public String accessToken;

        @Override
        void fill(Result res) {
            super.fill(res);
            accessToken = res.getAsString("/access_token");
        }
    }

    public static class UpdateProfileResponse extends Response {

        public String accessToken;

        @Override
        void fill(Result res) {
            super.fill(res);
            accessToken = res.getAsString("/access_token");
        }
    }

    public static class GetFoodsResponse extends Response {

        @Override
        public void fill(JSONObject table) {

            super.fill(table);
            FoodDB.getInstance().setFoods(table);

        }
    }
    public static class GuideLinesResponse extends Response{

        @Override
        void fill(JSONObject res) {
            try {
                super.fill(res);
                System.out.println("Guidelines:\n "+res.toString(2));
            } catch (JSONException ex) {
            }
        }
        
    }
     public static class GetFoodByBarcodeResponse extends Response {
         public static JSONObject data;
        @Override
        public void fill(JSONObject table) {

            System.out.println("## FoodResponse");
            super.fill(table);
            this.data = table;
           // FoodDB.getInstance().createFood(table);
        }
    }

    /*public static class FoodModelSearchResponse extends Response {
		public Vector<FoodModelModel> foods;
		public boolean userAdded;
		
		@Override
		void fill(JSONObject res) {
			Vector items;
                    try {
                        items = (Vector)res.get("items");
                    
			foods = new Vector<FoodModel>(items.size());
			
			for (Object obj : items) {
				foods.add(FoodModel.fromResult(Result.fromContent((Hashtable)obj)));
			}
			
                    userAdded = items.size()==0 && (Boolean.TRUE.equals(res.get("user_added")) || "true".equals(res.get("user_added")));
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
		}
	}*/
    protected class Request extends ConnectionRequest implements Externalizable{

        protected API.Callback callback;
        protected API.Response response;
        protected String contentType;
        private API.Request r;

        public Request(API.Callback callback, API.Response response) {
            super();
            this.callback = callback;
            this.response = response;
            this.setTimeout(REQUEST_TIMEOUT);
        }

        @Override
        protected void readHeaders(Object connection) throws IOException {
            super.readHeaders(connection);

            contentType = getHeader(connection, "Content-type");
        }

        @Override
        protected void readResponse(InputStream input) throws IOException {
            _handleResponse(contentType, response, input, callback);
        }

        public Class getReponseClass() {
            return response.getClass();
        }

        public Request getR() {
            return r;
        }

        @Override
        public int getVersion() {
            return 1;
        }

        @Override
        public void externalize(DataOutputStream out) throws IOException {
       //   Util.writeObject(callback, out);
          Util.writeObject(response, out);
          Util.writeUTF(super.getRequestBody(),out);
        }

        @Override
        public void internalize(int version, DataInputStream in) throws IOException {
            API.Callback cb = (API.Callback) Util.readObject(in);
            API.Response rsp = (API.Response) Util.readObject(in);
            String requestBody = (String) Util.readUTF(in);
            
            API.Request r = createRequest(cb,rsp);
            r.setRequestBody(requestBody);
            
          
        }

        @Override
        public String getObjectId() {
            return "ConnectionRequest";
        }
    }
    	protected class FileRequest extends MultipartRequest {
		protected Callback callback;
		protected Response response;
		protected String contentType;
		
		public FileRequest(Callback callback, Response response) {
			super();
			this.callback = callback;
			this.response = response;
			this.setTimeout(REQUEST_TIMEOUT);
		}

		@Override
		protected void readHeaders(Object connection) throws IOException {
			super.readHeaders(connection);
			
			contentType = getHeader(connection, "Content-type");
		}
		
		@Override
		protected void readResponse(InputStream input) throws IOException {
			_handleResponse(contentType, response, input, callback);
		}

		public Class getReponseClass() {
			return response.getClass();
		}
	}
    protected void _handleResponse(String contentType, API.Response response, InputStream input, API.Callback callback) {
        _hideLoading();
        try {
            if (contentType.equals("application/json")) {
                JSONParser parser = new JSONParser();
                Map<String, Object> table = parser.parseJSON(new InputStreamReader(input));
                JSONObject jsonObj = new JSONObject((HashMap) table);

                Result result = Result.fromContent(table);
                boolean success = result.getAsBoolean("/success");
                if (success) {
                    response.fill(result);
                    response.fill(jsonObj);
                    callback.success(response);
                } else {
                    Log.p("API:" + result.getAsString("/reason"));
                    callback.error(result.getAsString("/reason"), result);
                    callback.error(result.getAsString("/reason"), jsonObj);
                }
                callback.complete(result);
            } else {
                response.fill(input);
                callback.success(response);
                callback.complete(null);
            }
        } catch (Exception e) {
            Log.p("API:" + e);
            callback.complete(null);
        }
    }

    protected API.Request createRequest(Callback cb, API.Response rsp) {
        API.Request request = new API.Request(cb, rsp);
        String token = Store.getInstance().getAccessToken();
        if (token != null) {
            request.addRequestHeader("Authorization", "Client-ID " + token);
        }
       
        return request;
    }
    protected API.FileRequest createFileRequest(Callback cb, API.Response rsp) {
		API.FileRequest request = new API.FileRequest(cb, rsp);
		String token = Store.getInstance().getAccessToken();
		if (token!=null) {
			request.addRequestHeader("Authorization", "Client-ID "+token);
		}
		
		return request;
	}

    public static String dateToDbString(Date inputDate) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        //formatter.setLenient(false);
        return formatter.format(inputDate);
    }

    public static Date validateDate(String inputDate) {
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            //formatter.setLenient(false);
            date = formatter.parse(inputDate);
        } catch (ParseException e) {
        }
        return date;
    }
    public static boolean isConnectionAvaible() {
       
    boolean online = false;
    String net = NetworkManager.getInstance().getCurrentAccessPoint();
    System.out.println("AccessPoint: "+net);
    if (net == null || net == "" || net.equals(null)) {
        online = false;
    } else {
        online = true;
    }
        return online;
}
    public void finishPendingRequests(){
        if (pendingRequests.size() == 0)
            return;
        for (API.Request each : pendingRequests) {
            NetworkManager.getInstance().addToQueueAndWait(each.getR());
        }
        pendingRequests.clear();
        
    }

    public ArrayList<Request> getPendingRequests() {
        return pendingRequests;
    }

}
