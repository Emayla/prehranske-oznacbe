from izdelki.auth_forms import BasicLoginForm
from izdelki.utilities import build_url


def login_form_context_processor(request):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})

    return {'box_login_form': login_form}