from izdelki.models import BasicUser
from izdelki.setup import module_dir

from django.core.mail import send_mail
from django.templatetags.static import static

import logging
import os
import smtplib

EMAIL_CHANGED_SUBJECT    = "Sprememba osebnih podatkov na prehranskeoznacbe.si"
PASSWORD_CHANGED_SUBJECT = "Sprememba gesla na prehranskeoznacbe.si"




def handle_email_change(old_email, new_email):
    """
    Check if the user has changed his email
    If he did, send an email message to the old address notifying the user of this change 
    """
    print("HANDLING EMAIL CHANGE")
    logger = logging.getLogger(__name__)
    if new_email != old_email:
        #Log the info, in case we ever need to revert this change 
        #(because the account was stolen)
        logger.info("Email for user changed from {} to {}".format(old_email,new_email))
        
        #Now send a message to the old email address
        #In case someone else changed stole the account and changed the email address,
        #this should tell the real owner what happened
        email_body_path = os.path.join(module_dir, 'static', 'izdelki', 'txt', 'edit_user_email', "email_changed_body.txt")
        with open(email_body_path, 'r', encoding='utf8') as email_body_file:
            email_body = email_body_file.read()
            try:
                send_mail(
                    EMAIL_CHANGED_SUBJECT,
                    email_body,
                    'Prehranske Označbe <info@prehranskeoznacbe.si>',
                    [old_email],
                    fail_silently=False,
                )
            except smtplib.SMTPException:
                logger.info("Couldn't send email")
