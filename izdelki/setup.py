import os
from collections import OrderedDict

#  Defines the path to the json file with products.
module_dir = os.path.dirname(__file__)  # current directory
food_path = os.path.join(module_dir, 'static', 'izdelki', 'json', 'food.json')  # path to JSON file
components_path = os.path.join(module_dir, 'static', 'izdelki', 'json', 'food_components.json')  # path to JSON file

#  Defines the necessary and optional components when dealing with nutrition facts labels.
food_label = {
    'necessary': ['ENVAL', 'FAT', 'FASAT', 'CHOT', 'SUGAR', 'PROT', 'SALT'],
    'layout': OrderedDict([
            ('ENVAL', ()),
            ('FAT', ('FASAT', 'FAMS', 'FAPU')),
            ('CHOT', ('SUGAR', 'POLY', 'STARCH')),
            ('FIBT', ()),
            ('PROT', ()),
            ('SALT', ()),
            ('VIT', ('VITA', 'VITD', 'VITE', 'VITK', 'VITC', 'THIA', 'NIA', 'VITB6', 'FOLACID', 'VITB12', 'BIOT',
                     'PANTAC', 'K', 'CHD', 'CA', 'P', 'MG', 'HAEM', 'ZN', 'CU', 'MN', 'FD', 'SE', 'CR', 'MO', 'ID')),
    ])
}

#  The conversion table for different nutrients to energy values.
#  Example: carbohydrates have energy density of 17 kJ/g and 4 kcal/g.
enval_calculation = {
    'necessary': ['CHOT', 'PROT', 'FAT', 'ALC'],
    "CHOT": [17, 4],    # carbohydrates
    "PROT": [17, 4],    # proteins
    "FAT": [37, 9],     # fats
    "ALC": [29, 7],     # ethanol
    "FIBT": [8, 2],     # dietary fibers
}


daily_intake_percent_calculation = {
    # Everything but vitamins and minerals; all values are in gram unless noted otherwise.
    "ENVAL": [8400, 2000],  # energy value; first value is in kJ, the second in kcal
    "FAT": 70,              # fats
    "FASAT": 20,            # saturated fats
    "CHOT": 260,            # carbohydrates
    "SUGAR": 90,            # sugars
    "PROT": 50,             # proteins
    "SALT": 6,              # salt

    # Vitamins and minerals. The first value is the significand (mantissa), the
    # second is the exponent (the base is always 10). All values are in grams.
    "VITA": [800, -6],      # vitamin A
    "VITD": [5, -6],        # vitamin D
    "VITE": [12, -3],       # vitamin E
    "VITK": [75, -6],       # vitamin K
    "VITC": [80, -3],       # vitamin C
    "THIA": [1.1, -3],      # thiamine, vitamin B1
    "NIA": [16, -3],        # niacin, vitamin B3
    "VITB6": [1.4, -3],     # vitamin B6
    "FOLACID": [200, -6],   # folic acid, vitamin B9
    "VITB12": [2.5, -6],    # vitamin B12, cobalamin
    "BIOT": [50, -6],       # biotin
    "PANTAC": [6, -3],      # pantothenic acid, B5
    "K": [2000, -3],        # potassium
    "CHD": [800, -3],       # chloride
    "CA": [800, -3],        # calcium
    "P": [700, -3],         # phosphorus
    "MG": [375, -3],        # magnesium
    "HAEM": [14, -3],       # iron
    "ZN": [10, -3],         # zinc
    "CU": [1, -3],          # copper
    "MN": [2, -3],          # manganese
    "FD": [3.5, -3],        # fluoride
    "SE": [55, -6],         # selenium
    "CR": [40, -6],         # chromium
    "MO": [50, -6],         # molybdenum
    "ID": [150, -6]         # iodine
}

#  Defines name, default unit and possible units for each component.
component_detail = {
    'ENVAL': {'name': 'Energijska vrednost', 'def_unit': 'kcal', 'units': ['kcal', 'kJ']},
    'FAT': {'name': 'Maščobe', 'def_unit': 'g', 'units': ['g', 'mg']},
    'FASAT': {'name': 'Nasičene maščobe', 'def_unit': 'g', 'units': ['g', 'mg']},
    'FAMS': {'name': 'Enkrat nenasičene maščobne kisline', 'def_unit': 'g', 'units': ['g', 'mg']},
    'FAPU': {'name': 'Večkrat nenasičene maščobne kisline', 'def_unit': 'g', 'units': ['g', 'mg']},
    'CHOT':  {'name': 'Ogljikovi hidrati', 'def_unit': 'g', 'units': ['g', 'mg']},
    'SUGAR': {'name': 'Sladkorji', 'def_unit': 'g', 'units': ['g', 'mg']},
    'POLY': {'name': 'Polioli', 'def_unit': 'g', 'units': ['g', 'mg']},
    'STARCH': {'name': 'Škrob', 'def_unit': 'g', 'units': ['g', 'mg']},
    'FIBT': {'name': 'Prehranske vlaknine', 'def_unit': 'g', 'units': ['g', 'mg']},
    'PROT': {'name': 'Beljakovine', 'def_unit': 'g', 'units': ['g', 'mg']},
    'SALT': {'name': 'Sol', 'def_unit': 'g', 'units': ['g', 'mg']},
    'VITA': {'name': 'Vitamin A', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
    'VITD': {'name': 'Vitamin D', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
    'VITE': {'name': 'Vitamin E', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'VITK': {'name': 'Vitamin K', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
    'VITC': {'name': 'Vitamin C', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'THIA': {'name': 'Tiamin', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'NIA': {'name': 'Niacin', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'VITB6': {'name': 'Vitamin B6', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'FOLACID': {'name': 'Folna kislina', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
    'VITB12': {'name': 'Vitamin B12', 'def_unit': 'mcg', 'units': ['g', 'mg', 'mcg']},
    'BIOT': {'name': 'Biotin', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
    'PANTAC': {'name': 'Pantotenska kislina', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'K': {'name': 'Kalij', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'CHD': {'name': 'Klorid', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'CA': {'name': 'Kalcij', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'P': {'name': 'Fosfor', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'MG': {'name': 'Magnezij', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'HAEM': {'name': 'Železo', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'ZN': {'name': 'Cink', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'CU': {'name': 'Baker', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'MN': {'name': 'Mangan', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'FD': {'name': 'Fluorid', 'def_unit': 'mg', 'units': ['g', 'mg', 'mcg']},
    'SE': {'name': 'Selen', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
    'CR': {'name': 'Krom', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
    'MO': {'name': 'Molibden', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
    'ID': {'name': 'Jod', 'def_unit': 'mcg', 'units': ['mg', 'mcg']},
}

component_database_blacklist = ['ENVAL', 'SALT']

database_floats = ['serv_size', 'meal_size', 'neto_weight']
database_ints = ['group', 'diets']

allergens = [
    {'id': 1, 'name': 'žito, ki vsebuje gluten'},
    {'id': 18, 'name': 'raki, ribe in mehkužci'},
    {'id': 2, 'name': 'jajca'},
    {'id': 5, 'name': 'arašidi'},
    {'id': 4, 'name': 'mleko ali mlečni izdelki, ki vsebujejo laktozo'},
    {'id': 12, 'name': 'soja'},
    {'id': 11, 'name': 'oreščki'},
    {'id': 13, 'name': 'listnata zelena'},
    {'id': 14, 'name': 'gorčica, gorčično seme'},
    {'id': 15, 'name': 'sezamovo seme'},
    {'id': 16, 'name': 'žveplov dioksid in sulfiti'},
    {'id': 17, 'name': 'volčji bob'}
]


status_strings = {
    #Default value. The product has been saved to the database but hasn't been manually confirmed yet. It should not be shown publically.
    'entered': "<span class = 'dashboard-status dashboard-status-entered'>Izdelek čaka na potrditev</span>",           
    
    #The product was manually checked, and an error has been found. It should not be shown publically.
    'unconfirmed': "<span class = 'dashboard-status dashboard-status-unconfirmed'>V izdelku se nahajajo napake</span>",       
    
    #The product was checked manually and there were no problems. It can be shown publically.
    'confirmed': "<span class = 'dashboard-status dashboard-status-confirmed'>Izdelek je potrjen</span>",         
    
    #The default value for products added before statuses were implemented. What should the string for this be?
    #'None': "<span class = 'dashboard-status dashboard-status-unconfirmed'>Izdelek nima vnešenega statusa</span>"               
    'None': "<span class = 'dashboard-status dashboard-status-entered'>Izdelek čaka na potrditev</span>"               
}