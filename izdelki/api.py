import requests
from izdelki.utilities import write_products, build_dict_products
from izdelki.product import calculate_energy_value
# from utilities import write_products, build_dict_products
import logging


def pretty_print_post(req):
    """
    Takes a prepared POST request and returns a string in a human readable way. Used for debugging.
    """
    return '{}\n{}\n{}\n\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    )


class API:
    #URL = "http://open.ijs.si/si/sl_SI/api/"
    URL = "http://opkp.si/sl_SI/api/"
    KEY = "pevjNs8ccDz5JwDRgKQUx46HPaKdNGWprW3HgwJL"

    LOGIN = "authenticate/access-token"
    REGISTER = "authenticate/register"

    FOOD_ADD = "food/recipe"
    FOOD_LABEL_ADD = "food/ingredient"
    FOOD_ADD_PHOTO = "food/photo"
    FOOD_DOWNLOAD = "food/download"
    FOOD_DIETS = "food/diets"
    FOOD_COOKING_METHODS = "food/cook-methods"
    FOOD_GROUPS = "food/groups"
    FOOD_RETENTION_FACTORS_SPECIFIC = "food/retention-factors-specific"
    FOOD_RETENTION_FACTORS_GENERIC = "food/retention-factors-generic"
    FOOD_CALCULATE = "food/calc-recipe"
    FOOD_REMOVE = "food/mark-deleted"

    session = None  # a session object with access token in the headers will be stored here
    debug = False  # determines whether to print info messages to console

    logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        if len(kwargs) > 2 or not kwargs:
            return
        try:
            self.login(**kwargs)
        except requests.exceptions.ConnectionError as e:
            msg = "Could not connect to the database."
            self.logger.error(repr(e) + "\n" + msg)
            if self.debug: print(msg)
        except requests.exceptions.RequestException as e:
            msg = "Could not retrieve data from the database."
            self.logger.exception(msg)
            if self.debug: print(msg)

    def log_info(self, msg):
        """Logs the msg as 'info' and, if self.debug,  prints it to console."""
        self.logger.info(msg)
        if self.debug: print(msg)

    def login(self, **kwargs):
        """
        Tries to log into the API and retrieve the access token (which is to be used in all subsequent requests) when
        given two parameters (assumed to be API username and password). When given one parameter (assumed to be
        access token), logging into API is skipped. In both cases, if the access token exists, a Session object is
        created and the access token is added into headers.
        """
        if self.logged_in():
            msg = "Tried to login, but is already logged in."
            self.logger.warning(msg)
            if self.debug: print(msg)
            return
        if not 0 < len(kwargs) < 3:
            msg = "Tried to login with too many or too litle parameters."
            self.logger.error(msg)
            if self.debug: print(msg)
            return
        access_token = ''
        if len(kwargs) == 2:
            username, password = kwargs['username'], kwargs['password']
            url = self.URL + self.LOGIN
            user_data = {
                'username': username,
                'password': password,
                'active-check': 0,
                'api_key':  self.KEY
            }
            try:
                r = requests.post(url, data=user_data)
            except Exception as e:
                self.logger.warning("Could not connect to api server: " + repr(e))
                return
            try:
                r.raise_for_status()
            except requests.exceptions.RequestException as e:
                msg = "Could not login."
                self.logger.exception(msg)
                if self.debug: print(msg + "\n" + repr(e))
                return
            response = r.json()
            if response['success']:
                access_token = response['access_token']
            else:
                msg = "Could not login, reason: " + response['reason'] + '.'
                self.logger.error(msg)
                if self.debug: print(msg)
        else:  # len(kwargs) == 1 -- we already have an access token
            access_token = kwargs['access_token']

        if access_token:
            # prepares the session with access token in the headers
            self.session = requests.Session()
            self.session.headers.update({'Authorization': 'Client-ID ' + access_token})
            self.log_info("Successfully logged in.")

        return access_token

    def logout(self):
        """Deletes the session object (with the saved access token)."""
        if self.session is None:
            msg = "Tried to logout, but is not logged in yet."
            self.logger.warning(msg)
            if self.debug: print(msg)
            return
        self.session.close()
        self.session = None
        self.log_info("Successfully logged out.")

    def logged_in(self):
        """Checks whether a session with access token already exists."""
        return self.session is not None

    def register(self, username, password, email):
        """
        Tries to register a new user with the API. If this is successful, it uses the received access token to
        create a Session object with the right headers.
        """
        if self.logged_in():
            msg = "Tried to register, but is already logged in."
            self.logger.warning(msg)
            if self.debug: print(msg)
            return
        url = self.URL + self.REGISTER
        user_data = {
            'username': username,
            'password': password,
            'email': email,
            'api_key': self.KEY
        }
        try:
            r = requests.post(url, data=user_data)
        except Exception as e:
            self.logger.warning("Could not connect to api server: " + repr(e))
            return
        try:
            r.raise_for_status()
        except requests.exceptions.RequestException as e:
            msg = "Could not register."
            self.logger.exception(msg)
            if self.debug: print(msg + "\n" + repr(e))
        response = r.json()
        if response['success']:
            access_token = response['access_token']
            self.session = requests.Session()
            self.session.headers.update({'Authorization': 'Client-ID ' + access_token})
        else:
            msg = "Could not register, reason: " + response['reason'] + '.'
            self.logger.error(msg)
            if self.debug: print(msg)

    def requires_login(self):
        """ Checks if the user is logged in, if not logs an error and returns False."""
        if not self.logged_in():
            msg = "You are not logged in, data could not be retrieved."
            self.logger.error(msg)
            if self.debug: print(msg)
            return False
        return True

    def make_send_handle_request(self, url, params=None, post=False):
        """
        Makes and sends request using self.session with params and handles potential exceptions. If post,
        sends a POST request; default is GET.
        """
        # prepare request
        if params is None:
            params = {}
        req = requests.Request('POST', url, data=params) if post else requests.Request('GET', url, params=params)
        prepared_req = self.session.prepare_request(req)

        # log prepared request
        self.log_info(pretty_print_post(prepared_req))

        # send request and handle exceptions
        resp = self.session.send(prepared_req)
        try:
            resp.raise_for_status()
        except requests.exceptions.RequestException as e:
            msg = "Data could not be retrieved."
            self.logger.exception(msg)
            if self.debug: print(msg + "\n" + repr(e))
        try:
            response = resp.json()
        except Exception as e:
            self.logger.warning("Problem parsing API response: " + repr(e))
            return
        if not response['success']:
            msg = "Data could not be retrieved, reason: " + response['reason'] + '.'
            self.logger.error(msg)
            if self.debug: print(msg)
            return
        return response

    def download_food(self):
        """Downloads all food items from API and writes it to food.json file, location specified in setup."""
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_DOWNLOAD
        food_params = {
            'fields': 'CODE,NAME,COMMERCIAL_NAME,DIETS,INGREDIENTS,SERVING,COUNTRY,BARCODE,PRODUCER,GROUP_ID,PHOTO,'
                      'SOURCE,IS_PUBLIC,AUTHOR,COMPONENTS,SELF_API,COOKMETH, NETOWEIGHT, IS_RECIPE, FAVSCORE, STATUS',
            'components': 'PROT, PROTAN, PROTPL, AAEB, ILE, LEU, LYS, AAT, PHE, ASPM, GLUTN, FAT, FASAT, FAMS, F18:2, F18:3, F20:4N6, F20:5N3, F22:6N3, F14:0, F16:0, F18:0, F18:1CN9, FAPUN3, FAPUN6, FAPU, CHORL, CHOT, SUGAR, SSUGAR, FSUGAR, FRUS, GALS, GLUS, LACS, SUCS, STARCH, FIBT, FIBC, FIBINS, FIBSOL, VITA, CARTA, CARTB, CARTG, VITD, VITE, VITK, VITC, THIA, RIBF, NIA, VITB6, PANTAC, BIOT, FOL, VITB12, NA, CLD, K, CA, MG, P, FE, ID, FD, ZN, CU, MN, CR, MO, PRO, SE, WATER, ALC, TOTAL-ORAC, GI, GL, COLOR, PROBIO, CO2E'
        }
        response = self.make_send_handle_request(url, food_params)
        if response:
            write_products(build_dict_products(response))
            self.log_info("Food successfully downloaded.")
            


    def download_food_local(self):
        """Downloads all food items from API and returns them"""
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_DOWNLOAD
        food_params = {
            'fields': 'CODE,NAME,COMMERCIAL_NAME,DIETS,INGREDIENTS,SERVING,COUNTRY,BARCODE,PRODUCER,GROUP_ID,PHOTO,'
                      'SOURCE,IS_PUBLIC,AUTHOR,COMPONENTS,SELF_API,COOKMETH, NETOWEIGHT, IS_RECIPE',
            'components': 'PROT, PROTAN, PROTPL, AAEB, ILE, LEU, LYS, AAT, PHE, ASPM, GLUTN, FAT, FASAT, FAMS, F18:2, F18:3, F20:4N6, F20:5N3, F22:6N3, F14:0, F16:0, F18:0, F18:1CN9, FAPUN3, FAPUN6, FAPU, CHORL, CHOT, SUGAR, SSUGAR, FSUGAR, FRUS, GALS, GLUS, LACS, SUCS, STARCH, FIBT, FIBC, FIBINS, FIBSOL, VITA, CARTA, CARTB, CARTG, VITD, VITE, VITK, VITC, THIA, RIBF, NIA, VITB6, PANTAC, BIOT, FOL, VITB12, NA, CLD, K, CA, MG, P, FE, ID, FD, ZN, CU, MN, CR, MO, PRO, SE, WATER, ALC, TOTAL-ORAC, GI, GL, COLOR, PROBIO, CO2E'
        }
        response = self.make_send_handle_request(url, food_params)
        if response:
            self.log_info("Food successfully downloaded.")    
            return build_dict_products(response)


    def download_one_product(self, code):
        """Downloads one food item by its code(id)."""
        print("Downloading product with code " + code)
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_DOWNLOAD
        print(url)
        params = {'code': code}
        print(params)
        response = self.make_send_handle_request(url, params)
        print(response)
        if response:
            self.log_info("Product {0} successfully downloaded.".format(code))
            self.log_info(response)
            return response['items'][0]

    def get_diets(self):
        """Retrieves diets from the database."""
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_DIETS
        response = self.make_send_handle_request(url)
        if response:
            self.log_info('Diets successfully retrieved.')
            return response['diets']

    def get_cooking_methods(self):
        """Retrieves cooking methods from the database."""
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_COOKING_METHODS
        response = self.make_send_handle_request(url)
        if response:

            self.log_info('Cooking methods successfully retrieved.')
            return response['methods']

    def get_groups(self):
        """Retrieves food groups from the database."""
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_GROUPS
        response = self.make_send_handle_request(url)
        if response:
            self.log_info('Food groups successfully retrieved.')
            return response['groups']

    def add_product(self, data):
        """Adds a new product (given with a dict data) to the database."""
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_ADD

        response = self.make_send_handle_request(url, data, post=True)
        if response:
            self.log_info('Product {0} has been added.'.format(data))
            return response['code']

    def add_product_from_label(self, data):
        """Adds a new product (given with a dict data) to the database."""
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_LABEL_ADD

        response = self.make_send_handle_request(url, data, post=True)
        if response:
            self.log_info('Product {0} has been added.'.format(data))
            return response['code']

    def add_photo(self, code, photo_bin):
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_ADD_PHOTO + '?code=' + code
        try:
            resp = requests.post(url=url,
                            data=photo_bin,
                            headers={'Content-Type': 'application/octet-stream', 'Authorization': self.session.headers['Authorization']})
        except Exception as e:
            self.logger.warning("Could not connect to api server: " + repr(e))
            return
        try:
            resp.raise_for_status()
        except requests.exceptions.RequestException as e:
            msg = "Failed uploading photo for " + code
            self.logger.exception(msg)
            if self.debug:
                print(msg + "\n" + repr(e))
        response = resp.json()
        if not response['success']:
            msg = "Photo upload failed " + code
            print(msg)
            self.logger.error(msg)
            if self.debug:
                print(msg)
            return
        return response

    def calc_product(self, ingredients, cook_method, group, serv_size):
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_CALCULATE

        params = {
            'cook_method': cook_method,
            'group': group,
            'serv_size': serv_size
        }

        for i, ing in enumerate(ingredients):
            params["items[%d][code]" % i] = ing['code']
            params["items[%d][weight]" % i] = ing['weight']
        response = self.make_send_handle_request(url, params, post=True)

        if response and response['success']:
            components = {name.lower(): val for name, val in response['components'].items()}
            if 'enera' in components:
                enera = components['enera']
                #components['enval'] = [enera/1e3, enera/4184];
                #use our energy value calculation instead. Just converting to kcal with enera/4184
                #leads to inconsistencies with energy value calculated by using provided energy factors
                components['enval'] = calculate_energy_value(components, lower=True)
            return components
        
        return None

    def get_specific_retention_factor(self, comp=None, food_group=None, cook_method=None):
        """Retrieves specific retention factors from the database (these differ on soak, breaded ... columns)."""
        if not self.requires_login():
            return

        url = self.URL + self.FOOD_RETENTION_FACTORS_SPECIFIC
        parameters = {
            'COMPID': comp,
            'FOODID': food_group,
            'PREPMETH': cook_method
        }
        response = self.make_send_handle_request(url, parameters)
        if response:
            self.log_info('Specific retention factors for COMPID {0}, FOODID {1} and PREPMETH {2} successfully '
                          'retrieved.'.format(comp, food_group, cook_method))
            return response['items']

    def get_generic_retention_factor(self, comp=None, category=None):
        """Retrieves a generic retention factor from the database."""
        if not self.requires_login():
            return

        url = self.URL + self.FOOD_RETENTION_FACTORS_GENERIC
        parameters = {
            'COMPID': comp,
            'CATEGORY': category
        }
        response = self.make_send_handle_request(url, parameters)
        if response:
            self.log_info('Generic retention factor for COMPID {0} and CATEGORY {1} successfully '
                          'retrieved.'.format(comp, category))
            return response['items']


    def delete_product(self, product_id):
        if not self.requires_login():
            return
        url = self.URL + self.FOOD_REMOVE
        data = {'code': product_id}

        response = self.make_send_handle_request(url, data, post=True)
        if response:
            self.log_info('Product {0} has been removed'.format(product_id))
            return response['code']



if __name__ == '__main__':
    test = API()
    test.login(username='anonymous', password='anonymous')
    test.download_food()
    test.logout()
