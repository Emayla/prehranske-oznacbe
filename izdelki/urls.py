﻿from django.conf.urls import url
from django.contrib.auth import views as auth_views
from izdelki.auth_forms import HorizontalLoginForm
from . import views
from izdelki.views import get_pdf


from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView



urlpatterns = [
    #The login view wants to redirect us to 'home' after login from index
    #So we also need to have the name home associated with out index page
    url(r'^$', views.index, name='home'),
    url(r'^$', views.index, name='index'),
    url(r'^search/$', views.search, name='search'),
    url(r'^id/(?P<product_id>[A-Za-z0-9_]+)/$', views.detail, name='detail'),
    url(r'^register/$', views.register, name='register'),
    url(r'^edit_user/$', views.edit_user, name='edit_user'),
    #url(r'^login/$', views.login_view, name='login'),
    url(r'^login/$', auth_views.login, {'template_name': 'izdelki/login.html', 'authentication_form':
        HorizontalLoginForm}, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^add/$', views.add, name='add'),
    url(r'^add_label/$', views.add_label, name='add_label'),
    url(r'^get_components', views.get_component_values, name='get_components'),
    url(r'^add/submit/$', views.add_submit, name='add_submit'),
    url(r'^add/submit_label/$', views.add_submit_label, name='add_submit_label'),
    url(r'^add_label/submit_label/$', views.add_submit_label, name='add_submit_label', kwargs={'redirect': True}),
    url(r'^edit/(?P<product_id>[A-Za-z0-9_]+)/$', views.add, name='edit'),
    url(r'^edit/(?P<product_id>[A-Za-z0-9_]+)/submit/$', views.add_submit, name='edit_submit'),
    url(r'^edit/(?P<product_id>[A-Za-z0-9_]+)/submit_label/$', views.add_submit_label, name='edit_submit_label'),
    url(r'^edit_label/(?P<product_id>[A-Za-z0-9_]+)/$', views.add_label, name='edit_label'),
    url(r'^edit_label/(?P<product_id>[A-Za-z0-9_]+)/submit_label/$', views.add_submit_label, name='edit_submit_label'),
    url(r'^about/$', views.about, name='about'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^dashboard/submit$', views.dashboard_submit, name='dashboard_submit'),
    url(r'^delete/$', views.delete, name='delete'),
    url(r'^register_done/$', views.register_done, name='register_done'),
    url(r'^help/$', views.help, name='help'),
    url(r'^about/$', views.about, name='about'),
    url(r'^terms/$', views.terms_of_use, name='terms_of_use'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^password_reset/$', auth_views.password_reset, 
        {'template_name': 'izdelki/password_reset.html', 
         'email_template_name': 'izdelki/password_reset_email.html',
         'subject_template_name': 'izdelki/password_reset_email_subject.txt'},name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, 
        {'template_name': 'izdelki/password_reset_done.html'}, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, 
        {'template_name': 'izdelki/password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, 
        {'template_name': 'izdelki/password_reset_complete.html'}, name='password_reset_complete'),
    url(r'^get_xls/$', views.get_xls, name='get_xls'),
    url(r'^get_pdf/$', views.get_pdf, name='get_pdf'),
    url(r'^get_xls/(?P<product_id>[A-Za-z0-9_]+)/$', views.get_xls, name='get_xls'),
    url(r'^get_pdf/(?P<product_id>[A-Za-z0-9_]+)/$', views.get_pdf, name='get_pdf'),
    url(r'^components\.json/$', views.get_product_components, name='components_json'),
    url(r'^password_change/$', views.password_change, name='password_change'),
    url(r'^check_company_id/$', views.check_company_id, name='check_company_id'),
    url(r'^database_redownload/$', views.database_redownload, name='database_redownload'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            views.activate, name='activate'),
    url(r'^api/search/$', views.api_search, name='api_search'),
    url(r'^api/search_test/$', views.api_search_test, name='api_search_test'),
    url(r'^api/test/$', views.api_test, name='api_test'),
    url(r'^api/test_header/$', views.api_header, name='api_test_header'),
    url(r'^api/search2/$', views.api_search_alt_auth, name='api_search_alt_auth'),




    #Favicon redirects:
    #Browsers look for favicon icons in the root directory, but we have them in our static dir
    #So we need to redirect the root requests to the corect resouces
    url(
        r'^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/favicon.ico'),
            permanent=False),
        name="favicon"
    ),

    url(
        r'^favicon-32x32.png$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/favicon-32x32.png'),
            permanent=False),
        name="favicon-32"
    ),

    url(
        r'^favicon-16x16.png$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/favicon-16x16.png'),
            permanent=False),
        name="favicon-16"
    ),

    url(
        r'^apple-touch-icon.png$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/apple-touch-icon.png'),
            permanent=False),
        name="favicon-apple"
    ),

    url(
        r'^site.webmanifest$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/site.webmanifest'),
            permanent=False),
        name="manifest"
    ),

    url(
        r'^safari-pinned-tab.svg$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/safari-pinned-tab.svg'),
            permanent=False),
        name="favicon-safari"
    ),

    url(
        r'^android-chrome-192x192.png$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/android-chrome-192x192.png'),
            permanent=False),
        name="favicon-chrome192"
    ),

    url(
        r'^android-chrome-256x256.png$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/android-chrome-256x256.png'),
            permanent=False),
        name="favicon-chrome256"
    ),

    url(
        r'^browserconfig.xml$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/browserconfig.xml'),
            permanent=False),
        name="favicon-browserconfig"
    ),

    url(
        r'^mstile-150x150.png$',
        RedirectView.as_view(
            url=staticfiles_storage.url('izdelki/resources/img/favicon/mstile-150x150.png'),
            permanent=False),
        name="favicon-mstile"
    ),






]
