from django.apps import AppConfig


class IzdelkiConfig(AppConfig):
    name = 'izdelki'
    verbose_name = 'Izdelki'
