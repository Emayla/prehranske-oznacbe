# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-25 12:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('izdelki', '0006_auto_20171031_2354'),
    ]

    operations = [
        migrations.AddField(
            model_name='basicuser',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
