//The maximum number of letters before we start shortening search results
//Each position of the array represents a respective column:
//[name, producer, group, barcode]
////we don't shorten barcodes (since they have a defined max limit of 13)
///So the last number just needs to be something above 13
var MAXIMUM_COLUMN_LENGTHS = [15,15,34,20];

// Makes rows of search results clickable by directing the window location to the url stored in tr.
$( document ).ready(function() {
    $('.clickable-row').click(function() {
        window.location = $(this).data('url');
    });
    highlightResults($('tr.clickable-row .search-results-product-name'));


    $("#search-input").focus();

    //Initialize tooltips
    //$('[data-toggle="tooltip"]').tooltip()

});


/*Insert one sting into another at specified index
https://stackoverflow.com/questions/4313841/javascript-how-can-i-insert-a-string-at-a-specific-index?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa*/
function insert(str, index, value) {
    return str.substr(0, index) + value + str.substr(index);
}

// Makes jquery :containsi selector which is the same as :contains but is case insensitive.
$.extend($.expr[':'], {
  'containsi': function(elem, i, match, array)
  {
    return (elem.textContent || elem.innerText || '').toLowerCase()
    .indexOf((match[3] || "").toLowerCase()) >= 0;
  }
});





/**
 * Convert HTML escaped characters into normal characters. This is used by the highlight result
 * function to prevent for example "a" matching every "&amp;"
 *
 * By default, Dajngo will escape all product names. This causes problems for the search highlight function:
 * searching for a will highlight all & matches, since they are encoded as &amp; It will actually break the encoding,
 * since &amp will be turned oint &<span class="highlight">a</span>mp;
 *
 * THis function will remove Django's default escaping and let the highlight function search on the original name.
 * After the search, the text will need to be escaped again.
 * 
 * WARNING: Never put the unescaped HTML string into the actual HTML, to prevent code injection attacks
 * @param  {[type]} string [description]
 * @return {[type]}       [description]
 */
function unescapeHTMLString(escapedString){
    return escapedString.replace('&amp;', '&')
                        .replace('&lt;',  '<')
                        .replace('&gt;',  '>')
}


/**
 * Updates highlight_indices with additional ofsets.
 *
 * Every index that starts after escape_sequence_start will have it's start and end indices increased by escape_sequence_offset
 * Every index that starts exactly on escape_sequence_start will have its end index increased be escape_sequence_offset
 * 
 * @param  {Array} highlight_indices An array of objects, each with two properties: start and end. 
 * @return {Array} The array of highlight indices, properly adjusted.
 */
function updateIndicesWithOffset(highlight_indices, escape_sequence_start, escape_sequence_offset) {
    for (var i = 0; i < highlight_indices.length; i++) {
        if (highlight_indices[i].start == (escape_sequence_start)) {
            highlight_indices[i].end += escape_sequence_offset;
        } else if (highlight_indices[i].start > (escape_sequence_start)) {
            highlight_indices[i].end += escape_sequence_offset;
            highlight_indices[i].start += escape_sequence_offset;
        }
    }

    return highlight_indices
}

/**
 * When the highlightResults funciton finds what parts of the string to highlight, 
 * it does this on an non escaped HTML string (&, <, > are all displayed as is, instead of as &amp;, %lt;, &gt;)
 *
 * However, the string we want to highlight will need to be escaped to prevent security issues. This means that
 * the indices from the unescaped string will need to be updated to their correct positions on the escaped string.
 * @param  {Array} highlight_indices An array of objects, each with two properties: start and end. 
 * @return {Array} The array of highlight indices, properly adjusted.
 */
function updateUnescapedHighlightIndices(highlight_indices, escaped_string) {
    for (var i = 0; i < escaped_string.length; i++) {
        if (escaped_string.charAt(i) == '&'){
            var current_escape_sequence_start = i;
            var current_escape_sequence_offset = 0;
            //A & character in an escaped string represents the start of a special character
            while (escaped_string.charAt(i) != ';' && i <  escaped_string.length){
                //Every & character will be closed by a ; character that marks the end of the escape sequence
                current_escape_sequence_offset++;
                i++;
            }

            //After the loop, current_escape_sequence_offset has the length of the escapes character. 
            //This offset will need to be added to every index that starts after the character where
            //the escape sequence started
            highlight_indices = updateIndicesWithOffset(highlight_indices, 
                                                        current_escape_sequence_start,
                                                        current_escape_sequence_offset);
        }
    }
    return highlight_indices;
}

// Highlights the search query within the results.
// Simply using the regex insert breaks, because the regex will also pick up the HTML tags
// Instead, first loop over the string to find out where to insert tags, then insert tham at the end
// so that the search doesn't pick up the tags
function highlightResults (target) {
var highlightHtml = '<span class="highlight">$1</span>';
//var query = $('#search').val();
var query = searchQuery;
if (query !== '') {
    
    
    query = query.split(' ');
    
    
        
            // console.log('#search-results td:containsi(' + escapeQuotes(value) + ')');
            //$('#search-results td:containsi(' + escapeQuotes(value) + ')').each(function () {
            target.each(function () {
                var element = this;
                var highlight_indices = [];
                $.each(query, function (index, value) {
                    if (value !== '') {
                        /*first-child = name, last-child = barcode*/
                        if ($(element).is(':first-child') || $(element).is(':last-child')) {
                            /*
                            var txt = $(this).html();
                            txt = txt.replace(new RegExp('(' + escapeSelector(value) + ')', 'gi'), highlightHtml);
                            $(this).html(txt);
                            */
                            var original_txt = $(element).html();
                            var txt = unescapeHTMLString(original_txt);
                            var regex = new RegExp('(' + escapeSelector(value) + ')', 'gi');
                            while ( (result = regex.exec(txt)) ) {
                                highlight_indices.push({'start': result.index, 'end': result.index + value.length})
                            }
                        }
                    }
                 });

                
                if (highlight_indices.length === 0) return;
                //now that we have the places to highlight, we just need to properly insert the highlight tags
                //Every time we insert a tag, the string gets longer. So we will need to keep track of this
                //
                //In order to do this, it's easier if we first sort our indices, and then merge overlapping ones
                
                //First, sort indices
                highlight_indices.sort(function(a,b) {
                    return a.start - b.start
                });

                highlight_indices_merged=[]
                

                //now, merge overlapping indices
                
                var current_start_index = highlight_indices[0].start;
                var current_end_index = highlight_indices[0].end;
                for (var i = 1; i < highlight_indices.length; i++) {
                    if (highlight_indices[i].start <= current_end_index) {
                        current_end_index=Math.max(highlight_indices[i].end, current_end_index);
                    } else {
                        highlight_indices_merged.push({'start': current_start_index, 'end': current_end_index})
                        current_start_index = highlight_indices[i].start;
                        current_end_index = highlight_indices[i].end;
                    }
                }

                highlight_indices_merged.push({'start': current_start_index, 'end': current_end_index})

                highlight_indices = highlight_indices_merged;


                var offset = 0
                var open_tag = '<span class="highlight">'
                var close_tag = '</span>'
                /* Get the HTML text again. Since the unescape unction never overwrote it,
                 * this will still be the original escaped text.
                 * Because of this, some indices will need to be updated
                 * to take into account the new text length
                */
                var text = $(element).html();
                highlight_indices = updateUnescapedHighlightIndices(highlight_indices, text)

               
            

                for (var i = 0; i < highlight_indices.length; i++) {
                    text = insert(text, highlight_indices[i].start + offset, open_tag);
                    offset += open_tag.length;
                    text = insert(text, highlight_indices[i].end + offset, close_tag);
                    offset += close_tag.length;
                }

                $(element).html(text);

            });




    }
}

/*Not used anymore. Instead, special css classes are added ad removed on hover
in the $(".searchtableTop").on function*/
function generateEllipsis(target){
    target.children().each(function(index, value){
        var column_index = $(this).index();
        var max_len = MAXIMUM_COLUMN_LENGTHS[column_index];
        var txt = $(this).text();
        if (txt.length > max_len){
            /*The group column as the following problem: 
            There is only one very long group name (Sok iz svežega sadja brez dodanega sladkorja)
            That we need to shorten. 

            But, shortening this name by number of characters doesn't work well, because in addition to
            having a lot of characters, the characters are also wider than average:

            For example, the second largest group is 34 characters large and fits on one line. 
            Cuting the longest group to 34 characters takes a line and a half

            So we just cut it manually by hardcoding the string*/
            if (txt === "Sok iz svežega sadja brez dodanega sladkorja"){
                var txt_short = "Sok iz svežega sadja brez …";
            } else {
                var txt_short = txt.substring(0,max_len) + "…";
            }
            $(this).html(txt_short);
            
            $(this).hover(
                function(){
                    $(this).html(txt);
                    highlightResults($(this).children('.search-results-product-name'));
                },
                function(){
                    $(this).html(txt_short);
                    highlightResults($(this).children('.search-results-product-name'));
                });
        }
    });
}



/*Infinite scroll*/
var infinite = new Waypoint.Infinite({
    element: $('.infinite-container')[0],
    onBeforePageLoad: function () {
        $('.loading').show();
    },
    
    onAfterPageLoad: function ($items) {
        $('.loading').hide();

        $($items).click(function() {
            window.location = $(this).data('url');
        });
        //generateEllipsis($items);
        highlightResults($items.children('.search-results-product-name'));
    }
});


//TODO: Which variant to use:
//1. Expand just the column the user mouses over
//2. Expand all columns in the row when the user mouses over a row
$(".searchtableTop").on({
    mouseenter: function () {
        /*
        $(this).removeClass('searchEllipsis')
        $(this).addClass('searchExpand');
        */

        $(this).siblings().addBack().removeClass('searchEllipsis')
        $(this).siblings().addBack().addClass('searchExpand');
    },
    mouseleave: function () {
        /*
        $(this).addClass('searchEllipsis')
        $(this).removeClass('searchExpand');
        */

        $(this).siblings().addBack().addClass('searchEllipsis')
        $(this).siblings().addBack().removeClass('searchExpand');
    }
}, "td");