function toggleLogin () {
    //If login is already in the midle of collapsing (
    //for example, if the user clicks the login button fast twice),
    //then calling collapse the second time will do nothing.
    //
    //The result will be:
    //1st call: dim screen, expand login
    //2nd call: undim screen, keep login expanded (because it's still expanding)
    //
    //For this reason, don't allow login toggle when the login UI is still expanding
    //(in that case, #login will have class collapsing)
    if ($('#login').hasClass('collapsing')){
        return;
    }
    $('#login').collapse('toggle');
    $('#dim').fadeToggle('fast');
    window.scrollTo(0, 0);
    
}

/*Setup callback functions for login box*/
$(function(){
    /*Called when the show animation starts: show triangle*/
    $('#login').on('show.bs.collapse', function () {
        /*Only focus the input box after the animation finishes
          Otherwise, there are issues with the browsers input suggestion box being in the wrong place*/
        $("#login-arrow").show();
    });

    /*Called when the hide animation finishes: hide triangle*/
    $('#login').on('hidden.bs.collapse', function () {
        /*Only focus the input box after the animation finishes
          Otherwise, there are issues with the browsers input suggestion box being in the wrong place*/
        $("#login-arrow").hide();
    });


    /*Called when the show animation finishes: focus the input*/
    $('#login').on('shown.bs.collapse', function () {
        /*Only focus the input box after the animation finishes
          Otherwise, there are issues with the browsers input suggestion box being in the wrong place*/
        $("#id_username").focus();
    });
});

/*
$('#login-button').click(function () {
    $('#dim').fadeToggle('fast');
});
*/

$('body').click(function (e) {
    var clickedOn = $(e.target);


    if (clickedOn.parents().andSelf().is('#login')){
        return;
    }
    
    if (clickedOn.parents().andSelf().is('#login-button') || clickedOn.parents().andSelf().is('#login-text')) {
        toggleLogin();
    } else if ($("#login").hasClass('in')){
        toggleLogin();
    }
});

