/**
 * Contains all javascript variables and functions used by multiple other files
 * and other misc utility functions
 *
 * @module add/common
 */


//Calculates the component values of the currently entered product
//based on the entered ingredients
//
//The ingredients are pushed to the server, and the server returns 
//the correct component values
//
var requestComponentValues = $.debounce(500, false, function() {
    var reqIngredients = gatherIngredients();
    if (!reqIngredients) return;

    var reqComponents = [];
    $('#listComponents tr').each(function () {
        var id = $(this).attr('id');
        if (id) {
            reqComponents.push(id);
        }
    });
    //we also need salt, but the above will put in the 'salt' component
    //we actually need the 'nat' component
    //'salt'=2.5*'nat'
    reqComponents.push('na')

    $.ajax({
        url: '/get_components/',
        data: { 
            'components': JSON.stringify(reqComponents), 
            'ingredients': JSON.stringify(reqIngredients), 
            'cook_method': $('#listMethod tr:last-child').attr('id'), 
            'group': $('select[name=group]').val(), 
            'serv_size': $('input[name=serv_size]').val(),
        },
        type: 'GET',
        success: function(response) {
            refreshComponents(response['components']);
            refreshPercentages(response['percentages']);
            // console.log('test ' + response);
        },
        error: function(error) {
            console.log('error ' + error);
        }
    });
});



/**
 * Exclude ceratin links/buttons from giving a warning for leaving the page
 * This effecths both  export buttons and the login button
 *
 * This function isn't used right now, since we don't show a warning for leaving the page
 */
function disableUnloadWarning(e){
    preserveData=true; //Don't clear localstorage on unload
    $(window).off('beforeunload');
}


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}



var matchComp = function (to_match, pattern) {
    return function (x) {
        return x[to_match] === pattern
    }
};


/**
 * Resets the add buttons (from ingredient, from label) to the default non selected state
 * @param  {object} button - The jQuery button to reset
 */
function clearAddButton (button){
    button.removeClass('add-product-focus');
    button.children('img').attr('src', addImgUrl);
}

/**
 * Togles the add buttons (from ingredient, from label) between selected and not-selected
 * @param  {object} button - The jQuery button to toggle
 */
function toggleAddButton (button){
    button.toggleClass('add-product-focus');
    if (button.hasClass('add-product-focus')){
        button.children('img').attr('src', addInvertedImgUrl);
    } else {
        button.children('img').attr('src', addImgUrl);
    }
}



/**
 * Finds all ingredients entered by the user and returns them
 * @return {array} - An array of all ingredients, ready to be pushed to the server
 */
function gatherIngredients() {
    var reqIngredients = [];
    var inputError = false;
    $('#listIngredients tr').each(function () {
        var ingInput = $(this).find('input');
        if (ingInput.hasClass('error')) inputError = true;
        reqIngredients.push({ 'code':$(this).attr('id'), 'weight':ingInput.val().replace(',','.') });
    });
    return !inputError ? reqIngredients : null ;
}

/**
 * Finds all allergens entered by the user and returns them in
 * a form that can be sent to the server
 * @return {array} - The array of allergens entered by the user
 */
function gatherAllergens() {
    var reqAllergens = [];
    $('#listAllergens tr').each(function () {
        reqAllergens.push(parseInt($(this).attr('id')));
    });
    return reqAllergens;
}


/**
 * Displays component values in newValues in the page component table
 * @param  {array} newValues - The component values to display
 */
function refreshComponents (newValues) {
    $.each(newValues, function (key, value) {
        if (key === 'enval') {
            envalkJ = value[0];
            envalkcal = value[1];
            value = value[1];
        }
        var comp = otherComponents.find(matchComp('code', key.toUpperCase()));

        //refresh the originalComponent values with the new ones received from server
        //otherwise, unit conversion will still be working on the original values
        if (key === "salt") {
            originalComponents["NA"] = value / 2.5;
        } else {
            originalComponents[key.toUpperCase()] = value;
        }
        /*
        if (comp) {
            if (comp['def_unit'] !== 'g')
                value = convertUnits(value, 'g', comp['def_unit']);
        }
        */
        var currentUnit = findComponentUnit(key.toLowerCase());
        if (key !== "enval"){
            value = convertUnits(value, 'g', currentUnit);
        }
        setNutrientValue(key, '100', value);


    });
    calculatePortionNutrients();
}


/**
 * Takes a component code and returns the current unit (g, mg, mcg) of this component code.
 *
 * This unit is the unit that is currently displayed in the component table. This unit can be changed by the users.
 * @param  {string} component [The component code to find the unit of]
 * @return {string}           [The current unit of the component. One of "g, mg, mcg, kcja, kJ". Returns the components default unit if the component isn't in the table, or 'g' if the component can't be found]
 */
function findComponentUnit(component) {
    var selector = ".product-table tr#" + component;
    var element = $(selector);
    var defUnit = 'g';
    var componentDetails = otherComponents.find(matchComp('code', component.toUpperCase()));
    if (componentDetails){
        defUnit = componentDetails['def_unit'];
    }
    


    if (element.length === 0) {
        //we didn't find this code. If the user is editing a label, the tows have different IDs. Try those
        var selector = ".product-table tr#" + component + "Label";
        element = $(selector);
        if (element.length === 0) {
            //If this also doesn't exist, then the component isnt in the table
            return defUnit;
        }
    }

    var unitSelector = ".unit-change small span";
    var unit = element.find(unitSelector).text();
    
    if (unit.length === 0) {
        return defUnit;
    }
    return unit;

}

/**
 * Displays the percantage of daily intake based on the provided values
 * @param  {object} newValues - The component values to calculate percentages from
 */
function refreshPercentages (newValues) {
    $.each(newValues, function (key, value) {
        setNutrientValue(key, 'per', value);
    });
}



/**
 * Collects all values entered by the user and returns them in a form suitable
 * for uploading to the server
 * @return {object} - A data object that can be pushed to the server to add a product to the database
 *                    or edit an existing product
 */
function gatherAllProductData() {
    var data = $('#recipeForm').serializeArray();

    var items = gatherIngredients();
    if (!items) return;
    $.each(items, function (index, value) {
        $.each(value, function (k, v) {
            data.push({name: 'items[' +index+ '][' +k+ ']', value: v});
        });
    });

    data.push({name: 'diets', value: gatherAllergens()});
    data.push({name: 'cook_method', value: $('#listMethod tr:last-child').attr('id')});
    
    var hasErrors = false;
    $('input.productInput.gather').each( function () {
        if (this.id === "inputPortionSize" && $(this).val().length === 0){
            //We will check if portion size is empty seperately, so that we can show 
            //a special error message.
            return;
        }

        if ($(this).hasClass('error')) {
            hasErrors = true;
            return false;
        }

        var key = $(this).attr('name');
        var val = $(this).val();
        if ($(this).hasClass('number')) {
            val = val.replace(',','.');
            val = parseFloat(val);
        } else if ($(this).hasClass('integer')) {
            val = parseInt(val);
        }
        data.push({name: key, value: val});
    });
    if (hasErrors) return;

    if (selectedPhoto) {
        data.push({name: 'photo', value: selectedPhoto});
    }

    return data;
}



/**
 * This function is used in conjunction with the AreYouSure library for
 * displaying a warning if the user attempts to leave the add page with unsaved changes.
 *
 * The library uses a class called "dirty" to see if the warning should be triggered.
 * The class is automatically added to the #recipeForm whenever the form is changed.
 * (This means that leaving an unsaved form gives no warning, which is what we want).
 *
 * However, the library only detects changes on the form input fields, nothing else.
 * We also need to mark the from as dirty when the user:
 *   Adds Ingredients/Methods/Allergens OK
 *   Changes portion size
 *   Changes components when editing a label
 *   Adds a picture
 *
 * This function will be called in all of the above instances.
 *
 * The warning will also fire after saving a product, so we will also need a function there
 */
function dirtyForm(){
    //don't dirty the form while it's uploading
    $("#recipeForm").addClass("dirty");
}



(function ($) {
    /**
     * Inserts the ingredient HTML into the container element
     * @param {string} val    - The ID of the ingredient
     * @param {string} label  - The name of the ingredient
     * @param {Number} weight - The weight of the ingredient
     * @param {string} tooltip - A string containing basic ingredient component valuse, of the form 
     *                              OH:X B: X M:X. Will be displayed when hovering over the ingredient
     */
    $.fn.addIngredient = function (val, label, weight=0, tooltip="") {
        dirtyForm();
        this.find('tbody').append(
            $('<tr>', {id: val}).append(
                [$('<td>', {class:"ingredient-tooltip", text: label, "data-togle": "tooltip", "data-placement": "left", title: tooltip}),
                $('<td>').append($('<img>', {src: removeImgUrl, 'aria-hidden': true, class: 'removeImg'})),
                $('<td>', {class: 'weight'}).append($('<input>', {type: 'text', class: 'inputLikeTd', value: weight})),
                $('<td>', {class: 'unit', text: 'g'})]
                )
        );

        //Bootstrap tooltips
        //Needs conatiner:body otherwise the container <td> is resized on hover, which looks odd
        $("#" + val + " td").bootstrapTooltip({
            container:'body',
            html:true,
            template: '<div class="tooltip left" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner tooltip-inner-large"></div></div>'

        });


        saveIngredients();
        return this;
    };

    /**
     * Inserts the allergen HTML into the container element
     * @param {string} val    - The ID of the allergen
     * @param {string} label  - The name of the allergen
     */
    $.fn.addAllergen = function (val, label) {
        dirtyForm();
        this.find('tbody').append(
            $('<tr>', {id: val}).append(
                [$('<td>', {text: label}),
                $('<td>').append($('<img>', {src: removeImgUrl, 'aria-hidden': true, class: 'removeImg'}))]
            )
        );
        return this;
    };

    /**
     * Inserts the method HTML into the container element
     * @param {string} val     - The ID of the method
     * @param {string} label   - The name of the method
     * @param {number} serving - The weight of the finished product. Defaults to 0.
     */
    $.fn.setMethod = function (val, label, serving=0) {
        dirtyForm();
        this.find('tbody').html(
            $('<tr>').append(
                [$('<th>', {text: 'Postopek obdelave', colspan: 2}),
                $('<th>', { colspan: 2}).html('<div class="method-mass-container"> Končna masa </div>')]
            )
        );
        this.find('tbody').append(
            $('<tr>', {id: val}).append(
                [$('<td>', {text: label}),
                $('<td>').append($('<img>', {src: removeImgUrl, 'aria-hidden': true, class: 'removeImg'})),
                $('<td>', {class: 'weight'}).append($('<input>', {type: 'text', class: 'inputLikeTd productInput' +
                ' gather number numericInput', name: 'serv_size', value: serving})),
                $('<td>', {class: 'unit', text: 'g'})]
            )
        );
        return this;
    };

    /**
     * Inserts a new component into the main container table
     * @param {string} comp     - The component object
     * @param {string} label    - The name of the component
     * @param {number} before   - The ID of the component the new component should be inserted after
     *                            (For example, Vitamin C should come after Vitamin A, of after Salt if
     *                            Vitamin A doesn't exist in the table yet).
     */
    $.fn.addComponent = function (comp, label, before) {
        var code = comp['code'].toLowerCase();
        var newComp = '<tr id="' + code +'"><th>';
        if (comp['child']) newComp += '- od tega ' + label.toLowerCase();
        else newComp += label;

        //Unit change: Only add it to specific components
        newComp += '<div class="float-right inline-block">'
        if (unitChangeComponents.includes(comp.code)) {
            newComp += '<span class="unit-change"><small class="product-table-unit-select" data-placement="top"' +
                ' data-popover-content="#' + code + '-unit-popover" data-toggle="popover" data-trigger="focus"' +
                ' tabindex="0"> (<span id="' + code + '-unit">' + comp['def_unit'] + '</span>)</small>\n' +
                '<div class="hidden" id="' + code + '-unit-popover"><div class="popover-body"><ul id="' + code +
                '-units">';
            $.each(comp['units'], function (ind, unit) {
                newComp += '<li>' + unit + '</li>';
            });     
            newComp += '</ul></div></div></span></span>';
        }

        newComp += `<img src='${removeImgUrl}' aria-hidden='true' class='removeComponent'></span></div></th>\n` ;
        newComp += '<td id="' + code + '-100"><span class="value"></span> <span class="units">' + comp['def_unit'] + '</span></td>\n' +
            '<td id="' + code + '-port"><span class="value"></span> <span class="units">' + comp['def_unit'] + '</span></td>\n' +
            '<td id="' + code + '-per"><span class="value"></span> %</td>';
        
        newComp += '</tr>';
        $('#' + before).after(newComp);
        $('#' + code + ' [data-toggle="popover"]').initializePopover();
        return this;
    };
})(jQuery);


/**
 * Sets all functions and callbacks required for the sidebar to work.
 *
 * This function is called on page load, and whenever an ajax request to save a product fails.
 * 
 * When a ajax call for saving a product fails (for example, because a required field is missing),
 * the server responds with HTML code that includes error messages that tell the user which fields have problems.
 *
 * This HTML then replaces the sidebar. After the sidebar is replaced, we need to rerun all sidebar javascript code
 * that we run on page load.
 */
function initSidebar() {
    //after replaceWith call, we need to rerun some javascript functions
    //that don't properly get applied on the replaceWith call
    $('[id$="group"]').select2({
        placeholder:"Izberite kategorijo",
        allowClear: true
    });

    //reclear the group dropdown, but only if there's no group in it
    if (!(localStorage.getItem("oznacbe-group")) || (localStorage.getItem("oznacbe-group") === "null")) {
        $('[id$="group"]').val(null).trigger('change');
    }
    
    /*Reapply all the nonempty classes and listeners*/
    $('.group_dropdown_container [id$="group"]').on('change', function(e){
        if ($(this).val()){
            /*Add a white background to the selection box*/
            $(this).parent().find('#select2-id_group-container').addClass('nonempty');
            /*Remove teh + box*/
            $(this).parent().addClass('nonempty');
        } else {
            /*Remove the white background to the selection box*/
            $(this).parent().find('#select2-id_group-container').removeClass('nonempty');
            /*Add the + box*/
            $(this).parent().removeClass('nonempty');   
        }
    });

    $('[data-toggle="tooltip"]').bootstrapTooltip();


    $('dl.product input.productInput').each( function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $('#labelForm input').each( function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });


    $('dl.product input.productInput').on('change', function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $('#inputPortionSize').each( function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $('#inputPortionSize').on('change', function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    //$('#id_group.main-group-select').each(function(e){
    $('.group_dropdown_container [id$="group"]').each(function(e){
        if ($(this).val()){
            $(this).parent().find('#select2-id_group-container').addClass('nonempty');
            $(this).parent().addClass('nonempty');
        } else {
            $(this).parent().find('#select2-id_group-container').removeClass('nonempty');
            $(this).parent().removeClass('nonempty');   
        }
    });


    setupLongInputBoxes();
}


/**
 * Clears the select2 group dropdowns
 */
function clearDefaultGroupSelection(){
    //only clear when editing a product. Otherwise, we would delete the value sent from the server
    //productId is defined in add.html
    if(!productId) {
        $('[id$="group"]').val(null).trigger('change');
    }
}

function clearLabelData(){
    $('.addLabelInfoTable').find('input').val("")
    $('.addLabelInfoTable [id$="group"]').val(null).trigger('change');
}


/**
 * When editing a label, the extra components have "." instead of "," as a decimal seperator.
 * This needs to be manually fixed
 */
function fixComponentTableDecimalSeperators() {
    $(".label-table-input").each(function(){
        var value = $(this).val();
        value = value.replace(".",",");
        $(this).val(value);
    })
}


/**
 * Marks the form as 'dirty' display a message when trying to leave the page
 */
function dirtyForm() {
 $('form').addClass('dirty')
}