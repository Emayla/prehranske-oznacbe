/**
 * Contains all functions related to gathering up data of a product entered from label
 * and pushing it to the server
 *
 * @module add/upload-label
 */



/**
 * Finds and returns the weight of the salt component on the "add from label" interface
 * @param  {object} obj - The DOM object of the salt field
 * @return {object}       An object with the fields ["code", "weight"] representing the code and weight of
 *                        the salt component
 */
function gatherSalt(obj) {
    var ingInput = $(obj).find('input');
    
    //this is the salt input, which will be pushed to the API as the NA (natrium) component
    //since salt=2.5*NA, we need to divide this input by 2.5 to get the correct NA value
    var naWeight = parseFloat(ingInput.val().replace(',','.')) / 2.5;
    var oldUnits = $(obj).find('small span').text();
    naWeight = convertUnits(naWeight, oldUnits, 'g');
    
    var labelSubstringIndex= $(obj).attr('id').indexOf('Label');
    var code = $(obj).attr('id').substring(0,labelSubstringIndex);
    return { 'code': code, 'weight':naWeight.toString() };
}


/**
 * Finds and returns the weight of a  non-salt component on the "add from label" interface
 * @param  {object} obj - The DOM object of a component field
 * @return {object}       An object with the fields ["code", "weight"] representing the code and weight of
 *                        the component
 */
function gatherComponent(obj){
    var ingInput = $(obj).find('input');
    
    var labelSubstringIndex= $(obj).attr('id').indexOf('Label');
    var code = $(obj).attr('id').substring(0,labelSubstringIndex);
    
    //make sure to convert the units to grams
    var weight = ingInput.val().replace(',', '.');
    var oldUnits = $(obj).find('small span').text();
    weight = convertUnits(weight, oldUnits, 'g');

    return { 'code': code, 'weight': weight };
}

/**
 * Finds all component values entered by the user when adding an item from a label,
 * and collects them into an array ready to be sent to the server for processing
 * @return {array}  An array of component values in the form suitable for sending to the server
 */
function gatherComponents() {
    var reqComponents = [];
    var inputError = false;
    $('#listComponentsLabel tr').each(function () {
        var ingInput = $(this).find('input');
        if (ingInput['length'] == 0){
            //first row is just the 100g label, so it won't have an input - skip it
            return;
        }
        //all ids here are of the form componentLabel, to distinguish them from the normal component values
        //so to get the real code, we need to remove the "Label" part of the id
        var id = $(this).attr('id');
        //skip the final row (the one that allows users to add extra components)
        if (id=="addComponentLabel" || id=="addComponent"){
            return;
        }

        if (id=="naLabel"){
            reqComponents.push(gatherSalt(this));
            return;
        }

        reqComponents.push(gatherComponent(this));
    });
    return reqComponents;
}



/**
 * Finds all allergens entered into the add from label screen
 * and returns them a form suitable for uploading to the server
 */
function gatherAllergensLabel() {
    var reqAllergens = [];
    $('#listAllergensLabel tr').each(function () {
        reqAllergens.push(parseInt($(this).attr('id')));
    });
    return reqAllergens;
}


/**
 * Checks if a numeric input field contains an error
 *
 * returns true if the input isn't empty and contains characters that aren't numbers.
 * 
 * @param  {$object}  $object - a jQuery object (input field) to check
 * @return {boolean}           True if the numeric input has an error (if it contains characters that aren't numbers). 
 *                             False otherwise
 */
function hasNumericError($object){
    var object_value = $object.val().replace(',','.')
    return (!$.isNumeric(object_value) && $object.val().length > 0)
}


/**
 * Gathers all product data when the user is adding a new product from a label
 * @return {object} An object with fields prepresenting the product data, 
 *                  in the form ready to be uploaded to the server
 */
function gatherAllIngredientLabelData() {
    //replace , with . in the neto weight category
    //everything else is already handled elswhere
    $('#labelDiv #id_neto_weight').val($('#labelDiv #id_neto_weight').val().replace(',','.'));
    
    var data = $('#labelForm').serializeArray();
    var items = gatherComponents();
    var allergens = gatherAllergensLabel();
    data.push({name: 'diets', value:allergens});
    if (!items) return false;
    $.each(items, function (index, value) {
        $.each(value, function (k, v) {
            data.push({name: 'components[' +index+ '][' +k+ ']', value: v});
        });
    });

    //find the item producer, which isn't included in the form
    var producer_name = $("#producerInput").val();
    data.push ({name: 'producer', value: producer_name});
    
    //now that we have the data, perform input validation
    //first, clear any results of previous validation
    $('#labelDiv input').each(function(){
        $(this).parent().removeClass("error");
    });

    //check if required fields are empty
    var errors= false;
    $('#labelDiv input[required]').each(function(){
        if ($(this).val() == "") {
            $(this).parent().addClass("error");
            errors= true;
            $("#requiredErrorMessage").show();
        }
    });

    //check if numeric fields have only numbers
    //since the next two fields aren't required, only display the error if the user entered something into them
    //otherwise an empty input is treated as non-numeric and would give us an error
    if (hasNumericError($('#labelDiv #id_barcode'))){
        $('#labelDiv #id_barcode').parent().addClass("error");
        errors= true;
        $("#numericErrorMessage").show();
    }

    if (hasNumericError($('#labelDiv #id_neto_weight'))){
        $('#labelDiv #id_neto_weight').parent().addClass("error");
        errors= true;
        $("#numericErrorMessage").show();
    }

    $.each($('input.addLabelComponentsTableInput'), function () {
        if (hasNumericError($(this))){
            $(this).parent().addClass("error");
            errors= true;
            $("#numericErrorMessage").show();
        }
    });

    if (errors){
        return false;
    }
    return data;
}



/**
 * Gathers all product data when the user is editing a product added from a label or using the add_label interface
 * @return {object} An object with fields prepresenting the product data, 
 *                  in the form ready to be uploaded to the server
 */
function gatherAllIngredientLabelEdit() {
    //replace , with . in the neto weight category
    //everything else is already handled elswhere
    $('#recipeForm #id_neto_weight').val($('#recipeForm #id_neto_weight').val().replace(',','.'));
    
    var data = $('#recipeForm').serializeArray();
    var items = gatherComponents();
    var allergens = gatherAllergens();
    data.push({name: 'diets', value:allergens});
    if (!items) return false;
    $.each(items, function (index, value) {
        $.each(value, function (k, v) {
            data.push({name: 'components[' +index+ '][' +k+ ']', value: v});
        });
    });

    //find the item producer, which isn't included in the form
    var producer_name = $("#producerInput").val();
    data.push ({name: 'producer', value: producer_name});
    
    //now that we have the data, perform input validation
    //first, clear any results of previous validation
    $('#add-label-components-table input').parent().removeClass("error");
    $('#recipeFormData input').parent().removeClass("error");
    $(".group_dropdown_container").removeClass("error");
    $("#requiredErrorMessage").hide();
    $("#numericErrorMessage").hide();
    $("#componentsErrorMessage").hide();
    $("#groupErrorMessage").hide();


    //check if required fields are empty
    var errors= false;
    $('#recipeForm input[required]').each(function(){
        if ($(this).val() == "") {
            $(this).parent().addClass("error");
            errors= true;
            $("#requiredErrorMessage").show();
        }
    });

    //check if numeric fields have only numbers
    //since the next two fields aren't required, only display the error if the user entered something into them
    //otherwise an empty input is treated as non-numeric and would give us an error
    if (hasNumericError($('#recipeForm #id_barcode'))){
        $('#recipeForm #id_barcode').parent().addClass("error");
        errors= true;
        $("#numericErrorMessage").show();
    }

    if (hasNumericError($('#recipeForm #id_neto_weight'))){
        $('#recipeForm #id_neto_weight').parent().addClass("error");
        errors= true;
        $("#numericErrorMessage").show();
    }

    //these are required, so don't check for length > 0
    $.each($("#add-label-components-table .label-table-input"), function () {
        var value = $(this).val().replace(',','.');
        var isNumeric = $.isNumeric(value)
        if (!isNumeric){
            $(this).parent().addClass("error");
            errors= true;
            $("#componentsErrorMessage").show();
        }
    });

    //check product group
    var groupInData = (data.filter(function(x){ return x.name === "group" }).length) > 0;
    if (!groupInData){
        errors = true;
        $(".group_dropdown_container").addClass("error");
        $("#groupErrorMessage").show();
    }

    if (errors){
        return false;
    }

    return data;
}


/**
 * Collects all the data of a product on the "add from label" form and uploads it to the server
 */
function saveIngredientFromLabel(){
    $('#ajaxErrorMessage').hide();
    $('#requiredErrorMessage').hide();
    $('#numericErrorMessage').hide();
    preserveData = true;
    if (authenticated) {
        var oldText = $("#saveLabel-text").html();
        var productData = gatherAllIngredientLabelData();

        if (productData){
            $(this).attr('disabled', true);
            var oldText = $("#saveLabel-text").html();
            $("#saveLabel-text").html('Shranjujem');
             $.ajax({
                context: this,
                type: "POST",
                url: "submit_label/",
                data: productData,
                dataType: 'json',

                //after the product is added to the api, we also want to include it on this page
                //the ajax response returns the product's id and name, so that we can add it
                success: function (data) {
                    $('#listIngredients').addIngredient(data.product_code, data.product_name, 0);
                    $('#addLabelForm').hide();
                },

                error: function (data){
                    var keyTranslations = {
                        "group": "Skupina živila",
                        "neto_weight": "Neto količina",
                        "commercial_name": "Prodajno ime",
                        "name": "Naziv živila",
                        "country": "Država porekla",
                        "producer": "Nosilec dejavnosti",
                        "server": "Strežnik"
                    }

                    //In case of an error, we get back a json of the form
                    //{fieldname: [{message:'', code:''}]          
                    //first, iterate through all errors and display them as ul elements
                    var newErrorHTML="Pri shranjevanju je prišlo do sledečih napak: <ul>";
                    var jsonResponse = JSON.parse(data.responseText);
                    $.each(jsonResponse, function(key, value){
                        var translatedKey=keyTranslations[key];

                        //value is an array of objects
                        $.each(value, function(index, valueInner){
                            //Barcode error messages already mention they refer to the barcode field
                            //Such as: "Črtna koda ni ustrezna" instead of "To polje ni ustrezno",
                            //so we can just print the errors without checking what field they belong to 
                            if (key === "barcode" || key == "server"){
                                newErrorHTML+=`<li>${valueInner}</li>`;
                            //In other errors we need to include the field name
                            } else {
                                newErrorHTML+=`<li>${valueInner} - ${translatedKey}</li>`;
                            }
                        });
                    });
                    newErrorHTML+='</ul>';
                    $('#ajaxErrorMessage')[0].innerHTML = newErrorHTML;
                    $('#ajaxErrorMessage').show();
                    preserveData = false;
                    $('form').addClass("dirty")
                },

                complete: function (data){
                    $(this).attr('disabled', false);
                    $("#saveLabel-text").html(oldText);
                }
             });
        }
    } else {
        //scroll up to make it obvious the user needs to log in
        window.scrollTo(0,0);
        toggleLogin();
    }
}


/**
 * Collects all the data of a product on the "edit label" form and uploads it to the server
 */
function saveIngredientFromLabelEdit(){
    //Disable the warning for leaving the page
    $('#recipeForm').areYouSure( {'silent':true} );
    var productData = gatherAllIngredientLabelEdit();
    preserveData = true;
    $('form').removeClass('dirty')
    var path="submit_label/"
    var oldText = $("#saveProduct-text").html();
    if (productData){
        var parameters = productData;
        $(this).attr('disabled', true);
        $("#saveProduct-text").html('Shranjujem');
        parameters.push({name: 'csrfmiddlewaretoken', value: csrf_token});
        $.post(path, $.param(parameters))
            .done(function (response) {
            //set the previous page  so that we don't get a warning for not saving the data
            localStorage.clear();
            try {
                sessionStorage.setItem("prevPage", "submit", 0.1)
            } catch (error){
                //MDN: https://developer.mozilla.org/en-US/docs/Web/API/Storage/setItem#Exceptions
                console.log("Session storage isn't supported");
            }

            if (response.redirect){
                window.location.replace(response.redirect);

            }
        })
        .fail(function (response) {
            var keyTranslations = {
                "group": "Skupina živila",
                "neto_weight": "Neto količina",
                "commercial_name": "Prodajno ime",
                "name": "Naziv živila",
                "country": "Država porekla",
                "producer": "Nosilec dejavnosti",
                "server": "Strežnik"
            }

            //In case of an error, we get back a json of the form
            //{fieldname: [{message:'', code:''}]          
            //first, iterate through all errors and display them as ul elements
            var newErrorHTML="Pri shranjevanju je prišlo do sledečih napak: <ul>";
            var jsonResponse = JSON.parse(response.responseText);
            $.each(jsonResponse, function(key, value){
                var translatedKey=keyTranslations[key];

                //value is an array of objects
                $.each(value, function(index, valueInner){
                    //Barcode error messages already mention they refer to the barcode field
                    //Such as: "Črtna koda ni ustrezna" instead of "To polje ni ustrezno",
                    //so we can just print the errors without checking what field they belong to 
                    if (key === "barcode" || key == "server"){
                        newErrorHTML+=`<li>${valueInner}</li>`;
                    //In other errors we need to include the field name
                    } else {
                        newErrorHTML+=`<li>${valueInner} - ${translatedKey}</li>`;
                    }
                });
            });
            
            newErrorHTML+='</ul>';
            $('#ajaxErrorMessage')[0].innerHTML = newErrorHTML;
            $('#ajaxErrorMessage').show();

            $("#saveLabelEdit").attr('disabled', false);
            $("#saveProduct-text").html(oldText);
        });
    }
}



 /**
  * Inserts a new component into the label container table
  * @param {string} comp     - The ID of the component
  * @param {string} label    - The name of the component
  * @param {number} before   - The ID of the component the new component should be inserted after
  *                            (For example, Vitamin C should come after Vitamin A, of after Salt if
  *                            Vitamin A doesn't exist in the table yet).
  */
function addLabelComponent(comp, label, before) {
    var code = comp['code'].toLowerCase();
    var elementId = code + 'Label'
    var newComp = '<tr id="' + elementId + '"><th>';
    var hidden = "";
    if (!unitChangeComponents.includes(comp.code)) {
        hidden = "hidden"
    }
    
    var unitChangeHTML = `<span class=float-right><span class="unit-change ${hidden}"> 
                            <small class="product-table-unit-select" data-placement="top"
                            data-popover-content="#${code}Label-unit-popover" data-toggle="popover"
                            data-trigger="focus" tabindex="-1">
                              (<span id="${code}Label-unit">${comp['def_unit']}</span>)
                            </small>`

    var units =""
    $.each(comp['units'], function (ind, unit) {
        units += '<li>' + unit + '</li>';
    });
    var unitsHTML = `<div class="hidden" id="${code}Label-unit-popover"> <div class="popover-body"> <ul id="${code}Label-units"> ${units} </ul></div></div></span>`
    
    if (comp['child']) newComp += '- od tega ' + label.toLowerCase();
    else newComp += label;

    //newComp += '<span class="floatRight">(g)</span>';
    newComp += unitChangeHTML + unitsHTML;


    newComp += `<img src='${removeImgUrl}' aria-hidden='true' class='removeComponent'></span></th>` ;

    /*editingLabel is a global variable passed from the server
      that tells us if the user is currently trying to edit an
      existing product (added from a label), instead of entering a new one

      If this is the case, the editing interface uses a different interface then
      the one used for adding products (it uses the same "main") table
      that is used when viewing product information

      The only real difference here is that the style for the newly added row
      needs to be changed to match the overall look of the table*/
    if (editingLabel){
        newComp += `<td> <input type = "text" id="${code}-label-input" class ="numericInput label-table-input extra-margin" required/>`;
    } else {
        newComp += `<td> <input type = "text" id="${code}-label-input" class ="addLabelComponentsTableInput numericInput label-table-input extra-margin" required/>`;
    }

    var unitDisplay = `<label id="${code}-unit-label" class = "label-table-units" for="${code}-label-input"> ${comp.def_unit} </label>`
    var closingTags = "</td></tr>";
    newComp += unitDisplay; 
    newComp += closingTags; 

    $('#'+before).after(newComp);
    $('#' + code + 'Label [data-toggle="popover"]').initializePopover();
    return this;
}


/**
 * Sets up event listeners needed to get the label upload working 
 */
function setupLabelUploadListeners(){
    $('#saveIngredientFromLabel').on('click', saveIngredientFromLabel);
    $('#saveLabelEdit')          .on('click', saveIngredientFromLabelEdit);
}