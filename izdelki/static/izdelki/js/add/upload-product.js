/**
 * Contains all functions related to gathering up product data and pushing this data to the server
 *
 * @module add/upload
 */

/**
 * Tracks what components the user added to the component table
 */
var extraComponents = [];

//Should data in localstorage be kept between page reloads
var preserveData = false;

/**
 * Check if the user forgot to enter any ingredients
 * @return {boolean}   True if there are no entered ingredients.
 *                     False otherwise
 */
function noIngredients(){
    return ($("#listIngredients tr").length == 0)
}


/**
 * Send the product data to the server
 *
 * If the server returns an error, this function also displays the correct error messages
 * @param  {string} path       - The server url to send the data to
 * @param  {object} parameters - The data to send
 */
function post(path, parameters) {
    //Disable the warning for leaving the page
    $('form').areYouSure( {'silent':true} );
    $('form').hasClass('dirty')
    parameters.push({name: 'csrfmiddlewaretoken', value: csrf_token});
    $('#error-server').hide();
    $.post(path, $.param(parameters))
        .done(function (response) {
            //set the previous page cookie so that we don't get a warning for not saving the data
            //setCookie("prevPage", "submit", 0.1)
            try {
                sessionStorage.setItem("prevPage", "submit", 0.1)
            } catch (error){
                //MDN: https://developer.mozilla.org/en-US/docs/Web/API/Storage/setItem#Exceptions
                console.log("Session storage isn't supported");
            }

            if (path == 'submit/'){
                //Clear currently stored from data after a sucessfull submit.
                localStorage.clear();
            }
            if (response.redirect)
            window.location.replace(response.redirect);
        })
        .fail(function (response) {
            //400 means the response was processed, but there was something wrong with the data
            //As a reply, we get the HTML code that should replace the product form, with the
            //incorrect fields marked and instructions on how to fix them
            if (response.status == 400){
                $('#recipeForm').replaceWith(response.responseText);
            //Other error codes means something went wrong in the server. Disaply a generic error message instead
            } else {
                $('#error-server').show();
            }

            //after replaceWith call, we need to rerun some javascript functions
            //that don't properly get applied on the replaceWith call
            //in common
            initSidebar();
            $('#saveProduct').attr('disabled', false);
            $('#saveProduct-text').html('Shrani');
            preserveData = false;
            $('form').addClass("dirty")
        });
}


function saveProduct(){
    preserveData = true;
    $("#error-submit-portion").hide();
    $("#error-submit-input").hide();
    /*
    refresh the timestamp
    we don't want the data to expire between clicking save and the user logging in
    */
    saveTimestamp();
    $("#error-no-ingredients").hide();
    if (authenticated) {
        //show a warning if the user didn't add any ingredients to the product
        if (noIngredients()){
            $("#error-no-ingredients").show();
            return;
        }
        $(this).attr('disabled', true);
        $("#saveProduct-text").html("Shranjujem");
        
        var productData = gatherAllProductData();
        /*
        if(!portionSize){
            portionSize=0;
        }*
        /*
        if (portionSize === 0 || !portionSize) {
            console.log('You forgot to input meal size!');
            //alert('Prosimo, vnesite pravilno velikost porcije.');
            $("#error-submit-portion").show();
            $(this).attr('disabled', false);
        */
        if (productData){
            /*
            var meal_size = productData.find(function(obj){
                return obj.name==="meal_size"
            });

            if (meal_size === undefined) {
                $("#error-submit-portion").show();
                $(this).attr('disabled', false);
                $("#saveProduct-text").html("Shrani");
                return;
            }
            */
            //also upload the components the user added to the component table
            productData.push({name:"ExtraComponents", value: extraComponents});
            $("#error-submit-portion").hide();
            $("#error-submit-input").hide();
            post('submit/', productData);
        }
        else {
            //console.log('You have an error in one of the inputs!');
            //alert('V enem izmed vaših vnosnih polj se nahaja napaka.');
            $("#error-submit-input").show()
            $(this).attr('disabled', false);
            $("#saveProduct-text").html("Shrani");
        }
    } else { 
        toggleLogin();
    }
}