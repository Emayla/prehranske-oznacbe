/**
 * Contains all functions related to saving and loading data from localstorage
 *
 * All data the user enters when adding or editing a product is stored client side using
 * javascript localstorage. If the user ever leaves the page (for example, to log in), his data isn't lost.
 *
 * When the user reenters the page, the data is loaded back in.
 *
 * The stored data is cleared when:
 *     - A certain amount of time has expired
 *
 * @module add/localstorage
 */


//expiration time of saved data, in minutes
var EXPIRATION_TIME_IN_MINUTES = 120;
//some functions want miliseconds instead
var EXPIRATION_TIME_IN_MS = EXPIRATION_TIME_IN_MINUTES * 60 * 1000


/**
 * Restores product data saved in localstorage
 *
 * When the user leaves the add/edit page without saving his data,
 * the data is stored in javasctipt localstorage.
 *
 * This allows us to restore the data when the user comes back to the add screen
 */
function restoreData(){
    checkExpiry(EXPIRATION_TIME_IN_MS);

    /*check what item we are editing
    If we want to save data for edits too, we need to remember for which item we are saving the data,
    so that if the user stops editing the current item and goes to edit a different one, we don't save the data of the first one */
    var address = window.location.href;
    var currentItem;
    if (address.match("edit_label")){
        currentItem = address.match("edit_label/(.+)")[1];
    } else if (address.match("edit")){
        currentItem = address.match("edit/(.+)")[1];
    } else {
        currentItem = "new";
    }

    //Should we load data from the javascript localstorage or not
    //This should be set to false the first time the user edits a product
    //because we don't wan't to overwrite the product data from the server
    //with out local product informations
    var useLocalStorage= true;

    //first, we check if the user is trying to add/edit a new product,
    //or if he is returning to an existing one (and we need to load his saved data)
    if (!localStorage['oznacbe-current-item']){
        //This is the first time the user is viewing any product
        //If the server sent us data about the product, we need to fill it in
        fillCurrentProductData(productIngredients, productAllergens, productMethod, productServing, productMealsize);
        if (productEdit){
            //We want to use the data from the server, not our local one
            saveAllItems();
            useLocalStorage=false;
        } else {
            //The user is adding a new product for the first time:
            //set the default barcode value
            localStorage.setItem("oznacbe-barcode", barcode_prefix);
        }


    } else {
        //The user is returning to the add screen after leaving it
        //check to see if he is coming back to an item he was adding before, 
        //or if he went to add/edit something else.
        //
        //If it's a different item, we need to clear our current data, then do the same as above
        //Else, we don't need to do anything other than load the data later in the function
        if (localStorage['oznacbe-current-item'] != currentItem){
            localStorage.clear();
            
            fillCurrentProductData(productIngredients, productAllergens, productMethod, productServing, productMealsize);

            if (productEdit){
                saveAllItems();
                useLocalStorage=false;
            } else {
                localStorage.setItem("oznacbe-barcode", barcode_prefix);
            }
        } 
    }
    //now mark the current item
    localStorage['oznacbe-current-item'] = currentItem;

    /*
    This reloads the form data that is saved in localStorage

    It is only triggered when adding new items, not updating existing ones.
    This is because the saving code conflicts with the editing code. They will both load ingredients, 
    which will cause them to show up multiple times
    */
    if (useLocalStorage){
        restoreItems();
    }
}



/**
 * Gets the current time and saves it to localstorage.
 *
 * This is used when checking if the saved data has expired.
 * We don't wan't to load data that is several days old, 
 * since it probably isn't relevant anymore
 */
function saveTimestamp(){
    var current_date = new Date();
    localStorage['timestamp'] = JSON.stringify(current_date);
}


/**
 * Checks the saved data for expiry and deletes it if it's too old
 *
 * The function compares the current time with the time when the data was saved.
 * If the difference is larger than expirationTimeInMS, the data is deleted.
 * @param  {number} expirationTimeInMS - How long do we store the data before deleting it.
 */
function checkExpiry(expirationTimeInMS){
    var current_date = new Date();
    if (!localStorage['timestamp']){
        saveTimestamp();
        return;
    }

    var saved_date = new Date(JSON.parse(localStorage['timestamp']));

    var time_difference = current_date-saved_date;
    if (time_difference > expirationTimeInMS) {
        localStorage.clear();
    } else {
        /*
        if data is not expired, refresh the timestamp
        we don't wan't the user to come back 10 minutes before expiry and see his data
        then go to log in for 11 minutes and lose all his data when he comes back
        */
        saveTimestamp();
    }
}


/**
 * Saves all currently entered ingredients to HTML session storage
 * Supports IE8+, Firefox 2+, Chrome 5+
 * They are cleared after submitting the form or on browser exit
 */
function saveIngredients() {
    saveTimestamp();
    var reqIngredients = [];
    $('#listIngredients tr').each(function (i) {
        var ingInput = $(this).find('input');
        var itemCode = $(this).attr('id');
        localStorage.setItem("oznacbe-ing-code-"+i, $(this).attr('id'));
        localStorage.setItem("oznacbe-ing-weight-"+i, ingInput.val());
        localStorage.setItem("oznacbe-ing-name-"+i, $(this).children("td:first").text());
        localStorage.setItem("oznacbe-ing-tooltip-"+i, $(this).children("td:first").attr('data-original-title'));
    });

}



/**
 * Deletes the specified ingredient from localstorage
 * @param  {string} name - The name of the ingredient to remove
 */
function removeSavedIngredients(name){
    var sessionValues = Object.keys(localStorage).reduce(function(obj, str) { 
                    obj[str] = localStorage.getItem(str); 
                    return obj
                    }, {});
    jQuery.each( sessionValues, function( key, value) {
        if (key.match("^oznacbe-ing-code")){
            var item_number=key.match("^oznacbe-ing-code-(\\d+)")[1];
            localStorage.removeItem("oznacbe-ing-code-" + [item_number]);
            localStorage.removeItem("oznacbe-ing-weight-" + [item_number]);
            localStorage.removeItem("oznacbe-ing-name-" + [item_number]);
            localStorage.removeItem("oznacbe-ing-tooltip-" + [item_number]);
        }
    });

}


/**
 * Saves all currently entered allergens to HTML local storage
 * Supports IE8+, Firefox 2+, Chrome 5+
 * They are cleared after submitting the form or on browser exit
 */
function saveAllergens() {
    saveTimestamp();
    $('#listAllergens tr').each(function (i) {
        localStorage.setItem("oznacbe-alg-code-"+i, $(this).attr('id'));
        localStorage.setItem("oznacbe-alg-name-"+i, $(this).children("td:first").text());
    });
} 


/**
 * Clear all saved allergens from localstorage
 */
function removeSavedAllergens(name){
    var sessionValues = Object.keys(localStorage).reduce(function(obj, str) { 
                    obj[str] = localStorage.getItem(str); 
                    return obj
                    }, {});
    jQuery.each( sessionValues, function( key, value) {
        if (key.match("^oznacbe-alg-code")){
            var item_number=key.match("^oznacbe-alg-code-(\\d+)")[1];
            localStorage.removeItem("oznacbe-alg-code-" + [item_number]);
            localStorage.removeItem("oznacbe-alg-name-" + [item_number]);
        }
    });

}


/**
 * Restores the provided allergen from localstorage to the product page 
 * @param  {string} key           - The localstorage key of the allergen
 * @param  {string} value         - The localstorage value of the allergen
 * @param  {object} sessionValues - An object containg all localstorage key/value pairs 
 */
function restoreAllergen(key, value, sessionValues){
    //regex to find the item number - the last number in the key name
    var allg=Object();
    var item_number=key.match("^oznacbe-alg-code-(\\d+)")[1];
    allg['CODE']    = value;
    allg['NAME']    = sessionValues["oznacbe-alg-name-" + item_number];
    $('#addAllergens').find('[data-id="'+ allg['CODE'] + '"]').hide();
    $('#listAllergens').addAllergen(allg['CODE'], allg['NAME']);
}


/**
 * Saves the currently selected cooking method and weight to localstorage
 */
function saveMethod(){
    saveTimestamp();
     $('#listMethod tr').each(function (){
        var input = $(this).find('input');
        var itemCode = $(this).attr('id');
        localStorage.setItem("oznacbe-meth-code", $(this).attr('id'));
        localStorage.setItem("oznacbe-meth-name", $(this).children("td:first").text());
        localStorage.setItem("oznacbe-meth-weight", input.val());
     });
}


/**
 * Delete stored method information from localstorage
 */
function removeSavedMethod(){
    localStorage.removeItem("oznacbe-meth-code");
    localStorage.removeItem("oznacbe-meth-name");
    localStorage.removeItem("oznacbe-meth-weight");
}


/**
 * Restores the cooking method from localstorage to the product page 
 * @param  {object} sessionValues - An object containg all localstorage key/value pairs 
 */
function restoreMethod(sessionValues){
    var mval = sessionValues["oznacbe-meth-code"];
    var mlabel = sessionValues["oznacbe-meth-name"];
    var serving = sessionValues["oznacbe-meth-weight"];
    $('#chooseMethod').find('[data-id="'+ mval + '"]').parent().parent().hide().prev().hide();
    $('#listMethod').setMethod(mval, mlabel.toLowerCase(), serving);   
}


/**
 * Saves the component weight informations on the add_label/edit_label screen
 */
function saveComponents(){
    $("#listComponentsLabel input").each(function(){
        var id = this.id;
        var value = $(this).val();
        
        localStorage.setItem(id, value);
        if (value){
            var units = $(this).siblings().text().trim();
            localStorage.setItem(id + "-units", units);
        }
    });
}

/**
 * Restores the component weight informations on the add_label/edit_label screen
 */
function restoreComponentsLocalstorage(){
    $("#listComponentsLabel input").each(function(){
        var id = this.id;
        var value = localStorage.getItem(id);
        if (value) {
            var storedUnits = localStorage.getItem(id + "-units");
            var currentUnits = $(this).siblings().text().trim();
            if(storedUnits!=currentUnits){

                //we need to make sure that the units displayed now match the ones that were used when the product was saved
                //The easiest way to do this with the current code is to first convert back to base units
                value = convertUnits(value, storedUnits, currentUnits);
                $(this).val(value);
                //then simulate clicking on the unit change button to change to correct units
                
                var unitChangeButton = $(this).parent().siblings().find("li:contains(" + storedUnits + ")")
                unitChangeButton.trigger("click");
            } else {
                $(this).val(value);
            }
            //Force an update from the error and nonempty listeners
            $(this).trigger('input');
            $(this).trigger('change');
        }
    });
}


/**
 * Saves all other inputs on the form to localstorage. 
 * This includes the sidebar and portion size
 */
function saveInputs(){
    saveTimestamp();
    localStorage.setItem("oznacbe-portion-size", $('#inputPortionSize').val());

    var name_val = $("#recipeForm #id_commercial_name").val();
    /* When the user isn't logged in, inputs other than portion-size don't exist,
       and their values are undefined. In that case, we have to stop saving, otherwise all other values 
       get saved as a string "undefined"
    */
    if (name_val == undefined) {
        return;
    }

    localStorage.setItem("oznacbe-commercial-name", $("#recipeForm #id_commercial_name").val());
    localStorage.setItem("oznacbe-name", $("#recipeForm  #id_name").val());
    localStorage.setItem("oznacbe-country", $("#recipeForm #id_country").val());
    localStorage.setItem("oznacbe-weight", $("#recipeForm #id_neto_weight").val());
    localStorage.setItem("oznacbe-barcode", $("#recipeForm #id_barcode").val());
    localStorage.setItem("oznacbe-group", $('#recipeForm select[name=group]').val());
    localStorage.setItem("oznacbe-producer", $('#recipeForm #producerInput').val());
    
}

/**
 * Loads the sidebar fields from localstorage onto the page
 */
function restoreInputs(){
    /*
    Generally, we can just load the input even if nothing is saved
    If we do that, we just load in an empty string, so the form remains empty

    There are some inputs where we can't do this:
    1. The commercial name. Since this input is required, loading an empty screen will make the browser warn us that we need to fill it in
       which gives a red border around the input
    2. The group, since it already starts with a default value that we don't want to overwrite
    3. The barcode, since it also has a defualt value

    Because of this, for these inputs, we first check if we have anything saved
    */
   
    var formContainer = "#product-sidebar"

    if (localStorage.getItem("oznacbe-commercial-name")){
        $("#recipeForm #id_commercial_name").val(localStorage.getItem("oznacbe-commercial-name"));
    }

    $("#recipeForm #id_name").val(localStorage.getItem("oznacbe-name"));
    $("#recipeForm #id_country").val(localStorage.getItem("oznacbe-country"));
    $("#recipeForm #id_neto_weight").val(localStorage.getItem("oznacbe-weight"));
    
    if (localStorage.getItem("oznacbe-barcode") && localStorage.getItem("oznacbe-barcode")!="undefined"){
        $("#recipeForm #id_barcode").val(localStorage.getItem("oznacbe-barcode"));
    }
    
    
    if (localStorage.getItem("oznacbe-portion-size")){
        $("#inputPortionSize").val(localStorage.getItem("oznacbe-portion-size"));
    }

    if (localStorage.getItem("oznacbe-group")){
        $('#recipeForm select[name=group]').val(localStorage.getItem("oznacbe-group"));
        //the dropdown needs this command to trigger a refresh and display the correct value
        //because we call the change method, this needs to be on the bottom
        //since the change method causes a refresh of all stored components
        //i this doesn't execute last, then we will overwrite the intended component values 
        //with the curent ones (ussually 0) before we can load the intended one
        $('#recipeForm select[name=group]').trigger("change")
    } else {
        //set it to an empty value
        $('[id$="group"]').val(null).trigger('change');
    }

    if (localStorage.getItem("oznacbe-producer") && localStorage.getItem("oznacbe-producer")!="undefined"){
        $('#recipeForm #producerInput').val(localStorage.getItem("oznacbe-producer"));
    }
}


/**
 * Save the current photo to localstorage
 */
function savePhoto(){
   saveTimestamp();
   localStorage.setItem("oznacbe-photo", selectedPhoto);
}

/**
 * Delete the saved photo from localstorage
 */
function clearPhoto(){
    selectedPhoto="";
    localStorage.removeItem("oznacbe-photo");
}

/**
 * Load the photo from localstorage
 */
function restorePhoto(){
    selectedPhoto = localStorage.getItem('oznacbe-photo');
    $('#product-img').css({'background-image': 'url(' + selectedPhoto + ')', 'background-size': 'contain'}).addClass('has-image');
}



/**
 * Saves all data on the page into localstorage
 */
function saveAllItems(){
    saveIngredients();
    saveInputs();
    saveAllergens();
    saveMethod();
    savePhoto();
    saveComponents();
}


/**
 * Restores the provided ingredient from localstorage to the product page 
 * @param  {string} key           - The localstorage key of the ingredient
 * @param  {string} value         - The localstorage value of the ingredient
 * @param  {object} sessionValues - An object containg all localstorage key/value pairs 
 */
function restoreIngredient(key, value, sessionValues){
    //regex to find the item number - the last number in the key name
    var ing=Object();
    var item_number=key.match("^oznacbe-ing-code-(\\d+)")[1];
    ing['CODE']    = value;
    ing['NAME']    = sessionValues["oznacbe-ing-name-" + item_number];
    ing['WEIGHT']  = sessionValues["oznacbe-ing-weight-" + item_number];
    ing['tooltip'] = sessionValues["oznacbe-ing-tooltip-" + item_number];
    $('#listIngredients').addIngredient(ing['CODE'], ing['NAME'], ing['WEIGHT'], ing['tooltip']);
}



/**
 * Loads all data from localstorage to the page fields
 */
function restoreItems() {
    //transform all localStorage items into one object
    //you can then iterate through the object with .each
    var sessionValues = Object.keys(localStorage).reduce(function(obj, str) { 
                    obj[str] = localStorage.getItem(str); 
                    return obj
                    }, {});

    var ingredients = []
    var allergens    = []

    /*
    localStorage can only store strings
    So go through each key and use the name of the key to determine what kind of data it has
    Keys have the following values:
    oznacbe-ing-code-(number)    : code of the ingredient number (number)
    oznacbe-ing-name-(number)    : name of the ingredient number (number)
    oznacbe-ing-weight(number)   : weight of the ingredient number (number)
    oznacbe-alg-code-(number)    : code of the allergen number (number)
    oznacbe-alg-name-(number)    : name of the allergen number (number)
    */
    jQuery.each( sessionValues, function( key, value) {
        
        //Key is an ingredient code
        if (key.match("^oznacbe-ing-code")) {
            restoreIngredient(key, value, sessionValues);
        }

        //key is an allergen
        if (key.match("^oznacbe-alg-code")) {
            restoreAllergen(key, value, sessionValues);
        }
    });
    
    //load cooking method
    if (sessionValues["oznacbe-meth-code"]){
        restoreMethod(sessionValues);
    }

    //load inputs
    restoreInputs();

    //load photo
    if (localStorage.getItem('oznacbe-photo')){
        restorePhoto();
    }

    restoreComponentsLocalstorage();

    //force refresh of newly added items
    requestComponentValues();
}



/**
 * Fills the current product information with the provided data
 * @param  {object} ingredients - The ingredients of the product
 * @param  {object} allergens   - The allergens of the product
 * @param  {object} method      - The cooking methid of the product
 * @param  {number} serving     - The weight of the product after cooking
 * @param  {number} mealsize    - The portion size of the product
 */
function fillCurrentProductData(ingredients, allergens, method, serving, mealsize) {
    // method
    if (method){
        var mval = method['CODE'];
        var mlabel = method['NAME'];
        $('#chooseMethod').find('[data-id="'+ mval + '"]').parent().parent().hide().prev().hide();
        $('#listMethod').setMethod(mval, mlabel.toLowerCase(), serving);
    }

    // allergens
    $.each(allergens, function (index, obj) {
       $('#addAllergens').find('[data-id="'+ obj['CODE'] + '"]').hide();
       $('#listAllergens').addAllergen(obj['CODE'], obj['NAME']);
    });

    // ingredients
    $.each(ingredients, function (index, obj) {
        $('#listIngredients').addIngredient(obj['CODE'], obj['NAME'], obj['WEIGHT'], obj['TOOLTIP']);
    });

    if (mealsize) {
        $('#inputPortionSize').val(mealsize);
    }

    //Since we added new ingredients, we need to recalculate component values
    requestComponentValues();
}

/**
 * Sets up event listeners needed to get localstorage functionality working 
 */
function setupLocalstorageListeners(){
    /*Saving data to localstorage*/
    $( "body" ).on( "change", "#recipeForm .productInput", saveInputs);
    $( "body" ).on( "change", "#inputPortionSize", saveInputs);
    $( "body" ).on( "change", "#listComponentsLabel input", saveComponents);
}