/**
 * Contains all functions that are called when the add/edit page loads
 * and are used to setup javascript functionality
 *
 * @module add/setup
 */

/**
 * Sets up the product photo picker event listeners
 */
function setupImageListeners(){
    /*Photo Picker*/
    $('#product-img').on('click', function() {
        $('#pick-photo').trigger('click');
    });
    $('#pick-photo').on('change', function() {
        var input = this;
        $('#product-img').removeClass('has-image');
        $('#photoError').attr('hidden', 'true');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var file = input.files[0];
            var extension = input.files[0].name.split('.').pop().toLowerCase();
            var fileTypeOK = ALLOWED_PHOTO_FILE_TYPES.indexOf(extension) > -1;
            var sizeOK = file.size < ALLOWED_PHOTO_FILE_SIZE;
            if (fileTypeOK && sizeOK) {
                reader.onload = function (e) {
                    selectedPhoto = e.target.result;
                    savePhoto();
                    $('#product-img').css({'background-image': 'url(' + e.target.result + ')', 'background-size': 'contain'}).addClass('has-image');
                };

                reader.readAsDataURL(input.files[0]);
            } else {
                console.log('INCORRECT FILE TYPE');
                clearPhoto();
                $('#product-img').css({'background-image':''});
                $('#photoError').removeAttr('hidden');
            }
        }
    });

    $("#remove-product-photo").on('click', function(event) {
        /*Prevent event propagation to stop the click also triggerring the photo select screen*/
        event.stopPropagation();
        selectedPhoto = '';
        savePhoto();
        $('#product-img').removeAttr("style").removeClass('has-image');

        /*Editing a product with a photo works differently*/
        if ($(this).parent().hasClass('has-image-detail')) {
            $(this).siblings('img').hide();
            $('#product-img').removeAttr("style").removeClass('has-image-detail');  
            $('#product-img').append('<img src=' + addImgUrl + '>');
        }

    });

}

/**
 * Sets up listeners for functions related to adding and removing
 * ingredients, allergens and cooking method.
 */
function setupIngredientAllergenMethodListeners(){
    /*Adding components, allergent, etc.*/
    $('table#listAllergens').on('click', 'tr > td > img', function () {
        var allergen = $(this).parent().parent().attr('id');
        $('#addAllergens').find('[data-id="'+ allergen + '"]').show();
        $(this).parent().parent().remove();
        removeSavedAllergens();
        saveAllergens();
        dirtyForm();
    });

    /*Removing components*/
    $('table#listComponents').on('click', '.removeComponent', function(){
        var od_tega = "- od tega ";
        var component = $(this).parents("th")[0].childNodes[0].textContent;
        var componentId = $(this).parents("tr")[0].id;
        removeElementFromArray (extraComponents, componentId.toUpperCase());
        //cut out the "- od tega" part some components have
        component = component.replace(new RegExp("^" + od_tega), '');
        //strings that were cut above will also need to have the first letter capitalized
        //we can just capialize every string
        component =  component.charAt(0).toUpperCase() + component.slice(1);
        
        $(this).parents("tr").remove();
        var sourceArray = $('#componentsInput').autocomplete('option', 'source');
        sourceArray.push(component);
        $('#componentsInput').autocomplete('option', 'source', sourceArray);
    });

    $('table#listComponentsLabel').on('click', '.removeComponent', function(){
        var od_tega = "- od tega ";
        var component = $(this).parents("th")[0].childNodes[0].textContent;
        //cut out the "- od tega" part some components have
        component = component.replace(new RegExp("^" + od_tega), '');
        //strings that were cut above will also need to have the first letter capitalized
        //we can just capialize every string
        component =  component.charAt(0).toUpperCase() + component.slice(1);
        
        $(this).parents("tr").remove();
        var sourceArray = $('#componentsInputLabel').autocomplete('option', 'source');
        sourceArray.push(component);
        $('#componentsInputLabel').autocomplete('option', 'source', sourceArray);
    });

    $('table#listAllergensLabel').on('click', 'tr > td > img', function () {
        var allergen = $(this).parent().parent().attr('id');
        $('#addAllergensLabel').find('[data-id="'+ allergen + '"]').show();
        $(this).parent().parent().remove();
    });


    $('table#listMethod').on('click', 'tr > td > img', function () {
        $(this).parent().parent().parent().parent().prev().show().prev().show();
        $(this).parent().parent().prev().remove();
        $(this).parent().parent().remove();
        requestComponentValues();
        removeSavedMethod();
    });

    $('ul.dropdown-menu.dropdown-add#addAllergens > li').on('click', function () {
        var val = $(this).data('id');
        var label = $(this).text();
        $(this).hide();
        $('#listAllergens').addAllergen(val, label);
        saveAllergens();
        dirtyForm();
    });

    $('ul.dropdown-menu.dropdown-add#addAllergensLabel > li').on('click', function () {
        var val = $(this).data('id');
        var label = $(this).text();
        $(this).hide();
        $('#listAllergensLabel').addAllergen(val, label);
        dirtyForm();
    });

    $('ul.dropdown-menu.dropdown-add#chooseMethod > li').on('click', function () {
        var val = $(this).data('id');
        var label = $(this).text();
        $(this).parent().parent().hide().prev().hide();
        $('#listMethod').setMethod(val, label);
        $('#listMethod input.inputLikeTd').select();
        saveMethod();
        requestComponentValues();
        dirtyForm();
    });

    $(document).on('input', 'td.weight > input', function () {
        if ($.isNumeric($(this).val().replace(',','.'))) {
            $(this).removeClass('error');
            requestComponentValues();
            saveIngredients();
            saveMethod();
        } else {
            $(this).addClass('error');
        }
    });

    $(document.body).on('change', '#cook_method, select[name=group], input[name=serv_size]', function () {
        requestComponentValues();
    });


    $('table#listIngredients').on('click', 'tr > td > img', function () {
        $(this).parent().parent().remove();
        removeSavedIngredients();
        saveIngredients();
        requestComponentValues();
    });

    $('input.numericInput').on('input', function () {
        var val = $(this).val().replace(',','.');
         if ($.isNumeric(val)) {
             $(this).removeClass('error');
             if ($(this).attr('name') === 'meal_size')
                portionSize = parseFloat(val);
             refreshComponents();
         } else {
             $(this).addClass('error');
         }
    });

}

/**
 * Sets up listeners related to deleting an item from the database
 */
function setupDeleteItemListeners() {

    /*Deleting an item*/
    $('.edit-delete-item').on('click',function(){
        var pathname = window.location.pathname;
        //pathname will be /edit/ID/
        //split gives us ["","edit","ID",""], so we need the third one
        item_to_be_deleted = pathname.split("/")[2];
        $('#deleteItemModal').modal();
    });

    $('#editDeleteItemConfirm').click(function(){
        $('#deleteError').hide();
        $('#deleteLabelInredientError').hide();
        
        if (item_to_be_deleted.substring(0,3) === LABEL_INGREDIENT_PREFIX){
            $('#deleteLabelInredientError').show();
            $('#deleteItemModal').modal('hide');
            return;
        }
        parameters =  [];
        parameters.push({name: 'csrfmiddlewaretoken', value: csrf_token})
        parameters.push({name: 'product_id', value: item_to_be_deleted})
        $(this).text("Brisanje poteka ...")
        $(this).attr('disabled', true);
        //$('#deleteItemModal').modal('hide');
        
        $.post('/delete/', $.param(parameters))
            .done(function (response) {
                //set the previous page so that we don't get a warning for not saving the data
                try {
                    sessionStorage.setItem("prevPage", "submit", 0.1)
                } catch (error){
                    //MDN: https://developer.mozilla.org/en-US/docs/Web/API/Storage/setItem#Exceptions
                    console.log("Session storage isn't supported");
                }
                //Redirect back to dashboard
                window.location.replace(response.redirect);
            })
            .fail(function (response) {
                $('#deleteError').show();
                $('#deleteItemModal').modal('hide');
            });
        item_to_be_deleted = "";
    });
}

/**
 * Sets up event listeners for reseting values entered in
 * the add product interface.
 */
function setupResetListeners(){
    $('#resetIngredient').on('click', function(){
        $('#labelResetModal').modal();
        return false;
    });
    $('#labelResetButton').on('click', function(){
        $('#labelForm').trigger('reset');
        $('#labelResetModal').modal('hide');
    });
    $('#reset-product img').on('click', function(){
        $('#product-reset-modal').modal();
        return false;
    });

    $('#help-product span').on('click', function(){
        $('#product-help-modal').modal();
        return false;
    });
    
    $('#product-reset-button').on('click', function(){
        //To clear the entire product screen, we can remove all localstorage data and then reload
        //That should give us a clean screen
        $('#recipeForm').trigger('reset');
        $('#inputPortionSize').val(0);
        localStorage.clear();
        window.location.reload(true);
        $('#product-reset-modal').modal('hide');
    });
}


/**
 * Creates and sets up the ingredient searchbar listeners
 */
function setupIngredientSearchWidget(){
    var searchWidget = $('#ingredientSearchbox').ingredientsearch();
    $('#addDatabase').on( 'click', function() {
        searchWidget.ingredientsearch('reset');

        //$('#addDatabase').toggleClass('add-product-focus');
        //toggleAddButton($('#addDatabase'));

        //$('#addLabel').removeClass('add-product-focus');
        //clearAddButton($('#addLabel'));
        $('#searchIngredients').show();
        $('#addLabelForm:visible').slideToggle();
        $('.searchIngredients-input').focus();
    });

    $('#addLabel').on('click', function() {
        $('#addLabelForm').slideDown();
        $('#searchIngredients').hide();
        //$('#addLabel').toggleClass('add-product-focus');
        //toggleAddButton($('#addLabel'));
        //$('#addDatabase').removeClass('add-product-focus');
        //clearAddButton($('#addDatabase'));
        $('#fatLabel input').focus();
    });

    $('#ingredientSearchbox').on('change', function (e, data) {
        ALLOWED_NAME_LEN = 50
        name = data.label
        if (name.length > ALLOWED_NAME_LEN) {
            name = name.substring(0,ALLOWED_NAME_LEN) + "..."
        }
        $('#listIngredients').addIngredient(data.val, name, 0, data.tooltip);
        $('select[name=ingredients]').val(null);
        
        //focus on the newly added element
        //this defaults to the last element matched, which will be the newest one
        //focus just focuses(), while select() also clears all existing text in input
        $('#listIngredients input.inputLikeTd').select();
    });

    $(".searchIngredients-input").on("click", function(){
        var data = $(this).val()
        if (data){
             $(".searchIngredients-input").autocomplete("search");
        }
    })

}

/**
 * Sets up event listeners that check if input fields are empty or not and apply different styles
 * based on that
 */
function setupNonemptyListeners(){
    $('dl.product input.productInput').on('change', function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $('.addLabelComponentsNewComponent').on('change', function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $('#inputPortionSize').on('change', function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $('#componentsInput').on('change', function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $('#componentsInputLabel').on('change', function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $("#labelForm").on('change', 'input', function () {
        if ($(this).val()) $(this).addClass('nonempty');
        else $(this).removeClass('nonempty');
    });

    $("#add-label-components-table").on('change', 'input', function () {
        if ($(this).val()){
            $(this).addClass('nonempty');
            $(this).parent().addClass('nonempty');
        } else {
            $(this).removeClass('nonempty');
            $(this).parent().removeClass('nonempty');
        }
    });

    $("#add-label-components-table input").each(function(){
        if ($(this).val()){
            $(this).addClass('nonempty');
            $(this).parent().addClass('nonempty');
        }
    });
}

/**
 * Sets up all other listeners that don't fit into any of the above functions
 */
function setupMiscListeners(){
    $("#btn-login-form-register").on('click', disableUnloadWarning);
    $("#login form").on('submit', disableUnloadWarning);
    $('#add-help-icon').on('click', function(){
        $('#add-help-icon-text').slideToggle();
    });
    $('#saveProduct').on('click', saveProduct);
}

/**
 * Registers all event listeners used by components on the add page
 */
function setupEventListeners(){
    setupExportListeners();
    setupLocalstorageListeners();
    setupLabelUploadListeners();
    setupImageListeners();
    setupIngredientAllergenMethodListeners();
    setupDeleteItemListeners();
    setupResetListeners();
    setupIngredientSearchWidget();
    setupNonemptyListeners();
    setupMiscListeners();
    setupConversionListeners();
}

/**
 * Adds a component to the main component table
 *
 * This function is used to add any extra components that belong to the product being edited.
 * @param {string} compName - The component to add
 */
function addComponentToMainTable(compCode, label) {
    var comp = otherComponents.find(matchComp('code', compCode)); 
    if (!comp) return;
    var compName = comp.name;
    //add the component to an array that will be uploaded to the server to tell it
    //what extra components have been uploaded
    extraComponents.push(comp.code)
    var compBeforeCode = comp['before'];


    if (label){
        //Even when we are editing a product, we still have diferent id names for component table rows,
        //so we need to handle this case seperately
         if (compBeforeCode === "SALT"){
            compBeforeCode = "NA";
        }
        while ($('#' + compBeforeCode.toLowerCase() +'Label').length === 0) {
            var compBefore = otherComponents.find(matchComp('code', compBeforeCode));
            compBeforeCode = compBefore['before'];
            //We also need to check here
            if (compBeforeCode === "SALT"){
                compBeforeCode = "NA";
            }
        }
        compBeforeCode=compBeforeCode.toLowerCase() + "Label"
        addLabelComponent(comp, compName, compBeforeCode);
        //When editing a label, we need to manually set the value of the input
        var newComponentInputSelector = "#"+compCode.toLowerCase()+"Label input";
        var newComponentValue = productComponents[compCode] || 0;
        $(newComponentInputSelector).val(newComponentValue)

    } else {
        while ($('#' + compBeforeCode.toLowerCase() +'-100').length === 0) {
            var compBefore = otherComponents.find(matchComp('code', compBeforeCode));
            compBeforeCode = compBefore['before'];
        }
        compBeforeCode=compBeforeCode.toLowerCase();
        $('#listComponents').addComponent(comp, compName, compBeforeCode);
    }

    
}


/**
 * Adds all additional components of a product being edited to the main component table
 * The extra components are saved in a global variable serverExtraComponents provided by the Django server
 */
function addExtraComponents(label){
    for (var i=0; i < serverExtraComponents.length; i++){
        addComponentToMainTable(serverExtraComponents[i], label);
    }
    requestComponentValues();
    //The added components have a "." instead of a "," as a decimal seperator. We need to fix this
    fixComponentTableDecimalSeperators();
}


/**
 * Initializes the input box for adding a component to the component table
 * @param  {object}  element - The jQuery object of the input box we want to initialize
 * @param  {boolean} label   - True if we are initializing an input box in the "add from label" interface,
 *                             false otherwise
 */
function setupAddComponent(element, label){
    var componentNames = otherComponents.map(function (x) {
        return x['name'];
    });
    element.autocomplete({
        source: componentNames,
        
        select: function(event, selected){
            /*the select function overwrites autocomplete's default behaviour
             which is to set the input box text to what the user selected
             Since we still want that to happen (otherwise the enter funxtion breaks)
             we need to do it ourselves)*/
            $(this).val(selected.item.value);
            /*then, we can trigger the enter event*/
            
            $(this).trigger('enter');
            //Returning false stops the default select function from running
            //if it did, it would fill in the same text again, which we don't want
            return false;
        }
        
    });
    element.keydown(function (e) {
        if (e.keyCode == 13) $(this).trigger('enter');
    });
    
    /*addComponent*/
    element.bind('enter', function () {
        var compName = $(this).val();
        var comp = otherComponents.find(matchComp('name', compName));
        //add the component to an array that will be uploaded to the server to tell it
        //what extra components have been uploaded
        extraComponents.push(comp.code)
        var compBeforeCode = comp['before'];
        if (label){
            //in the label form, the salt input box has the id naLabel
            //instead of salt label.
            //That's because this name is used when uploading the product
            //and the API doesn't allow us to upload salt, only natrium (na)
             if (compBeforeCode === "SALT"){
                compBeforeCode = "NA";
            }
            while ($('#' + compBeforeCode.toLowerCase() +'Label').length === 0) {
                var compBefore = otherComponents.find(matchComp('code', compBeforeCode));
                compBeforeCode = compBefore['before'];
                //We also need to check here
                if (compBeforeCode === "SALT"){
                    compBeforeCode = "NA";
                }
            }
            //make sure we add the component after the one in the label table, not the main one
            compBeforeCode=compBeforeCode.toLowerCase() + "Label"
            addLabelComponent(comp, compName, compBeforeCode);
        } else {
            while ($('#' + compBeforeCode.toLowerCase() +'-100').length === 0) {
                var compBefore = otherComponents.find(matchComp('code', compBeforeCode));
                compBeforeCode = compBefore['before'];
            }
            compBeforeCode=compBeforeCode.toLowerCase();
            $('#listComponents').addComponent(comp, compName, compBeforeCode);
        }

        componentNames = componentNames.filter(function (x) {
           return x !== compName;
        });
        $(this).autocomplete('option', 'source', componentNames);

        $(this).val('');

        requestComponentValues();
    });
}


function setupUnloadWarning(){
    //uses the are-you-sure library
    //https://github.com/codedance/jquery.AreYouSure
    //Doesn't work: doesn't register adding ingredients/changing portion as a form change
    //so it doesn't disaply a warning
    //
    //We will need custom logic to handle form changes
    
    $('form').areYouSure( {'addRemoveFieldsMarksDirty':true,
                           'message':'vnešeni podatki ne bodo shranjeni',
                            'silent': true});

    //Custom warning function when the user tries to leave the page.
    //Don't show when saving.
    //If the user is leaving the page for anything other than logging in, delete the data
    $(window).on('beforeunload', function() {
            if ($('form').hasClass('dirty')) {
                if (!preserveData){
                    localStorage.clear();
                }

                return "Podatki, ki ste jih vnesli, niso shranjeni";

            }
        });
}




/**
 * Sets up helper listeners related to conversion
 *
 * In order to not lose precision when converting, the page uses a global variable called originalComponents.
 * This variable is a dictionary holding component values before conversion.
 *
 * Initially, this is passed from the server. This works fine for recipes.
 * With labels, the user can change the original component values. If that happens, the originalComponents
 * variable needs to be updated as well.
 */
function setupConversionListeners(){ 
    $("#listComponentsLabel").on("change", ".label-table-input", function(event){
        var id = this.id;
        //the id of the element is something like code-label-input
        //so cut of everything after the first - to get the code
        var componentCode = id.substring(0,id.indexOf("-"))
        var componentWeight = Number($(this).val());
        if (componentWeight) {
            //The component weight first needs to be converted to grams,
            //since originalComponents doesn't hold information about units, and assumes all its values are in grams
            var unitsContainer = $(this).parents("tr").find("small span")
            var units = unitsContainer.text();
            
            componentWeight = convertUnits(componentWeight, units, 'g')
            if(componentCode.toUpperCase() === "NA") componentWeight = componentWeight / 2.5;
            originalComponents[componentCode.toUpperCase()] = componentWeight;
        }
    })
}


/**
 * Sets up input boxes that will hold long strings and require line break support.
 *
 * By default, input boxed are fixed to one line and don't support line breaks
 * This function replaces a given inpud box with a div with contenteditable=true, which behaves
 * like a traditional input box, but allows line breaks. 
 * A textarea would be an alternative, but would require much more JS to get working properly.
 *
 * In addition, this also sets up a listener function that will add the div input into the form data,
 * as it normally wouldn't be included.
 */

function setupLongInputBoxes() {
    setupLongInputBox($("#recipeForm #id_commercial_name")[0]);
    setupLongInputBox($("#recipeForm #id_name")[0]);

}


/**
 * Sets up a single input box that will hold long strings and require line break support.
 *
 * By default, input boxed are fixed to one line and don't support line breaks
 * This function replaces a given inpud box with a div with contenteditable=true, which behaves
 * like a traditional input box, but allows line breaks. 
 * A textarea would be an alternative, but would require much more JS to get working properly.
 *
 * In addition, this also sets up a listener function that will add the div input into the form data,
 * as it normally wouldn't be included.
 * 
 * @param  {DOM element} element [The DOM element representing the input box to replace]
 */
function setupLongInputBox(element)  {
    if (!element) return;

    //First, hide the initial input box
    $(element).hide();

    //Then, replace it with a div with contenteditable="true"
    var originalId = element.id;
    var expandableId = originalId + "-expand";
    var inputDiv = $("<div></div>", {'class': 'expandable-input productInput', 'id': expandableId, 'contenteditable':'true' });
    $(element).after(inputDiv);

    //If the input box already has input, we need to copy it into the new div
    inputDiv.html($(element).val())
    if (inputDiv.text().length > 0) {
        inputDiv.addClass('nonempty');
    }

    //Prevent the user from manually inputing linebreaks:
    //The box should only expand to a new line once the content gets too long, not from user input
    //https://stackoverflow.com/a/428139
    inputDiv.on("keypress", function(e){ return e.which != 13; });

    //since divs aren't sent during a form submit, 
    //we need to copy the data from this div into the original (hidden) input box
    //
    //use more than just input event for browser compatibility (IE11 doesn't support it, maybe some mobile)
    inputDiv.on('blur keyup paste input', function(){
        $(element).val(inputDiv.text());
        //we need to manually trigger the change event to run all associated listeners (for example localstorage)
        $(element).trigger("change")


        //also check for nonempty and apply the proper css class
        if (inputDiv.text().length > 0) inputDiv.addClass('nonempty');
        else inputDiv.removeClass('nonempty');
    });
}