/**
 * Takes in an ingredient array and returns an array of component names only,
 * preproccessed by the fuzzysearch library for better performance
 * @param  {array} initArray  the array of ingredient data received from the server
 * @return {array}           an array containing only ingredient names, processed by the fuzzysearch library
 */
function prepareFuzzysearchArray(initArray){
    var tempArray = initArray.map(function (element) {
                        return element.text
                    });
    return tempArray;
}


/**
 * Create an array that will be used to display an ingredient in the searchbox
 * The important fields are:
 *
 * label: The HTML hat is displayed inside the searchbox, on the left (the name of the ingredient)
 * code: The code of the product
 * barcode: needed to enable searching for barcodes, not just names
 * @param  {array} initArray  the array of ingredient data received from the server
 * @return {array}           the new, formatted array
 */
function prepareOptionArray(initArray){
    var tempArray =  initArray.map(function (element) {
                        var text = element.text;
                        return {
                            label: " <img src=" + element.typeImage +"><span class = 'searchbox-text'>" + text + "</span></img>",
                            value: text,
                            components: element.components || "",
                            code: element.id,
                            option: this,
                            barcode: element.barcode || "",
                            tooltip: element.tooltip,
                            favscore: element.favscore
                        };
                     });

    $.each(tempArray, function(index, e){
        //incorrect value this.optin
        //on.value ("IND%") for record "OTROŠKA KAŠICA Z BANANO IN JABOLKOM, NESTLE" in database
        if(e.value != "OTROŠKA KAŠICA Z BANANO IN JABOLKOM, NESTLE"){
            var comps = e.components;
            e.label= e.label + comps;
        }
    });

    return tempArray;

}

$(function () {
    //The lowest fuzzysearch score that we still display in the results (A higher score means a better match)
    //A score of 0 is a perfect match, everything else is lower than zero
    //A single insertion (swp -> swap) is ussualy somewhere around -1000 
    var FUZZAYSEARCH_THRESHOLD = -5000;
    //For performance, we will store all ingredient data in a json file
    //that will be retrieved from the server asynchronously.
    //This way, we can load the website fast, and then hopefully load the 
    //ingredientdata before the user starts searching. 

    //componentArray will hold all ingredient data
    //this data is loaded with an Ajax request to improve page loading time,
    //since the data itself is pretty big
    var componentArray = []
    //the array to run the fuzzysearch on
    var fuzzyArray = []

    var optionArray = []


    //Variables used for the fuzzy search 
    var matches = []
    var pr = []

    $.ajax({
        url: '/components.json',
        type: 'GET',
        dataType: 'json'
    })
    .done(function( data ) {
        componentArray = data["results"];
        //index the results with the fuzzysearch library, to make searching faster
        fuzzyArray  = prepareFuzzysearchArray(componentArray);
        optionArray = prepareOptionArray(componentArray);
        //hide the message that tells the user the data hasnt loaded yet
        $('#ingredient-data-loading-message').hide();
        $('.ingredientSearch input').autocomplete("search");
    })
    .fail(function( error) {
        $('#ingredient-data-loading-message')
            .text("Prišlo je do napake pri prenosu podatkov o sestavinah. Prosim, zaprite stran in poskusite ponovno.")
        $('#ingredient-data-loading-message').show();
    });
    

    $.widget('custom.ingredientsearch', {
        _create: function () {
            this.wrapper = $('<span>')
                .addClass('ingredientSearch')
                .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(':selected'),
                value = selected.val() ? selected.text() : '';
                

            this.input = $('<input>')
                .appendTo(this.wrapper)
                .val(value)
                .attr('title', '')
                .attr('type', 'text')
                .addClass('searchIngredients-input ui-widget ui-widget-content ui-state-default')
                .autocomplete({
                    classes: {
                         "ui-autocomplete": "autocomplete-large"
                    },
                    delay: 0,
                    minLength: 0,
                    source: $.proxy(this, '_source'),
                    html: true,
                    //add header
                    open: function(){
                        $('ul.ui-autocomplete').prepend('<li><div><li class="list-header"><span class="preserve-whitespace">\xa0Živilo</span>'+
                                           '<span class="searchbox-components align-right-ingredient-table">Na 100g\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0</span></li></div></li>');
                        
                        $('[data-toggle="tooltip"]').bootstrapTooltip();
                    },

                    select: function(){
                        //clear the search input box to allow the user to keep adding new items
                        this.value = "";
                        //prevents autocomplete's default behaviour, which is to set the input text to the selected input
                        //we don't want that, because it would overwrite the cleared input field
                        return false;
                    }
                })
                .tooltip({
                    classes: {
                        'ui-tooltip': 'ui-state-highlight'
                    }
                });

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    data={label:ui.item.value, val:ui.item.code, tooltip:ui.item.tooltip};
                    //ui.item.option.selected = true;
                    this._trigger('select', event, {
                        item: ui.item.option
                    });
                    this.element.trigger('change', [data]);
                },

                autocompletechange: '_removeIfInvalid'
           });
        },

        reset: function () {
            this.input.val('');
        },

        /*
            Determines what gets shown on the autocomplete results list
            request.term gives the text the user entered


            label: "<span class = 'searchbox-text'>"+text + "</span>",
            value: text,
            components: element.components || "",
            code: element.id
         */
        _source: function (request, response) {
            //if the ajax data hasn't finished loading yet, display a message and return
            if (componentArray.length === 0){
                $('#ingredient-data-loading-message').show();
                return;
            }


            //clear all extra whitespace from the search term
            var term = request.term.trim().replace(/\s\s+/g, ' ');
            //Don't return anything if the search field is empty 
            //(which happens when the user deletes his search term, or just presses down in the input box)
            //This is needed because there are some ingredients with empty names in the database that
            //we don't want to show.
            if (!term) {
                response([]);
                return;
            }








            //FUZZY

                matches = []
                var input = request.term.split(' ');
                var result = perms(input);
                //Result <-- all permuntations of typed string in searchbox, stored as Array
                //For loop goes thru array "result" and stores all matches of each permuntation in array "matches"
                //tempArray <--Only names of ingredients
                var tempArray = []; 

                for (var i = 0; i < result.length; i++) {
                    result[i] = result[i].join(' ');
                    //var matcher = new RegExp($.ui.autocomplete.escapeRegex(result[i]), 'i');

                    var a=0;
                    /*
                    var array = componentArray.map(function (element) {
                        var text = element.text;
                        tempArray.push(text);
                        return {
                            label: "<span class = 'searchbox-text'>"+text + "</span>",
                            value: text,
                            components: element.components || "",
                            code: element.id,
                            option: this
                        };

                     });
                    */
                    var startms = Date.now() //only for testing speeds of alghoritms
                    //fuzzysort works only for arrays, not map arrays like our "array", so I made new array "tempArray" with only ingredients names
                    //tmp returns temponary 10 most common results to search string (searching by fuzzy sort)
                    var tmp = fuzzysort.go(result[i],fuzzyArray,
                        {
                            //only return 30 best scores
                            limit: 30,
                            //ignore matches with very low scores
                            threshold: FUZZAYSEARCH_THRESHOLD,
                        });

                    //in "pr" are saved 10 search results in correct form (label, value, option)
                    pr = [];
                    for(var m=0;m<tmp.length;m++){              //10 times
                        for(var n=0;n<optionArray.length;n++){        //m times (m=number of records in database), still faster than testing matching with regex
                            if(tmp[m].target == optionArray[n].value && !pr.includes(optionArray[n])){
                                optionArray[n].score = tmp[m].score
                                pr.push(optionArray[n]);
                            }
                        }
                    }
                    matches.push(pr);
                }


                //Matches now contains an array where each element contains results of one permutation of search strings
                //First, we flatten this array to get an array of all matches
                flattenArray(matches);
                //Then , we sort the array by search score (closer matches come first), and take the first 30 results
                matches = matches.sort(function(a,b){
                    return b.score - a.score
                }).slice(0,30);

                //Then, sort the results by their FAVSCORE parameter. 
                //This parameter helps push more popular products to the top of the search results
                matches = matches.sort(function(a,b){
                    return parseFloat(b.favscore) - parseFloat(a.favscore)
                })
                //If we don't have enough matches yet, search by barcode
                //When the user just enters some numbers, we can't know if he's searching by name or barcode
                //We will prioritize name matches first, then only add in barcode matches after that
                if (matches.length < 30) {
                    var remaining_matches = 30-matches.length;
                    var term_length = term.length;
                    var barcode_matches = [];
                    //loop through all items to search for barcodes
                    for (var i = 0; i < optionArray.length; i++) {
                        barcode_substring = optionArray[i].barcode.substring(0,term_length)
                        if (barcode_substring == term){
                            barcode_matches.push(optionArray[i]);
                        } 
                    }

                    barcode_matches = barcode_matches.slice(0,remaining_matches);
                    matches = matches.concat(barcode_matches)

                }

                //shorten the names to make them fit into the text boxes
                
                response(matches);
                //console.log(pr);
        },

        _removeIfInvalid: function (event, ui) {

            // Selected an item, nothing to do
            if (ui.item) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
            this.element.children('option').each(function () {
                if ($(this).text().toLowerCase() === valueLowerCase) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if (valid) {
                return;
            }

            this.element.val('');
            this._delay(function () {
                this.input.tooltip('close').attr('title', '');
            }, 2500);
            this.input.autocomplete('instance').term = "";
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        },

    });
});






 //Permuntations of String typed in searchbox
function perms(data) {
    if (!(data instanceof Array)) {
        throw new TypeError("input data must be an Array");
    }

    data = data.slice();  // make a copy
    var permutations = [],
        stack = [];

    function doPerm() {
        if (data.length == 0) {
            permutations.push(stack.slice());
        }
        for (var i = 0; i < data.length; i++) {
            var x = data.splice(i, 1);
            stack.push(x);
            doPerm();
            stack.pop();
            data.splice(i, 0, x);
        }
    }

    doPerm();
    /*
    for (var i = 0; i < permutations.length; i++) {
        permutations[i] = flattenArray([permutations[i]]).join(" ");
    }
    */
    return permutations;
}


// -----------------------------------------------------------------------FUZZY SEARCH-----------------------------------------------------------------------------------

//Fuzzysort Licence
//https://github.com/farzher/fuzzysort
/*
Copyright (c) 2018 Stephen Kamenar
x
https://github.com/farzher/fuzzysort/blob/master/LICENSE
*/


;(function(root, UMD) {
  if(typeof define === 'function' && define.amd) define([], UMD)
  else if(typeof module === 'object' && module.exports) module.exports = UMD()
  else root.fuzzysort = UMD()
})(this, function UMD() { function fuzzysortNew(instanceOptions) {

  var fuzzysort = {

    single: function(search, target, options) {
      if(!search) return null
      if(!isObj(search)) search = fuzzysort.getPreparedSearch(search)

      if(!target) return null
      if(!isObj(target)) target = fuzzysort.getPrepared(target)

      var allowTypo = options && options.allowTypo!==undefined ? options.allowTypo
        : instanceOptions && instanceOptions.allowTypo!==undefined ? instanceOptions.allowTypo
        : true
      var algorithm = allowTypo ? fuzzysort.algorithm : fuzzysort.algorithmNoTypo
      return algorithm(search, target, search[0])
    },

    go: function(search, targets, options) {
      if(!search) return noResults
      search = fuzzysort.prepareSearch(search)
      var searchLowerCode = search[0]

      var threshold = options && options.threshold || instanceOptions && instanceOptions.threshold || -9007199254740991
      var limit = options && options.limit || instanceOptions && instanceOptions.limit || 9007199254740991
      var allowTypo = options && options.allowTypo!==undefined ? options.allowTypo
        : instanceOptions && instanceOptions.allowTypo!==undefined ? instanceOptions.allowTypo
        : true
      var algorithm = allowTypo ? fuzzysort.algorithm : fuzzysort.algorithmNoTypo
      var resultsLen = 0; var limitedCount = 0
      var targetsLen = targets.length

      // This code is copy/pasted 3 times for performance reasons [options.keys, options.key, no keys]

      // options.keys
      if(options && options.keys) {
        var scoreFn = options.scoreFn || defaultScoreFn
        var keys = options.keys
        var keysLen = keys.length
        for(var i = targetsLen - 1; i >= 0; --i) { var obj = targets[i]
          var objResults = new Array(keysLen)
          for (var keyI = keysLen - 1; keyI >= 0; --keyI) {
            var key = keys[keyI]
            var target = getValue(obj, key)
            if(!target) { objResults[keyI] = null; continue }
            if(!isObj(target)) target = fuzzysort.getPrepared(target)

            objResults[keyI] = algorithm(search, target, searchLowerCode)
          }
          objResults.obj = obj // before scoreFn so scoreFn can use it
          var score = scoreFn(objResults)
          if(score === null) continue
          if(score < threshold) continue
          objResults.score = score
          if(resultsLen < limit) { q.add(objResults); ++resultsLen }
          else {
            ++limitedCount
            if(score > q.peek().score) q.replaceTop(objResults)
          }
        }

      // options.key
      } else if(options && options.key) {
        var key = options.key
        for(var i = targetsLen - 1; i >= 0; --i) { var obj = targets[i]
          var target = getValue(obj, key)
          if(!target) continue
          if(!isObj(target)) target = fuzzysort.getPrepared(target)

          var result = algorithm(search, target, searchLowerCode)
          if(result === null) continue
          if(result.score < threshold) continue

          // have to clone result so duplicate targets from different obj can each reference the correct obj
          result = {target:result.target, _targetLowerCodes:null, _nextBeginningIndexes:null, score:result.score, indexes:result.indexes, obj:obj} // hidden

          if(resultsLen < limit) { q.add(result); ++resultsLen }
          else {
            ++limitedCount
            if(result.score > q.peek().score) q.replaceTop(result)
          }
        }

      // no keys
      } else {
        for(var i = targetsLen - 1; i >= 0; --i) { var target = targets[i]
          if(!target) continue
          if(!isObj(target)) target = fuzzysort.getPrepared(target)

          var result = algorithm(search, target, searchLowerCode)
          if(result === null) continue
          if(result.score < threshold) continue
          if(resultsLen < limit) { q.add(result); ++resultsLen }
          else {
            ++limitedCount
            if(result.score > q.peek().score) q.replaceTop(result)
          }
        }
      }

      if(resultsLen === 0) return noResults
      var results = new Array(resultsLen)
      for(var i = resultsLen - 1; i >= 0; --i) results[i] = q.poll()
      results.total = resultsLen + limitedCount
      return results
    },

    goAsync: function(search, targets, options) {
      var canceled = false
      var p = new Promise(function(resolve, reject) {
        if(!search) return resolve(noResults)
        search = fuzzysort.prepareSearch(search)
        var searchLowerCode = search[0]

        var q = fastpriorityqueue()
        var iCurrent = targets.length - 1
        var threshold = options && options.threshold || instanceOptions && instanceOptions.threshold || -9007199254740991
        var limit = options && options.limit || instanceOptions && instanceOptions.limit || 9007199254740991
        var allowTypo = options && options.allowTypo!==undefined ? options.allowTypo
          : instanceOptions && instanceOptions.allowTypo!==undefined ? instanceOptions.allowTypo
          : true
        var algorithm = allowTypo ? fuzzysort.algorithm : fuzzysort.algorithmNoTypo
        var resultsLen = 0; var limitedCount = 0
        function step() {
          if(canceled) return reject('canceled')

          var startMs = Date.now()

          // This code is copy/pasted 3 times for performance reasons [options.keys, options.key, no keys]

          // options.keys
          if(options && options.keys) {
            var scoreFn = options.scoreFn || defaultScoreFn
            var keys = options.keys
            var keysLen = keys.length
            for(; iCurrent >= 0; --iCurrent) { var obj = targets[iCurrent]
              var objResults = new Array(keysLen)
              for (var keyI = keysLen - 1; keyI >= 0; --keyI) {
                var key = keys[keyI]
                var target = getValue(obj, key)
                if(!target) { objResults[keyI] = null; continue }
                if(!isObj(target)) target = fuzzysort.getPrepared(target)

                objResults[keyI] = algorithm(search, target, searchLowerCode)
              }
              objResults.obj = obj // before scoreFn so scoreFn can use it
              var score = scoreFn(objResults)
              if(score === null) continue
              if(score < threshold) continue
              objResults.score = score
              if(resultsLen < limit) { q.add(objResults); ++resultsLen }
              else {
                ++limitedCount
                if(score > q.peek().score) q.replaceTop(objResults)
              }

              if(iCurrent%1000/*itemsPerCheck*/ === 0) {
                if(Date.now() - startMs >= 10/*asyncInterval*/) {
                  isNode?setImmediate(step):setTimeout(step)
                  return
                }
              }
            }

          // options.key
          } else if(options && options.key) {
            var key = options.key
            for(; iCurrent >= 0; --iCurrent) { var obj = targets[iCurrent]
              var target = getValue(obj, key)
              if(!target) continue
              if(!isObj(target)) target = fuzzysort.getPrepared(target)

              var result = algorithm(search, target, searchLowerCode)
              if(result === null) continue
              if(result.score < threshold) continue

              // have to clone result so duplicate targets from different obj can each reference the correct obj
              result = {target:result.target, _targetLowerCodes:null, _nextBeginningIndexes:null, score:result.score, indexes:result.indexes, obj:obj} // hidden

              if(resultsLen < limit) { q.add(result); ++resultsLen }
              else {
                ++limitedCount
                if(result.score > q.peek().score) q.replaceTop(result)
              }

              if(iCurrent%1000/*itemsPerCheck*/ === 0) {
                if(Date.now() - startMs >= 10/*asyncInterval*/) {
                  isNode?setImmediate(step):setTimeout(step)
                  return
                }
              }
            }

          // no keys
          } else {
            for(; iCurrent >= 0; --iCurrent) { var target = targets[iCurrent]
              if(!target) continue
              if(!isObj(target)) target = fuzzysort.getPrepared(target)

              var result = algorithm(search, target, searchLowerCode)
              if(result === null) continue
              if(result.score < threshold) continue
              if(resultsLen < limit) { q.add(result); ++resultsLen }
              else {
                ++limitedCount
                if(result.score > q.peek().score) q.replaceTop(result)
              }

              if(iCurrent%1000/*itemsPerCheck*/ === 0) {
                if(Date.now() - startMs >= 10/*asyncInterval*/) {
                  isNode?setImmediate(step):setTimeout(step)
                  return
                }
              }
            }
          }

          if(resultsLen === 0) return resolve(noResults)
          var results = new Array(resultsLen)
          for(var i = resultsLen - 1; i >= 0; --i) results[i] = q.poll()
          results.total = resultsLen + limitedCount
          resolve(results)
        }

        isNode?setImmediate(step):step()
      })
      p.cancel = function() { canceled = true }
      return p
    },

    highlight: function(result, hOpen, hClose) {
      if(result === null) return null
      if(hOpen === undefined) hOpen = '<b>'
      if(hClose === undefined) hClose = '</b>'
      var highlighted = ''
      var matchesIndex = 0
      var opened = false
      var target = result.target
      var targetLen = target.length
      var matchesBest = result.indexes
      for(var i = 0; i < targetLen; ++i) { var char = target[i]
        if(matchesBest[matchesIndex] === i) {
          ++matchesIndex
          if(!opened) { opened = true
            highlighted += hOpen
          }

          if(matchesIndex === matchesBest.length) {
            highlighted += char + hClose + target.substr(i+1)
            break
          }
        } else {
          if(opened) { opened = false
            highlighted += hClose
          }
        }
        highlighted += char
      }

      return highlighted
    },

    prepare: function(target) {
      if(!target) return
      return {target:target, _targetLowerCodes:fuzzysort.prepareLowerCodes(target), _nextBeginningIndexes:null, score:null, indexes:null, obj:null} // hidden
    },
    prepareSlow: function(target) {
      if(!target) return
      return {target:target, _targetLowerCodes:fuzzysort.prepareLowerCodes(target), _nextBeginningIndexes:fuzzysort.prepareNextBeginningIndexes(target), score:null, indexes:null, obj:null} // hidden
    },
    prepareSearch: function(search) {
      if(!search) return
      return fuzzysort.prepareLowerCodes(search)
    },



    // Below this point is only internal code


    getPrepared: function(target) {
      if(target.length > 999) return fuzzysort.prepare(target) // don't cache huge targets
      var targetPrepared = preparedCache.get(target)
      if(targetPrepared !== undefined) return targetPrepared
      targetPrepared = fuzzysort.prepare(target)
      preparedCache.set(target, targetPrepared)
      return targetPrepared
    },
    getPreparedSearch: function(search) {
      if(search.length > 999) return fuzzysort.prepareSearch(search) // don't cache huge searches
      var searchPrepared = preparedSearchCache.get(search)
      if(searchPrepared !== undefined) return searchPrepared
      searchPrepared = fuzzysort.prepareSearch(search)
      preparedSearchCache.set(search, searchPrepared)
      return searchPrepared
    },

    algorithm: function(searchLowerCodes, prepared, searchLowerCode) {
      var targetLowerCodes = prepared._targetLowerCodes
      var searchLen = searchLowerCodes.length
      var targetLen = targetLowerCodes.length
      var searchI = 0 // where we at
      var targetI = 0 // where you at
      var typoSimpleI = 0
      var matchesSimpleLen = 0

      // very basic fuzzy match; to remove non-matching targets ASAP!
      // walk through target. find sequential matches.
      // if all chars aren't found then exit
      for(;;) {
        var isMatch = searchLowerCode === targetLowerCodes[targetI]
        if(isMatch) {
          matchesSimple[matchesSimpleLen++] = targetI
          ++searchI; if(searchI === searchLen) break
          searchLowerCode = searchLowerCodes[typoSimpleI===0?searchI : (typoSimpleI===searchI?searchI+1 : (typoSimpleI===searchI-1?searchI-1 : searchI))]
        }

        ++targetI; if(targetI >= targetLen) { // Failed to find searchI
          // Check for typo or exit
          // we go as far as possible before trying to transpose
          // then we transpose backwards until we reach the beginning
          for(;;) {
            if(searchI <= 1) return null // not allowed to transpose first char
            if(typoSimpleI === 0) { // we haven't tried to transpose yet
              --searchI
              var searchLowerCodeNew = searchLowerCodes[searchI]
              if(searchLowerCode === searchLowerCodeNew) continue // doesn't make sense to transpose a repeat char
              typoSimpleI = searchI
            } else {
              if(typoSimpleI === 1) return null // reached the end of the line for transposing
              --typoSimpleI
              searchI = typoSimpleI
              searchLowerCode = searchLowerCodes[searchI + 1]
              var searchLowerCodeNew = searchLowerCodes[searchI]
              if(searchLowerCode === searchLowerCodeNew) continue // doesn't make sense to transpose a repeat char
            }
            matchesSimpleLen = searchI
            targetI = matchesSimple[matchesSimpleLen - 1] + 1
            break
          }
        }
      }

      var searchI = 0
      var typoStrictI = 0
      var successStrict = false
      var matchesStrictLen = 0

      var nextBeginningIndexes = prepared._nextBeginningIndexes
      if(nextBeginningIndexes === null) nextBeginningIndexes = prepared._nextBeginningIndexes = fuzzysort.prepareNextBeginningIndexes(prepared.target)
      var firstPossibleI = targetI = matchesSimple[0]===0 ? 0 : nextBeginningIndexes[matchesSimple[0]-1]

      // Our target string successfully matched all characters in sequence!
      // Let's try a more advanced and strict test to improve the score
      // only count it as a match if it's consecutive or a beginning character!
      if(targetI !== targetLen) for(;;) {
        if(targetI >= targetLen) {
          // We failed to find a good spot for this search char, go back to the previous search char and force it forward
          if(searchI <= 0) { // We failed to push chars forward for a better match
            // transpose, starting from the beginning
            ++typoStrictI; if(typoStrictI > searchLen-2) break
            if(searchLowerCodes[typoStrictI] === searchLowerCodes[typoStrictI+1]) continue // doesn't make sense to transpose a repeat char
            targetI = firstPossibleI
            continue
          }

          --searchI
          var lastMatch = matchesStrict[--matchesStrictLen]
          targetI = nextBeginningIndexes[lastMatch]

        } else {
          var isMatch = searchLowerCodes[typoStrictI===0?searchI : (typoStrictI===searchI?searchI+1 : (typoStrictI===searchI-1?searchI-1 : searchI))] === targetLowerCodes[targetI]
          if(isMatch) {
            matchesStrict[matchesStrictLen++] = targetI
            ++searchI; if(searchI === searchLen) { successStrict = true; break }
            ++targetI
          } else {
            targetI = nextBeginningIndexes[targetI]
          }
        }
      }

      { // tally up the score & keep track of matches for highlighting later
        if(successStrict) { var matchesBest = matchesStrict; var matchesBestLen = matchesStrictLen }
        else { var matchesBest = matchesSimple; var matchesBestLen = matchesSimpleLen }
        var score = 0
        var lastTargetI = -1
        for(var i = 0; i < searchLen; ++i) { var targetI = matchesBest[i]
          // score only goes down if they're not consecutive
          if(lastTargetI !== targetI - 1) score -= targetI
          lastTargetI = targetI
        }
        if(!successStrict) {
          score *= 1000
          if(typoSimpleI !== 0) score += -20/*typoPenalty*/
        } else {
          if(typoStrictI !== 0) score += -20/*typoPenalty*/
        }
        score -= targetLen - searchLen
        prepared.score = score
        prepared.indexes = new Array(matchesBestLen); for(var i = matchesBestLen - 1; i >= 0; --i) prepared.indexes[i] = matchesBest[i]

        return prepared
      }
    },

    algorithmNoTypo: function(searchLowerCodes, prepared, searchLowerCode) {
      var targetLowerCodes = prepared._targetLowerCodes
      var searchLen = searchLowerCodes.length
      var targetLen = targetLowerCodes.length
      var searchI = 0 // where we at
      var targetI = 0 // where you at
      var matchesSimpleLen = 0

      // very basic fuzzy match; to remove non-matching targets ASAP!
      // walk through target. find sequential matches.
      // if all chars aren't found then exit
      for(;;) {
        var isMatch = searchLowerCode === targetLowerCodes[targetI]
        if(isMatch) {
          matchesSimple[matchesSimpleLen++] = targetI
          ++searchI; if(searchI === searchLen) break
          searchLowerCode = searchLowerCodes[searchI]
        }
        ++targetI; if(targetI >= targetLen) return null // Failed to find searchI
      }

      var searchI = 0
      var successStrict = false
      var matchesStrictLen = 0

      var nextBeginningIndexes = prepared._nextBeginningIndexes
      if(nextBeginningIndexes === null) nextBeginningIndexes = prepared._nextBeginningIndexes = fuzzysort.prepareNextBeginningIndexes(prepared.target)
      var firstPossibleI = targetI = matchesSimple[0]===0 ? 0 : nextBeginningIndexes[matchesSimple[0]-1]

      // Our target string successfully matched all characters in sequence!

      //More advanced and strict test to improve the score
      // only count it as a match if it's consecutive or a beginning character!
      if(targetI !== targetLen) for(;;) {
        if(targetI >= targetLen) {
          // We failed to find a good spot for this search char, go back to the previous search char and force it forward
          if(searchI <= 0) break // We failed to push chars forward for a better match

          --searchI
          var lastMatch = matchesStrict[--matchesStrictLen]
          targetI = nextBeginningIndexes[lastMatch]

        } else {
          var isMatch = searchLowerCodes[searchI] === targetLowerCodes[targetI]
          if(isMatch) {
            matchesStrict[matchesStrictLen++] = targetI
            ++searchI; if(searchI === searchLen) { successStrict = true; break }
            ++targetI
          } else {
            targetI = nextBeginningIndexes[targetI]
          }
        }
      }

      { // tally up the score & keep track of matches for highlighting later
        if(successStrict) { var matchesBest = matchesStrict; var matchesBestLen = matchesStrictLen }
        else { var matchesBest = matchesSimple; var matchesBestLen = matchesSimpleLen }
        var score = 0
        var lastTargetI = -1
        for(var i = 0; i < searchLen; ++i) { var targetI = matchesBest[i]
          // score only goes down if they're not consecutive
          if(lastTargetI !== targetI - 1) score -= targetI
          lastTargetI = targetI
        }
        if(!successStrict) score *= 1000
        score -= targetLen - searchLen
        prepared.score = score
        prepared.indexes = new Array(matchesBestLen); for(var i = matchesBestLen - 1; i >= 0; --i) prepared.indexes[i] = matchesBest[i]

        return prepared
      }
    },

    prepareLowerCodes: function(str) {
      var strLen = str.length
      var lowerCodes = [] // new Array(strLen)    sparse array is too slow
      var lower = str.toLowerCase()
      for(var i = 0; i < strLen; ++i) lowerCodes[i] = lower.charCodeAt(i)
      return lowerCodes
    },
    prepareBeginningIndexes: function(target) {
      var targetLen = target.length
      var beginningIndexes = []; var beginningIndexesLen = 0
      var wasUpper = false
      var wasAlphanum = false
      for(var i = 0; i < targetLen; ++i) {
        var targetCode = target.charCodeAt(i)
        var isUpper = targetCode>=65&&targetCode<=90
        var isAlphanum = isUpper || targetCode>=97&&targetCode<=122 || targetCode>=48&&targetCode<=57
        var isBeginning = isUpper && !wasUpper || !wasAlphanum || !isAlphanum
        wasUpper = isUpper
        wasAlphanum = isAlphanum
        if(isBeginning) beginningIndexes[beginningIndexesLen++] = i
      }
      return beginningIndexes
    },
    prepareNextBeginningIndexes: function(target) {
      var targetLen = target.length
      var beginningIndexes = fuzzysort.prepareBeginningIndexes(target)
      var nextBeginningIndexes = [] // new Array(targetLen)     sparse array is too slow
      var lastIsBeginning = beginningIndexes[0]
      var lastIsBeginningI = 0
      for(var i = 0; i < targetLen; ++i) {
        if(lastIsBeginning > i) {
          nextBeginningIndexes[i] = lastIsBeginning
        } else {
          lastIsBeginning = beginningIndexes[++lastIsBeginningI]
          nextBeginningIndexes[i] = lastIsBeginning===undefined ? targetLen : lastIsBeginning
        }
      }
      return nextBeginningIndexes
    },

    cleanup: cleanup,
    new: fuzzysortNew,
  }
  return fuzzysort
} // fuzzysortNew

// This stuff is outside fuzzysortNew, because it's shared with instances of fuzzysort.new()
var isNode = typeof require !== 'undefined' && typeof window === 'undefined'
// var MAX_INT = Number.MAX_SAFE_INTEGER
// var MIN_INT = Number.MIN_VALUE
var preparedCache = new Map()
var preparedSearchCache = new Map()
var noResults = []; noResults.total = 0
var matchesSimple = []; var matchesStrict = []
function cleanup() { preparedCache.clear(); preparedSearchCache.clear(); matchesSimple = []; matchesStrict = [] }
function defaultScoreFn(a) {
  var max = -9007199254740991
  for (var i = a.length - 1; i >= 0; --i) {
    var result = a[i]; if(result === null) continue
    var score = result.score
    if(score > max) max = score
  }
  if(max === -9007199254740991) return null
  return max
}

// prop = 'key'              2.5ms optimized for this case, seems to be about as fast as direct obj[prop]
// prop = 'key1.key2'        10ms
// prop = ['key1', 'key2']   27ms
function getValue(obj, prop) {
  var tmp = obj[prop]; if(tmp !== undefined) return tmp
  var segs = prop
  if(!Array.isArray(prop)) segs = prop.split('.')
  var len = segs.length
  var i = -1
  while (obj && (++i < len)) obj = obj[segs[i]]
  return obj
}

function isObj(x) { return typeof x === 'object' } // faster as a function

var fastpriorityqueue=function(){var r=[],o=0,e={};function n(){for(var e=0,n=r[e],c=1;c<o;){var f=c+1;e=c,f<o&&r[f].score<r[c].score&&(e=f),r[e-1>>1]=r[e],c=1+(e<<1)}for(var a=e-1>>1;e>0&&n.score<r[a].score;a=(e=a)-1>>1)r[e]=r[a];r[e]=n}return e.add=function(e){var n=o;r[o++]=e;for(var c=n-1>>1;n>0&&e.score<r[c].score;c=(n=c)-1>>1)r[n]=r[c];r[n]=e},e.poll=function(){if(0!==o){var e=r[0];return r[0]=r[--o],n(),e}},e.peek=function(e){if(0!==o)return r[0]},e.replaceTop=function(o){r[0]=o,n()},e};
var q = fastpriorityqueue() // reuse this, except for async, it needs to make its own

return fuzzysortNew()
})


//------------- FLATTEN ARRAY -----------------//
/*
Copyright (c) 2017-2018 Eli Doran
MIT Licenced
https://github.com/elidoran/flatten-array/blob/master/LICENSE
*/

//
// flatten arrays into a single array.
// NOTE:
//   this mutates the specified array.
//   if you want to avoid that, then wrap your array:
//     flatten([myArray])
function flattenArray (array) {

  // usual loop, but, don't put `i++` in third clause
  // because it won't increment it when the element is an array.
  for (var i = 0; i < array.length; ) {

    var value = array[i]

    // if the element is an array then we'll put its contents
    // into `array` replacing the current element.
    if (Array.isArray(value)) {

      // only process `value` if it has some elements.
      if (value.length > 0) {

        // to provide the `value` array to splice() we need to add the
        // splice() args to its front.
        // these args tell it to splice at `i` and delete what's at `i`.
        value.unshift(i, 1)

        // NOTE:
        // This is an in-place change; it mutates `array`.
        // To avoid this, wrap your array like: flatten([myarray])
        array.splice.apply(array, value)

        // take (i, 1) back off the `value` front
        // so it remains "unchanged".
        value.splice(0, 2)
      } else {
        // remove an empty array from `array`
        array.splice(i, 1)
      }

      // NOTE: we don't do `i++` because we want it to re-evaluate
      // the new element at `i` in case it is an array,
      // or we deleted an empty array at `i`.

    } else {
      // it's not an array so move on to the next element.
      i++
    }
  }

  // return the array so `flatten` can be used inline.
  return array
}