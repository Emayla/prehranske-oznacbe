//Force the select2 dropdown width to always fill its container
//Otherwise, we get issues when we resize the page, since the select2 library
//will only calculate the dropdown width on page load
$.fn.select2.defaults.set( "width", "100%" );

var selectedPhoto = '';
var item_to_be_deleted = "";
var LABEL_INGREDIENT_PREFIX = "IND";

/**
 * Allowed filenames for the uploaded photo
 * @type {Array}
 */
var ALLOWED_PHOTO_FILE_TYPES = ['jpg', 'jpeg', 'png'];
/**
 * Maximum photo filesize, in bytes
 * @type {Number}
 */
var ALLOWED_PHOTO_FILE_SIZE = 10000000;

/**
 * Django requires a csrftoken parameter to be sent with every ajax request
 * @type {string}
 */
var csrftoken = getCookie('csrftoken');
var envalkJ;

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});




// On document ready
$(function () {


    //Manually reset group dropdowns. Otherwise, they will default to selecting the first dropdown item
    //automatically. This has to be done before restoring data to not overwrite the resotred data.
    clearDefaultGroupSelection();
    

    //Django will fill out our label form with the data of the actual product when editing it
    //This function will clear this extra data
    clearLabelData();

    //To avoid conflicts with local storage when editing items, this now handles all data restoration, so fillCurrentProductData is merged into it
    //On the first load, data is restored with fillCurrentProductData and saved localy. This local data is then used on any page refresh
    restoreData();

    addExtraComponents(editingLabel);

    portionSize = $('#inputPortionSize').val();

    $('select[name=ingredients]').val(null);
    $('.searchIngredients-input').keydown(function (e) {
        if (e.keyCode == 13) $(this).trigger('enter');
    });

    $('#searchIngredients').hide();
    $('#addLabelForm').hide();
    

    setupAddComponent($('#componentsInput'), false);
    setupAddComponent($('#componentsInputLabel'), true);

    //in common
    initSidebar();


    $('.group_dropdown_container [id$="group"]').each(function(e){
        if ($(this).val()){
            $(this).parent().find('#select2-id_group-container').addClass('nonempty');
            $(this).parent().addClass('nonempty');
        } else {
            $(this).parent().find('#select2-id_group-container').removeClass('nonempty');
            $(this).parent().removeClass('nonempty');   
        }
    });


    $('#ajaxErrorMessage').hide();

    setupEventListeners();
    setupUnloadWarning();

    //make add ingredient active
    $("#addDatabase").prop("checked", true);
    $('#addDatabase').triggerHandler('click');

    $('.searchIngredients-input').attr("placeholder", "Poišči sestavino po imenu ali črtni kodi...");

});






