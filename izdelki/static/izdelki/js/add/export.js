/**
 * Contains all functions related to exporting data from the add product screen to an Excel/PDF file
 * @module add/export
 */

/*Array.find polyfill, for IE support */
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find#Polyfill
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return kValue.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return kValue;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return undefined.
      return undefined;
    },
    configurable: true,
    writable: true
  });
}





/**
 * Creates a hidden form and uses this form to create a POST request. <br>
 *
 * This is used instead of a normal AJAX request whenever we need to download files
 * (such as when exporting data), because AJAX requests cannot trigger the browser's
 * download file dialog
 * 
 * @param  {string} path       - The server url to send the data to
 * @param  {object} parameters - The data to send
 */
function postForm(path, parameters) {
        var form = $('<form></form>');
        var csrfElement =  $('input[name="csrfmiddlewaretoken"]').clone(); 
        form.attr("method", "post");
        form.attr("action", path);
        form.append(csrfElement);
        parameters.forEach(function(item) {
            var field = $('<input />');
            field.attr("type", "hidden");
            field.attr("name", item.name);
            field.attr("value", item.value);
            form.append(field);
            });
        $(document.body).append(form);
        form.submit();
}


/**
 * Collects all data on the product page and sends it to the server
 * @param  {string} format - Either "PDF" or "XLS" (Excel)
 * @param  {boolean} label - Is the product being exported rom a label add/edit page or not. Defaults to false
 */
function exportFile(format, label = false){
    var productData = gatherAllProductData();
    $("#error-submit-input").hide();
    if (!productData){
        $("#error-submit-input").show();
        return;
    }
    
    /*
    var meal_size = productData.find(function(obj){
        return obj.name==="meal_size"
    });


    if (meal_size === undefined) {
        $("#error-submit-portion").show();
        return;
    }
    */

    var reqIngredients = gatherIngredients();
    if (!reqIngredients) return;

    var reqComponents = [];
    
    if (label){
        reqComponents = gatherComponents()
        productData.push({name: 'csrfmiddlewaretoken', value: csrf_token});
        $.each(reqComponents, function (index, val) {
            code = val.code;
            weight = val.weight;
            //undefined salt will show up as "NaN", so we need to catch that
            //other variables seem to work fine 
            if (weight==="NaN"){
                weight = 0;
            }
            productData.push({name: 'components[' +code+ ']', value: weight});
            
            
        });
        if (format === "XLS"){
            postForm('/get_xls/', productData)
        } else if (format === "PDF"){
           postForm('/get_pdf/', productData) 
        }

    } else {
        $('#listComponents tr').each(function () {
            var id = $(this).attr('id');
            if (id) {
                reqComponents.push(id);
            }
        });

    
        //we need to manually add in an entry for natrium instead of salt   
        reqComponents.push('na')

        //Before we submit the export data, we need to call the API
        //to calculate the component values based on the ingredients
        //
        //We do this in the ajax call, then post the data to the server to get our file
        //with postForm()
        $.ajax({
            url: '/get_components/',
            data: { 
                'components': JSON.stringify(reqComponents), 
                'ingredients': JSON.stringify(reqIngredients), 
                'cook_method': $('#listMethod tr:last-child').attr('id'), 
                'group': $('select[name=group]').val(), 
                'serv_size': $('input[name=serv_size]').val(),
            },
            type: 'GET',
            success: function(response) {
                items = response['components']
                $.each(items, function (index, val) {
                    if (Array.isArray(val)){
                        productData.push({name: 'components[' +index+ ']['+0+']', value: val[0]});
                        productData.push({name: 'components[' +index+ ']['+1+']', value: val[1]});
                    } else {
                        if (index == "salt"){
                            productData.push({name: 'components[na]', value: val/2.5});
                        } else {
                            productData.push({name: 'components[' +index+ ']', value: val});
                        }
                    }
                    
                });
                productData.push({name: 'csrfmiddlewaretoken', value: csrf_token});
                var mealsize = $('#inputPortionSize').val();
                productData.push({name: 'mealsize', value: mealsize});
                if (format === "XLS"){
                    postForm('/get_xls/', productData)
                } else if (format === "PDF"){
                   postForm('/get_pdf/', productData) 
                }
            },
            error: function(error) {
                console.log('error ' + error);
            }
        });
    }
}

/**
 * Sets up event listeners needed to get the export functionality working 
 */
function setupExportListeners(){
    $('#pdf-export').on('click', function () {
        exportFile("PDF");
    });

    $('#xls-export').on('click', function () {
        exportFile("XLS");
    });

    $('#pdf-export-label').on('click', function () {
        exportFile("PDF", true);
    });

    $('#xls-export-label').on('click', function () {
        exportFile("XLS", true);
    });

    $('#xls-export-disabled').on('click', function () {
        window.scrollTo(0,0);
        toggleLogin();
    });

    $("#pdf-export a").on('click', disableUnloadWarning);
    $("#xls-export a").on('click', disableUnloadWarning);
    $("#pdf-export-label a").on('click', disableUnloadWarning);
    $("#xls-export-label a").on('click', disableUnloadWarning);
}