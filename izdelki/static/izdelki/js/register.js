// Show the input fields based on chosen user type when the site refreshes.
$( document ).ready(function() {
    toggleFields();
    disableCompanyInput();
    setupCompanyId();
});

function toggleFields() {
    var userType = $('#register_form input[name=user_type]:checked').val();
    if (userType === 'business') {
        $('#register_form').find('.business-only').show();
        $('#register_form').find('.physical-only').hide();
    }
    else {
        $('#register_form').find('.business-only').hide();
        $('#register_form').find('.physical-only').show();
    }
}

// On change of user type shows or hides the appropriate input fields.
$('#register_form input[name=user_type]').change( function () {
    toggleFields();
});

function disableCompanyInput() {
    //disable company name and address fields until an ID is entered
    $("#id_company_name").prop("disabled", true);
    $("#id_company_name").val("");
    $("#id_company_name").attr("placeholder", "Najprej vnesite matično številko ...");
    $("#id_address").prop("disabled", true);
    $("#id_address").val("");
    $("#id_address").attr("placeholder", "Najprej vnesite matično številko ...");
}

//CSRF setup
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


function setupCompanyId() {
    $("#id_registration_number").on("input", function(){
        $(".company_id_msg").remove();
        var registration_number = $("#id_registration_number").val()
        if (registration_number.length < 7) {
            setIDErrorMessage("Matična številka mora biti dolga natančno 7 znakov");
            disableCompanyInput();
            return;
        }
        
        $.ajax({
            url: '/check_company_id/',
            type: 'POST',
            dataType: 'json',
            data: {"company_id": registration_number},
        })
        .done(function(response) {
            if (response.success === false) {
                setIDErrorMessage("Ta matična številka ne pripada nobenemu slovenskemu podjetju. Preverite, če ste številko pravilno napisali")
                disableCompanyInput();
            } else {
                $("#id_company_name").prop("disabled", false);
                $("#id_company_name").val(response.name);
                $("#id_address").prop("disabled", false);
                $("#id_address").val(response.address);
            }
        })
        .fail(function(response) {
            setIDErrorMessage("Prišlo je do napake pri komunikaciji s strežnikom. Prosim poskusite kasneje")
            disableCompanyInput();
        })
      
    })
}


function setIDErrorMessage(message) {
    $("#id_registration_number").parent().append("<p class='company_id_msg'>" + message + "</p>");
}