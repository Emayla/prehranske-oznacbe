
/**
 * Disables the ability of the user to edit the data on the page
 */
function turnDataEditOff(){
    $("#register_form input").prop("disabled", true);
    $("#register_form input").addClass("form-control-plaintext");
    $("#register_form input").removeClass("form-control");
    $(".edit-user-form-button").hide();
    $(".edit-user-toggle-button").show();
}

function turnDataEditOn(){
    $("#register_form input").prop("disabled", false);
    $("#register_form input").addClass("form-control");
    $("#register_form input").removeClass("form-control-plaintext");
    $(".edit-user-form-button").show();
    $(".edit-user-toggle-button").hide();
}

function turnPasswordEditOn(){
  turnDataEditOff();
  $(".edit-user-toggle-button").hide();
  $("#password-change-wrapper").slideDown();
}

function turnPasswordEditOff(){
  $("#password-change-wrapper").slideUp(400, function() {
    $(".edit-user-toggle-button").show();
  });
}


$(document).ready(function() {
    /*On the edit user page, we want to first just show the user's data, without letting him change it.
     *Then, when he clicks the edit button, the displayed data should turn into input fields.
     *
     *First, on page laod, we style all inputs to look like normal text, by setting them to disabled 
     *and adding the form-control-plaintext class*/

    turnDataEditOff();
    /*Prevent a brief flash of styled input fields by hiding the form until the fields
      have the plaintext class.*/
    $("#edit-user-data-wrapper").removeClass("invisible");

    $("#edit-user-password-edit-on").on('click', turnPasswordEditOn);

    $("#edit-user-password-edit-off").on('click', function(){
        $("#password-change-wrapper").slideUp();
    })

    $("#edit-user-data-edit-on").on('click', turnDataEditOn);
    $("#edit-user-data-edit-off").on('click', turnDataEditOff);
    $("#edit-user-password-edit-on").on('click', turnPasswordEditOn);
    $("#edit-user-password-edit-off").on('click', turnPasswordEditOff);

});



