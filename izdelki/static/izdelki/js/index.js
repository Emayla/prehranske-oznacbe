/*The code used for stretching background images also breaks the width of the top menu bar
which makes it look inconsistent with the rest of the site

Since this menu bar uses bootstrap, its with is normaly adjusted with bootstrap's javascript,
but is overwritten in the index page

This script forces the width of the menu bar to be what it should be*/

/*
The script is also used to directly position the arrows. The default css values only work 
when unzoomed at a horizontal resolution of 1080px.

*/
/*
function resizeTopMenuBar(){
	console.log("Ready called");
	console.log($(".content"));
	console.log("Width: " + $(".container").width() + 'px');
    //Add 30 px for 15px padding on each side
    let width = $(".container").width() + 30;
	$("#topMenuBar").css({
      'width': width + 'px'
    });
}
*/


$(document).ready(function() {
	//resizeTopMenuBar();
    //$(window).on('resize', $.debounce(200,resizeTopMenuBar));
});