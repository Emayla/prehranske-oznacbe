/*
    The conversion table between different mass units and energy value units.
 */
var conversionUnits = {
    "kcal": {"kcal": 1, "kJ": 4.184},
    "kJ" : {"kcal": 1 / 4.184, "kJ": 1},
    "kg" : {"kg": 1, "g": Math.pow(10,3), "mg": Math.pow(10,6), "mcg": Math.pow(10,9), "ng": Math.pow(10,12)},
    "g": {"kg": Math.pow(10,-3), "g": 1, "mg": Math.pow(10,3), "mcg": Math.pow(10,6), "ng": Math.pow(10,9)},
    "mg": {"kg": Math.pow(10,-6), "g": Math.pow(10,-3), "mg": 1, "mcg": Math.pow(10,3), "ng": Math.pow(10,6)},
    "mcg": {"kg": Math.pow(10,-9), "g": Math.pow(10,-6), "mg": Math.pow(10,-3), "mcg": 1, "ng": Math.pow(10,3)},
    "ng": {"kg": Math.pow(10,-12), "g": Math.pow(10,-9), "mg": Math.pow(10,-6), "mcg": Math.pow(10,-3), "ng": 1}
};



function convertUnits(value, oldUnits, newUnits) {
    // Converts the value from oldUnits to newUnits using the conversionUnits table.
    return value * conversionUnits[oldUnits][newUnits];
}

var portionSize = $('#port-size').text().replace(',','.');  // size of the portion in grams
portionSize = parseFloat(portionSize);

function getNutrientFromElementId(e) {
    // Returns the nutrient name from the id of element e.
    var id = e.attr('id').split('-');
    return id[0];
}

function getNutrientValue(nutrient, category) {
    /*
        Returns the value of nutrient in category. Categories can be
        - '100': denoting nutrient value per 100 g,
        - 'port': denoting nutrient value per portion,
        - 'per': denoting the percentage of recommended daily intake value.
     */
    var select = '#' + nutrient + '-' + category;


    //modified so that it can work with input fields when comparing
    var value;
    var selectChildInput = $(select).find('.compare-input');
    if (selectChildInput.length > 0){
        value = selectChildInput.val();
    } else {
         value = $(select).find('.value').text();
    }
    value = parseFloat(value.replace(',','.'));
    return value ? value : 0;
}

function getNutrientValueLabel(nutrient) {
    /*
        Returns the value of nutrient in category from the "Iz Označbe" interface.
        Since that interface uses input fields for nutrient values, the change code needs to be diferent
    */
    var select = '#' + nutrient;
    value = $(select).find('input').val();
    value = parseFloat(value.replace(',','.'));
    return value ? value : 0;
}

function setNutrientValue(nutrient, category, value, units=false) {
    /*
        Sets the value of nutrient in category to be equal to value. Categories can be
        - '100': denoting nutrient value per 100 g,
        - 'port': denoting nutrient value per portion,
        - 'per': denoting the percentage of recommended daily intake value.
    */
    var select = '#' + nutrient + '-' + category;
    //we don't need this anymore, since rounding is taken care of elswhere and sometimes needs 2 decimal spaces
    //we still want it for percentage display
    if (category == 'per'){
        value = +(value).toFixed(1);
    }
    else {
        value = roundNutrient(nutrient, value);
    }
    value = String(value).replace('.', ',');
    
    var selectChildInput = $(select).find('.compare-input')
    if (selectChildInput.length > 0){
        selectChildInput.val(value);
        selectChildInput.trigger('change');
    } else {
        $(select).find('.value').text(value);
        $(select).find('.value').trigger('change');
    }
    if (!units === false) {
        $(select).find('.units').text(units);
        $(select).find('.units').trigger('change');
    }
}

function setNutrientValueLabel(nutrient, value, units=false) {
    /*
        Returns the value of nutrient in category from the "Iz Označbe" interface.
        Since that interface uses input fields for nutrient values, the change code needs to be diferent
    */
    var select = '#' + nutrient;
    
    value = roundNutrient(nutrient, value);
    value = String(value).replace('.', ',');
    $(select).find('input').val(value);
    
    /*Calling change on this field does two things:
     *1. It triggers the nonempty and error listeners. 
     *This only matters when the user clicks the unit conversion button
     *on an empty field. In that case, the converted value will be zero, and we need to trigger the listeners in this case
     *(otherwise we get a 0 still covered up by the + background)
     *In any other case, we don't need to trigger this, since the background has already been cleared, and the conversion won't introduce errors
     *
     *2. It updates the originalValues array, which stores component values and is used for conversion. We do not wnat this value to be stored in this case,
     *since the unit conversion function hasn't updated the unit labels yet. This would mean that originalComponents array would get a value with the wrong units, which is bad.
     *In this case, this doesn't matter if the value is 0, since 0 is the same for all units
     **/
    if(value==0){
        $(select).find('input').trigger('change');
    }
    if (!units === false) {
        $(select).find('.units').text(units);
    }
}

function calculatePortionNutrients() {
    /*
        Calculates the nutrient value for each nutrient based on portion size.
     */
    $('[id$="-port"]').each(function () {
        var nutrient = getNutrientFromElementId($(this));
        var nutrientVal100 = getNutrientValue(nutrient, '100');
        var nutrientValPort = portionSize * nutrientVal100 / 100;
        //do we also rond the portion part of the components?
        nutrientValPort = roundNutrient(nutrient, nutrientValPort)
        
        setNutrientValue(nutrient, 'port', nutrientValPort);
    });

    //roundPortion();
}


//Javascript's round and toFixed() don't properly round up 0.5 in all cases
//this solution does (multiply by 10*decimals, round with Math.round, then divide by 10*decimals)
//http://www.jacklmoore.com/notes/rounding-in-javascript/
function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function roundNutrient(nutrient, value){
    var nutrientValPort = 0;
    switch (nutrient){
        case 'enval':
            nutrientValPort = round(value,0);
            break;
        
        case 'fat'    :
        case 'chot'   :
        case 'sugar'  :
        case 'prot'   :
        case 'fibt'   :
        case 'starch' :
            if (value <= 0.5){
                nutrientValPort = 0;
            } else if (value < 10){
                nutrientValPort = round(value,1);
            } else {
                nutrientValPort = round(value,0);
            }
            break;

        case 'fasat':
        case 'fams' :
        case 'fapu' :
            if (value <= 0.1){
                nutrientValPort = 0;
            } else if (value < 10){
                nutrientValPort = round(value,1);
            } else {
                nutrientValPort = round(value,0);
            }
            break;   

        case 'salt':
            if (value <= 0.0125){
                nutrientValPort = 0;
            } else if (value < 1){
                nutrientValPort = round(value,2);
            } else {
                nutrientValPort = round(value,1);
            }
            break;

        case 'vita'    :
        case 'folacid' :
        case 'ca'      :
        case 'chd'     :
        case 'p'       :
        case 'mg'      :
        case 'id'      :
        case 'k'       :
            nutrientValPort = round(value,3);
            break;

        default:
            nutrientValPort = round(value,2);
            break;

    }
    return nutrientValPort;
}



var envalkcal = getNutrientValue('enval', '100');

// Calculates the nutrients based on portion size.
calculatePortionNutrients();

// Changes nutrient units in the per 100g and per portion columns based on the unit the user has chosen.
$(document).on('click', '.unit-change li', function () {
    var newUnits = $(this).text();
    var nutrient = getNutrientFromElementId($(this).parent());
    if (nutrient.indexOf("Label") > -1){
        convertUnitsLabel(this);
        return;
    }
    var oldUnits = $('#' + nutrient + '-100').find('.units').text();
    var newValue100;
    var newValuePort;

    if (nutrient === 'enval') {
        if (newUnits === 'kcal') newValue100 = envalkcal;
        else newValue100 = envalkJ;

        //are we converting the compare value?
        var select = '#' + nutrient + '-' + 'port';
        var selectChildInput = $(select).find('.compare-input')
        if (selectChildInput.length > 0){
            var valuePort = getNutrientValue(nutrient, 'port');
            newValuePort = convertUnits(valuePort, oldUnits, newUnits);
        } else {
            newValuePort = portionSize * newValue100 / 100;
        }


    }
    else {
        //Do conversion from the original values (in g)
        //When the units are in grams, we lose some precision due to rounding
        //(if real weight = 1234mg, the gram value will just show 1,2g)
        //
        //Because of this, we can't just convert from the browser stored values
        //Instead, we always remember the original exact weight and convert based on that
        //This weights are stored in originalComponents

        //Always use the uppercase nutrient string when accessing originalComponents, 
        //since that's what we get from the server
        var nutrientUpper = nutrient.toUpperCase();
        //originalComponents stores the SALT component under NA
        if (nutrientUpper === "SALT") nutrientUpper = "NA"
        if  (window.hasOwnProperty("originalComponents") && originalComponents && originalComponents[nutrientUpper]){
            var value100 = originalComponents[nutrientUpper];
            //salt = 2.5*NA
            if (nutrientUpper === "NA") {
                value100 = value100 * 2.5;
            }
            
            //originalComponents always stores values in g, so convert from g
            newValue100 = convertUnits(value100, 'g', newUnits);
        } else {
            //If it isn't stored in originalComponents yet, store it
            var value100 = getNutrientValue(nutrient, '100');
            var value100g = convertUnits(value100, oldUnits, 'g');
            if (nutrientUpper === "NA") {
                value100g = value100g / 2.5;
            }
            originalComponents[nutrientUpper] = value100g;
            newValue100 = convertUnits(value100, oldUnits, newUnits);
        }
    }

    setNutrientValue(nutrient, '100', newValue100, newUnits);
    //setNutrientValue(nutrient, 'port', newValuePort, newUnits);
    calculatePortionNutrients();
    $('#' + nutrient + '-unit').text(newUnits);
    $('#' + nutrient + '-port').find('.units').text(newUnits);
});


function convertUnitsLabel(obj){
    var nutrient = getNutrientFromElementId($(obj).parent());
    var LABEL_STRING_LEN=5;
    nutrientUpper = nutrient.toUpperCase();
    nutrientUpper = nutrientUpper.substring(0,nutrient.length - LABEL_STRING_LEN);
    if (nutrientUpper === "SALT") nutrientUpper = "NA"
    if (window.hasOwnProperty("originalComponents") && originalComponents && originalComponents[nutrientUpper]){
        convertUnitsLabelWithOriginal(obj);
    } else {
        convertUnitsLabelBasic(obj);
    }
    //Either way, update the label after the input that shows the units
    var newUnits = $(obj).text();
    var nutrient = getNutrientFromElementId($(obj).parent());
    //the nutrient string above is of the form nutrientLabel
    //to get just the nutrient, we need to cut off the last 5 letters
    var LABEL_STRING_LEN=5;
    nutrient = nutrient.substring(0,nutrient.length - LABEL_STRING_LEN);
    $('#' + nutrient + '-unit-label').text(newUnits)
}


/**
 * Converts a label nutrient value to the new unit
 *
 * This variant uses stored original values to do the conversion, 
 * so that precision is not lost when converting up and then rounding the result
 * @param  {DOM Object} obj The DOM Object dontaining the units we are converting to
 */
function convertUnitsLabelWithOriginal(obj) {
    var newUnits = $(obj).text();
    var nutrient = getNutrientFromElementId($(obj).parent());
    //the nutrient string above is of the form nutrientLabel
    //to get just the nutrient, we need to cut off the last 5 letters
    var LABEL_STRING_LEN=5;
    var nutrientName = nutrient.substring(0,nutrient.length - LABEL_STRING_LEN);
    nutrientName = nutrientName.toUpperCase();
    
    if (nutrientName === "NA"){
        var value100 = originalComponents["NA"] * 2.5;
        //change the name to salt so it works properly with the round method
        nutrientName = "SALT";
    } else {
        var value100 = originalComponents[nutrientName];
    }
    
    //The original values are always stored in g, since that's what the API returns
    var oldUnits = 'g';
    var newValue100;
    newValue100 = convertUnits(value100, oldUnits, newUnits);
    newValue100 = roundNutrient(nutrientName.toLowerCase(),newValue100)
    setNutrientValueLabel(nutrient, newValue100);
    $('#' + nutrient + '-unit').text(newUnits);

}

function convertUnitsLabelBasic(obj) {
    var newUnits = $(obj).text();
    var nutrient = getNutrientFromElementId($(obj).parent());

    var LABEL_STRING_LEN=5;
    var nutrientName = nutrient.substring(0,nutrient.length - LABEL_STRING_LEN);
    nutrientName = nutrientName.toUpperCase();
    
    
    var oldUnits = $('#' + nutrient + '-unit').text();
    var newValue100;
    var value100 = getNutrientValueLabel(nutrient);
    var value100g = convertUnits(value100, oldUnits, 'g');
    if (nutrientName === "NA") value100g = value100g / 2.5;
    originalComponents[nutrientName] = value100g;

    newValue100 = convertUnits(value100, oldUnits, newUnits);

    setNutrientValueLabel(nutrient, newValue100);
    $('#' + nutrient + '-unit').text(newUnits);
}
