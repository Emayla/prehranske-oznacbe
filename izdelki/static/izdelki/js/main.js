//Methods for getting and setting cookiess
//https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

//https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

/**
 * Removes the specified element from the array
 * @param  {array} array    - the array to remove from
 * @param  {object} element - the element to remove
 * @return {array}          - the input array without the provided element
 */
function removeElementFromArray(array, element){
    var index = array.indexOf(element);
    if (index > -1) {
        array.splice(index, 1);
    }
    return array
}


// Array for HTML escaping.
var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '`': '&#96;',
    ' ': '&#32;',
    '!': '&#33;',
    '@': '&#64;',
    '$': '&#36;',
    '%': '&#37;',
    '(': '&#40;',
    ')': '&#41;',
    '=': '&#61;',
    '+': '&#41;',
    '{': '&#123;',
    '}': '&#125;',
    '[': '&#91;',
    ']': '&#93;',
    '/': '&#47;',
    '\\': '&#92;'
};

// Escapes HTML in user input.
function escapeHtml (string) {
    return String(string).replace(/[&<>"'` !@$%()=+{}[\]/\\]/g, function (s) {
        return entityMap[s];
    });
}

// Escapes quotes in user input.
function escapeQuotes (string) {
    return string.replace(/(["'`])/g, '\\$1');
}


// Escapes the name of a selector. Should not be used for names where . or # means actual class or id, respectively.
function escapeSelector (selector_name) {
    return selector_name.replace( /([:.[\],=@!"#$%&'*+/;<>?\\^`{|}~()])/g, '\\$1' );
}

(function ($) {
    $.fn.initializePopover = function () {
        this.popover({
            html : true,
            content: function() {
              var content = $(this).attr("data-popover-content");
              return $(content).children(".popover-body").html();
            },
            title: function() {
              var title = $(this).attr("data-popover-content");
              return $(title).children(".popover-heading").html();
            }
        });
        return this;
    };
})(jQuery);

//The pageshow event is similar to onload, but will also be ran
//when the user loads the page from the browser cache 
//(such as when clicking the back button on the browser)
//
//Since we want to display warnings for unsaved data if the user clicks back
//we need the warning display code to go here, not into the onload function
/*
window.addEventListener('pageshow', function(event) {
    var currentPage = window.location.pathname.split('/')[1];
    //Show warning if the user left the edit page without saving his data
    var prevPage = sessionStorage.getItem("prevPage");
    console.log(currentPage);
    console.log(prevPage);
    if (prevPage === "add" || prevPage==="edit"){
        //We don't want to show the warning on the add/edit page
        if (currentPage !== "add" && currentPage !== "edit"){
            $('#edit-cancel-warning').show();
        }
    } else {
        //we need to explicitly hide the warning, otherwise it will keep being shown
        //more than once if the user presses the back/forward button
        $('#edit-cancel-warning').hide();
    }

    //Now save the current page so that the warning is only displayed once
    try {
        sessionStorage.setItem("prevPage", currentPage, 0.1)
    } catch (error){
        //MDN: https://developer.mozilla.org/en-US/docs/Web/API/Storage/setItem#Exceptions
        console.log("Session storage isn't supported");
    }
    
});
*/

function toggleSearch() {
    $("#search-new").toggle();
    $('#search-new').toggleClass('show-search');
    $('#search-button').toggleClass('show-search');

    //The dim doesn't dim the username (since it was made just for the login interace)
    //maybe it would be better to create a new dim class with an even bigget z-index that will
    //only show the search?
    //$('#dim-search').fadeToggle('fast');

    if ($('#search-new').hasClass('show-search')) {
        $('#search-input').focus();
    }
    
}

function hideSearch(){
    //$('#dim-search').hide();
    //$('#search').removeClass('show-search');
    $('#search-new').removeClass('show-search');
    $("#search-new").hide();
}


/**
 * Adjust right margin on pages without a scrollbar to match the pages with a scrollbar
 *
 * Adjusted from https://stackoverflow.com/a/18548909
 */
function fixScrollbarWidth(){
    var $body = $('body');
    var scrollHeight = $body[0].scrollHeight;
    var visibleHeight = $body.height();
    if(scrollHeight<=visibleHeight){
        $('#wrapper').addClass('scrollbar-fix');
    }
}

$(function(){
    // Initializes all popovers, makes popover HTML a lot neater.
    $("[data-toggle=popover]").each(function () {
        $(this).initializePopover();
    });

    fixScrollbarWidth();

    //Create search buton event listener. The search bar should slide out on button click
    
    $('#search-button').on('click', function(){
        toggleSearch();
    });

    //add a function to remove the dim
    $('body').on('click', function (e) {
    var clickedOn = $(e.target);
    if (!(clickedOn.parents().andSelf().is('#search-new') 
        || clickedOn.parents().andSelf().is('#search-button'))) {
            if ($('#search-new').hasClass('show-search')) {
                hideSearch();
        }
    }
    });
});

//Cookie notice
//
//Copyright (c) 2015 Silktide Ltd
//MIT LICENCED
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#edeff5",
      "text": "#838391"
    },
    "button": {
      "background": "#4b81e8"
    }
  },
  "content": {
    "message": "Spletno mesto uporablja piškotke, s katerimi izboljšujemo uporabniško izkušnjo.",
    "dismiss": "Strinjam se",
    "link": "Več informacij",
    "href": "/terms#cookies"
  }
})});


