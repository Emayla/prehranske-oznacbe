var compareEnabled


//make the default value of compareEnable false
$(function () {
    compareEnabled = false
});



//Saves the values of the header before the user starts comparing
//so that these values can be restored wen comparisson stops
function storeEditData(){
    $("#port-header").data('oldHTML', $("#port-header").html());
    $("#port-header").data('portion', $('#inputPortionSize').val());
    $("#port-header").html('izmerjena vrednost');

    $("#port-percent-header").data('oldHTML', $("#port-percent-header").html());
    $("#port-percent-header").html('odstopanje');
}



//Callback function called whenever a compare value is changed by the user
//Calculates the difference (deviation) between the database value and user's value
//and displays it in the third table column. Also higlights differences that are higher than allowed.
function compareInputListener(){
    baseID=this.id.split('-')[0];
    //the value we are comparing to (so the value in the 100g collumn)
    var storedValueString = '#'+baseID+'-100 > span.value'
    var percentString = '#'+baseID+'-per'
    var storedValue = $(storedValueString).html();
    storedValue=parseFloat(storedValue.replace(',','.'));
    
    var userValue = $(this).val();
    userValue=parseFloat(userValue.replace(',','.'));
    
    var diff = Math.abs(userValue-storedValue);
    var percentDiff = diff/storedValue * 100;
    //update the percent cell
    var percentDiffString = percentDiff.toFixed(1).replace('.',',');
    if (percentDiffString.includes("Infinity") || percentDiffString.includes("NaN")){
        //todo: what exactly to display here. Probably not the original english text
        percentDiffString='-';
    }
    var oldSpan = $(percentString + ' span')[0].outerHTML;
    $(percentString).html(percentDiffString + " % " + oldSpan);
    //we need to reinitialize the tooltip, since we now have a new span element
    addInfoTooltip(baseID);

    //highlight deviations that are too large
    /*
    if (percentDiff > 20 || isNaN(percentDiff)) {
        $(percentString).css('font-weight', 'bold');
        $(percentString + ' span').css('color','black');
    } else {
        $(percentString).css('font-weight', 'normal');
        $(percentString + ' span').css('color','grey');
    }
    */

    if (deviationTooLarge(percentDiff,baseID, storedValue, userValue)) {
        $(percentString).css('font-weight', 'bold');
        $(percentString + ' span').css('color','black');
    } else {
        $(percentString).css('font-weight', 'normal');
        $(percentString + ' span').css('color','grey');
    }


}





//Data from: http://www.uvhvvr.gov.si/fileadmin/uvhvvr.gov.si/pageuploads/DELOVNA_PODROCJA/Zivila/oznacevanje_zivil/smernicetolerance.pdf
//Todo: update logic and tooltips with inal values
function deviationTooLarge(percentDiff, nutrient, baseValue, compareValue){
var absoluteDiff=Math.abs(baseValue-compareValue)
    switch(nutrient){
        case 'fat':
            if (baseValue<10)
                return (absoluteDiff > 1.5);
            else if (baseValue<40)
                return (percentDiff>20);
            else
                return (absoluteDiff > 8);
            break;
        
        case 'fasat':
            if (baseValue<4)
                return (absoluteDiff > 0.8);
            else 
                return (percentDiff>20);
            break;

        case 'chot':
        case 'sugar':
        case 'prot':
            if (baseValue<10)
                return (absoluteDiff > 2);
            else if (baseValue<40)
                return (percentDiff>20);
            else
                return (absoluteDiff > 8);
            break;
        
        case 'salt':
            if (baseValue<1.25)
                return (absoluteDiff > 0.375);
            else
                return (percentDiff>20);
            break;
        
        default:
            return (percentDiff>20);
            break;

    }

}


//Sets the messages that tell the user what the allowed deviations are
function addInfoTooltips(){    
    //$("#enval-per span").attr("title", "Dovoljeno odstopanje: ± 20%");
    
    $("#fat-per   span").attr("title", "Dovoljeno odstopanje:\n"+
                                "Za količine < 10g: ±1,5g\n"+
                                "Za količine < 40g: ±20%\n"+
                                "Za količine > 40g: ±8g");
    
    $("#fasat-per span").attr("title", "Dovoljeno odstopanje:\n"+
                                  "Za količine < 4g: ±0,8g\n"+
                                  "Za količine > 4g: ±20%");
    
    $("#chot-per  span").attr("title", "Dovoljeno odstopanje:\n"+
                              "Za količine < 10g: ±2g\n"+
                              "Za količine < 40g: ±20%\n"+
                              "Za količine > 40g: ±8g");
    
    $("#sugar-per span").attr("title", "Dovoljeno odstopanje:\n"+
                              "Za količine < 10g: ±2g\n"+
                              "Za količine < 40g: ±20%\n"+
                              "Za količine > 40g: ±8g");
    
    $("#prot-per  span").attr("title", "Dovoljeno odstopanje:\n"+
                              "Za količine < 10g: ±2g\n"+
                              "Za količine < 40g: ±20%\n"+
                              "Za količine > 40g: ±8g");
    
    $("#salt-per  span").attr("title", "Dovoljeno odstopanje:\n"+
                              "Za količine < 1,25g: ±0,375g\n"+
                              "Za količine > 1,25g: ±20%");
    
    //default for those not added above
    $("#listComponents tr td[id$='-per'] span").not('[title]').attr("title", "Dovoljeno odstopanje: ±20%");


    $('span[title]').tooltip({ 
        position: {my: 'left center', at: 'right+10 center'}
    });

}

//same as above, only adds a tooltip to one element
//takes a nutrient name and gives it a correct tooltip and text
function addInfoTooltip(nutrient){
    switch(nutrient){
        case 'enval':
            $("#enval-per span").attr("title", "Dovoljeno odstopanje: ± 20%");
            $('#enval-per span').tooltip({ 
                 position: {my: 'left center', at: 'right+10 center'}
            });
            break;

        case 'fat':
            $("#fat-per span").attr("title", "Dovoljeno odstopanje:\n"+
                                        "Za količine < 10g: ±1,5g\n"+
                                        "Za količine < 40g: ±20%\n"+
                                        "Za količine > 40g: ±8g");
            $('#fat-per span').tooltip({ 
                 position: {my: 'left center', at: 'right+10 center'}
            });
            break;
            
        case 'fasat':
            $("#fasat-per span").attr("title", "Dovoljeno odstopanje:\n"+
                                          "Za količine < 4g: ±0,8g\n"+
                                          "Za količine > 4g: ±20%");
            $('#fasat-per span').tooltip({ 
                 position: {my: 'left center', at: 'right+10 center'}
            });
            break;

        case 'chot':
            $("#chot-per  span").attr("title", "Dovoljeno odstopanje:\n"+
                                      "Za količine < 10g: ±2g\n"+
                                      "Za količine < 40g: ±20%\n"+
                                      "Za količine > 40g: ±8g");
            $('#chot-per span').tooltip({ 
                 position: {my: 'left center', at: 'right+10 center'}
            });
            break;

        case 'sugar':    
            $("#sugar-per span").attr("title", "Dovoljeno odstopanje:\n"+
                                      "Za količine < 10g: ±2g\n"+
                                      "Za količine < 40g: ±20%\n"+
                                      "Za količine > 40g: ±8g");
            $('#sugar-per span').tooltip({ 
                 position: {my: 'left center', at: 'right+10 center'}
            });
            break;

        case 'prot':
            $("#prot-per  span").attr("title", "Dovoljeno odstopanje:\n"+
                                      "Za količine < 10g: ±2g\n"+
                                      "Za količine < 40g: ±20%\n"+
                                      "Za količine > 40g: ±8g");
            $('#prot-per span').tooltip({ 
                 position: {my: 'left center', at: 'right+10 center'}
            });
            break;

        case 'salt':
            $("#salt-per  span").attr("title", "Dovoljeno odstopanje:\n"+
                                      "Za količine < 1,25g: ±0,375g\n"+
                                      "Za količine > 1,25g: ±20%");
            $('#salt-per span').tooltip({ 
                 position: {my: 'left center', at: 'right+10 center'}
            });
            break;
        
        default:
            $("#"+nutrient+"-per span").attr("title", "Dovoljeno odstopanje: ± 20%");
            $("#"+nutrient+"-per span").tooltip({ 
                 position: {my: 'left center', at: 'right+10 center'}
            });
            break;
    }



}

//Sets the second column (compare inputs) when the user starts comparing
function setCompareInputs(){
    $('td[id*=-port]').each(function(){
        $(this).data('oldHTML', $(this).html())
        if (this.id == "enval-port"){
            $(this).html("<p class = 'text-center-no-margin'> - </p>");
        } else {
            $(this).html("<input id='"+this.id+"' class='compare-input' value=0 type='text'> <span class = units> g</span>");
        }            
    });
}



//Sets the third column (deviation precentage) when the user starts comparing
function setComparePercent(){
    $('td[id*=-per]').each(function(){
        $(this).data('oldHTML', $(this).html())
        if (this.id == "enval-per"){
            $(this).html("<p class = 'text-center-no-margin'> - </p>");
        } else {
            $(this).html("0 % <span class='glyphicon glyphicon-exclamation-sign'/>");
        }
        $("span", this).css('color', 'grey');       
    });
}



//Changes the color of the compare button to indicate if the user is currently comparing or not
function toggleCompareButton(compare){
    if(compareEnabled === true){
        compare.css('background-color','#91c2ff');
        compare.css('color','#e4f0ff');
    } else {
        compare.css('background-color','#e4f0ff');
        compare.css('color','#004cc5');
    }
}


//Restores all table components to the values they had before editing started
function restoreComponents(){
    $("#port-header").html($("#port-header").data('oldHTML'));
    $("#port-percent-header").html($("#port-percent-header").data('oldHTML'));
    $('#inputPortionSize').val($("#port-header").data('portion'));
    
    $('td[id*=-port]').each(function(){
        $(this).html($(this).data('oldHTML'));       
    })

    $('td[id*=-per]').each(function(){
        $(this).html($(this).data('oldHTML'));    
        //css styling (bold fonts) isn't restored by html, so restore it separately
        $(this).css('font-weight', 'normal');   
    })
    
    //also restore the listener function
    $('#inputPortionSize').on('input', function(){
        var val = $(this).val().replace(',','.');
        portionSize = parseFloat(val);
        refreshComponents();
    });

}

$('#compareProduct').click(function() {
    if (!compareEnabled){
        compareEnabled=true;
        toggleCompareButton($(this));
        storeEditData();
        setCompareInputs();
        $('.compare-input').on('input',compareInputListener);
        setComparePercent();
        addInfoTooltips();
    } else {
        compareEnabled=false;
        toggleCompareButton($(this));
        restoreComponents();
    }
});
