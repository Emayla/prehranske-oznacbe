var item_to_be_deleted = "";
var LABEL_INGREDIENT_PREFIX = "IND";


function setupProductTypeToggle(){
    $("#dashboard-select-recipe-wrapper").on("click", function(){
        $("#dashboard-labels").hide();
        $("#dashboard-recipes").show();
    });

    $("#dashboard-select-label-wrapper").on("click", function(){
        $("#dashboard-recipes").hide();
        $("#dashboard-labels").show();
    });
}

/*
$('.infinite-container').on('click', '.dash-img-container-empty-add', function(event) {
    var id = $(this).attr('id');
    var product_code= id.match("^product-img-dash-(.+)")[1]; 
    $('#pick-photo-' + product_code).trigger('click');
    
});
*/

/*Also do picture upload for existing pictures*/

/*
$('.infinite-container').on('click', '.dash-img-container', function(event) {
    var id = $(this).attr('id');
    var product_code= id.match("^product-img-dash-(.+)")[1]; 
    $('#pick-photo-' + product_code).trigger('click');
    
});
*/


/*

$('.product-box').click(function() {
    var id = $(this).children(":first").attr('id');
    var product_code= id.match("^product-img-dash-(.+)")[1]; 
    location.href= '../id/' + product_code

});


*/

$('.infinite-container').on('click', '.dashboard-delete-item', function(){
    var id = $(this).attr('id')
    var item_id = id.substring(0,id.indexOf('-'))
    item_to_be_deleted = item_id;
    $('#deleteItemModal').modal();
});

$('#dashboardDeleteItemConfirm').click(function(){
    $('#deleteError').hide();
    $('#deleteLabelInredientError').hide();
    
    if (item_to_be_deleted.substring(0,3) === LABEL_INGREDIENT_PREFIX){
        $('#deleteLabelInredientError').show();
        $('#deleteItemModal').modal('hide');
        return;
    }
    parameters =  [];
    parameters.push({name: 'csrfmiddlewaretoken', value: csrf_token})
    parameters.push({name: 'product_id', value: item_to_be_deleted})
    $(this).text("Brisanje poteka ...")
    $(this).attr('disabled', true);
    //$('#deleteItemModal').modal('hide');
    
    $.post('/delete/', $.param(parameters))
        .done(function (response) {
            location.reload(true);
        })
        .fail(function (response) {
            $('#deleteError').show();
            $('#deleteItemModal').modal('hide');
        });
    item_to_be_deleted = "";
});

/*
$('.pick-photo-dash').change(function() {
    var input = this;
    var data = [];
    var id = $(this).attr('id');
    var product_code= id.match("^pick-photo-(.+)")[1]; 
    data.push({name: 'code', value:product_code})
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            selectedPhoto = e.target.result
            if (selectedPhoto) {
                data.push({name: 'photo', value: selectedPhoto});
                post('/dashboard/submit',data)
            }
        }

        reader.readAsDataURL(input.files[0]);
    }
});
*/


function post(path, parameters) {
    parameters.push({name: 'csrfmiddlewaretoken', value: csrf_token});
    $.post(path, $.param(parameters))
        .done(function (response) {
            if (response.redirect)
            window.location.replace(response.redirect);
        })
        .fail(function (response) {
            // var jsonError = JSON.parse(jqXHR.responseText);
            // console.log('Error when submitting product: ' + jsonError.message);
            $('#recipeFormData').replaceWith(response.responseText);
            $('#saveProduct').attr('disabled', false);
        });
}

function moveItemsToLabelColum(){
    /**
     * Moves all labels into the correct column and shows them
     *
     * 
     * The current infinite scroll library has some limitations that make it hard to work with in the current two column design
     * In particular, it expects a single container and items marked with the "inifinite-item" class.
     *These items are then simply appended to the container
     *
     *
     * The current solution is:
     * 1. Have 2 columns. One is the container, and will get all new infinite items. The other is initialy empty
     * 2. On page load and on every refresh, move all labels from the first column to the second column
     * (these items are marked with the class "move-to-labels" and are initially hidden)
     */
     $('.move-to-labels').detach().appendTo("#dashboard-labels").removeClass('move-to-labels');
     $('#dashboard-labels').on('click', '.dashboard-delete-item', function(){
        var id = $(this).attr('id')
        var item_id = id.substring(0,id.indexOf('-'))
        item_to_be_deleted = item_id;
        $('#deleteItemModal').modal();
     });
}

/*Infinite scroll*/
var infinite = new Waypoint.Infinite({
    element: $('.infinite-container')[0],
    onBeforePageLoad: function () {
        $('.loading').show();
    },
    
    onAfterPageLoad: function ($items) {
        $('.loading').hide();
        moveItemsToLabelColum();
        //generateEllipsis($items);
    }
});


$( document ).ready(function() {
    moveItemsToLabelColum();
    setupProductTypeToggle();
});