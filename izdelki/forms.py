from django import forms
from django.forms.widgets import SelectMultiple
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe

import re


common_field_errors = {'required': _('To polje je obvezno.'), 'invalid': _('Vsebina tega polja ni veljavna.'),
                       'invalid_choice': _('Izbira ni veljavna.')}


def calculate_barcode_checksum (barcode):
    """
    Takes in a barcode (without the checksum digit) and calculates the checksum
    
    For the details of the checksum, see: https://www.gs1.org/services/how-calculate-check-digit-manually
    The calculated checksum should always match the results from: https://www.gs1.org/services/check-digit-calculator
    
    Args:
          barcode (str): The barcode, without the checksum digit.
  
    Returns:
          str: The checksum digits
    """
    total_sum = 0
    multiplier = 3
    for digit in reversed(barcode):
      total_sum += multiplier * int(digit)
      multiplier = 1 if multiplier == 3 else 3
    sum_last_digit = total_sum % 10
    checksum = sum_last_digit if sum_last_digit == 0 else 10 - sum_last_digit
    return str(checksum)

def validate_barcode_checksum(barcode):
    #first, we need to check if the barcode only consists of digits
    #because Djano will run all validators before returning the errors
    #So even if the numberic regex validator throws an error (because the barcode isn't a number)
    #This validator will still run
    regex_string = r'^\d{13}$'

    if not re.match(regex_string, barcode):
        #The other validator will catch this error
        #We can just return, since there's no point in trying to validate the checksum
        #when the barcode isn't a number (and the checksum function will throw an exception if we try)
        return

    check_digit = barcode[-1]
    other_digits = barcode[:-1]
    checksum = calculate_barcode_checksum(other_digits)
    if check_digit != checksum:
        raise ValidationError(
            _('Črtna koda ni ustrezna - napačna kontrolna števka.')
        )


def validate_neto_weight(weight):
    weight = weight.replace(",",".")
    try:
        weight = str(float(weight))
    except:
        raise ValidationError(
            _('Neto količina mora biti izražena kot število, brez dodatnih črk.')
        )
    print(weight)
    print("weight")
    if re.match("^\d+?\.?\d+?$", weight) is None:
        raise ValidationError(
            _('Neto količina mora biti izražena kot število.')
        )





class RecipeForm(forms.Form):
    label_suffix = ''
    required_css_class = 'required'
    commercial_name = forms.CharField(label=_('Prodajno ime'), 
                                      help_text='Prodajno ime je ime, pod katerim se živilo daje v promet.')
    name = forms.CharField(label=_('Naziv živila'), required=False, 
                           help_text='Naziv živila je opisno ime izdelka.')
    group = forms.ChoiceField(label=_('Skupina živila'), choices=(), 
                              help_text='Skupina živila je določena v skladu s klasifikacijo OPKP.' )
    country = forms.CharField(label=_('Država porekla'), required=False, 
                              help_text='Država porekla je lahko tudi “Kmetijstvo EU” ali “Kmetijstvo izven EU”.')
    neto_weight = forms.CharField(label=_('Neto količina (g)'), required=False, validators=[validate_neto_weight])
    barcode = forms.CharField(label=_('Črtna koda'), required=False,
                              max_length=13, 
                              help_text='Če vnesete črtno kodo, mora vsebovati GS1 predpono vašega podjeta, ki jo določite preko nastavitev vaših podatkov. ', 
                              validators=[
                                  RegexValidator(
                                      regex=r'^\d{13}$',
                                      message=_(mark_safe('Črtna koda mora biti sestavljena le iz števk in mora biti dolga 13 znakov.<br /><br />  Če ima vaš izdelek črtno kodo drugačne oblike, ga shranite brez črtne kode in nam sporočite izdelek in njegovo črtno kodo na <a href="mailto:info@prehranskeoznacbe.si"> info@prehranskeoznacbe.si</a>')),
                                      code=_('invalid_barcode')),
                                  validate_barcode_checksum,
                              ])
    is_public = forms.BooleanField(label=_('Dovolim objavo informacij v javni bazi'), required=False, 
                                   help_text='')

    def __init__(self, *args, **kwargs):
        group_choices = kwargs.pop('group_choices', None)
        self.barcode_prefix = kwargs.pop('barcode_prefix', None)
        self.all_barcodes = kwargs.pop('all_barcodes', [])
        self.current_barcode = kwargs.pop('current_product_barcode', None)
        kwargs.setdefault('label_suffix', '')
        super(RecipeForm, self).__init__(*args, **kwargs)

        if group_choices is not None:
            self.fields['group'].choices = group_choices

        for k, field in self.fields.items():
            field.error_messages = common_field_errors

    def clean_neto_weight(self):
        data = self.cleaned_data['neto_weight']
        return data.replace(",",".")

    def clean_barcode(self):
        barcode = self.cleaned_data['barcode']
        country = self.cleaned_data['country'].lower()

        #First, check if the barcode prefix matches
        #Barcode formats less than 13 places long dont use the GS1 prefix 
        #(Example: EAN-8, which is 8 characters long and uses only the country prefix)
        #In that case, don't validate the prefix
        if len(barcode) < 13:
            return barcode

        if self.barcode_prefix:
            if not barcode.startswith(self.barcode_prefix):
                raise ValidationError(
                    _('Črtna koda se mora začeti z GS1 predpono podjetja.')
                )



        #Now, check for uniqueness. A product can't have the same barcode as another product in the database
        #If the two products share a country (the problem is what to do when a country is None)
        #Saying two products without a country have the same country isn't right.
        #Should we just ignore products like this? (That's what we currently do)
        #The problem is that there aren't a lot of products with both a barcode and a country
        #self.all_barcodes is an array of tuples (barcode, country)
        #First, find a list of all tuples with the same barcode
        
        #First, check if this form is editing an existing product with an unchanged barcode.
        #If it is, we skip the check
        #
        #Current_barcode will be None on new products, or the barcode of the saved products on edits
        #barcode is the submitted new barcode in the form
        if self.current_barcode == barcode:
            return barcode

        barcode_country = (barcode, country)
        print("Searching for: {}".format(barcode_country))
        if barcode_country in self.all_barcodes:
            print("Found barcode match")
            raise ValidationError(
                _('vnešena črtna koda za to državo že obstaja v podatkovni bazi.')
            )

        
        return barcode




