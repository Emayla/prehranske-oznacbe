﻿from django.http import HttpResponseNotFound, HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.decorators.http import require_POST
from django.urls import reverse
import json
from django.conf import settings
from izdelki.auth_forms import BasicUserForm, CompanyUserProfileForm, BasicLoginForm, EditUserForm, \
                               EditUserEmailOnlyForm, PrivacyForm
from izdelki.decorators import anonymous_required
from izdelki.product import calculate_values, prepare_components, calculate_percentages, calculate_recipe_values, \
    find_product_diets_and_ingredients, round_component_values, calculate_portion, get_extra_components, convert_to_default_units, \
    make_https_photo

from izdelki.utilities import read_products, count_products, count_producers, add_product_to_file, find_product_by_id, \
    food_group_for_select, build_url, product_for_edit, photo_valid, resize_photo, send_register_confirm_email,\
    mark_product_as_deleted, get_all_barcodes, mark_label_products, create_select2_product_json, add_components_to_file, AJPES_check_company_id, \
    generate_tooltips, get_unit_change_components


from izdelki.setup import database_floats, database_ints, allergens
from izdelki.api import API
from izdelki.models import BasicUser, CompanyUserProfile
from izdelki.forms import RecipeForm
from izdelki.product import  write_product_to_xls
from izdelki.model_utilities import handle_email_change
from base64 import decodebytes
import logging
import locale
from easy_pdf.views import PDFTemplateView, PDFTemplateResponseMixin
from django.views.generic.base import TemplateView
from django.contrib.staticfiles.storage import staticfiles_storage


from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from izdelki.tokens import account_activation_token
from django.utils.encoding import force_bytes, force_text

from django.views.decorators.csrf import csrf_exempt


import re
import mimetypes
from xhtml2pdf import pisa
from  django.template.loader import get_template
from django.template import Context 
import os
import unicodedata
from decimal import Decimal
import requests


from io import BytesIO

from izdelki import view_utilities


#which components in the component table should have the unit change ui enabled
#They should be: salt and enval + all optional components that don't have a default unit of grams
UNIT_CHANGE_CODES = get_unit_change_components()


def index(request):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})
    user = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)
    return render(request, 'izdelki/index.html', {'user': user, 'product_no': count_products(), 'user_no':
        BasicUser.objects.count(), 'producer_no': count_producers()})


@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


@anonymous_required
def register(request):
    login_form = BasicLoginForm()

    if request.method == 'POST':
        basic_form = BasicUserForm(request.POST, user_type=request.POST['user_type'])
        company_form = CompanyUserProfileForm(request.POST)
        privacy_form = PrivacyForm(request.POST)



        if basic_form.is_valid() and privacy_form.is_valid():
            new_user = basic_form.save(commit=False)
            
            if request.POST['user_type'] == 'business':
                if company_form.is_valid():
                    new_profile = company_form.save()
                    new_user.company_profile = new_profile
                    new_user.is_company = True
                else:
                    return render(request, 'izdelki/register.html',
                                  {'form_basic': basic_form, 'form_company': company_form, 'form_privacy': privacy_form})
            
            new_user.save()
            #print(new_user)
            #new_user = authenticate(username=basic_form.cleaned_data['email'],
            #                        password=basic_form.cleaned_data['password1'],
            #                        )
            #login(request, new_user)

            send_register_confirm_email(new_user, basic_form.cleaned_data['email'], request)
            return HttpResponseRedirect(reverse('register_done'))
        else:
            return render(request, 'izdelki/register.html', {'form_basic': basic_form, 'form_company': company_form,
                                                             'form_privacy': privacy_form})
    else:
        basic_form = BasicUserForm()
        company_form = CompanyUserProfileForm()
        privacy_form = PrivacyForm
    return render(request, 'izdelki/register.html', {'form_basic': basic_form, 'form_company': company_form,
                                                     'form_privacy': privacy_form})
@login_required
def edit_user(request):
    password_errors = request.session.get('password_errors', None)
    request.session['password_errors'] = None
    login_form = BasicLoginForm()
    password_form = PasswordChangeForm(request.user)
    user = BasicUser.objects.get(email=request.user)
    old_email = user.email
    basic_form = EditUserForm(request.POST or None, user_type='physical', instance = user)
    
    if user.is_company:
        user_profile = user.company_profile
        email_form = EditUserEmailOnlyForm(request.POST or None, user_type='company', instance = user)
        company_form = CompanyUserProfileForm(request.POST or None, instance = user_profile)
        

    if request.method == 'POST':
        if user.is_company:
            user_profile = user.company_profile
            if company_form.is_valid() and email_form.is_valid():
                new_user = basic_form.save(commit=False)
                
                handle_email_change(old_email,new_user.email)
                new_user.save()
                update_session_auth_hash(request, new_user)
                new_profile = company_form.save()
                user.company_profile = new_profile

                return HttpResponseRedirect(reverse('edit_user'))
            else:
                return render(request, 'izdelki/edit_user.html',
                              {'form_basic': basic_form, 'form_company': company_form, 'form_password': password_form})
        else:
            user_profile = user

            if basic_form.is_valid():
                new_user = basic_form.save(commit=False)
                handle_email_change(user,new_user)
                new_user.save()
                return HttpResponseRedirect(reverse('edit_user'))

    else:
        if user.is_company:
            return render(request, 'izdelki/edit_user.html', {'form_basic': email_form, 'form_company': company_form,
                                                     'form_password': password_form,
                                                     'password_errors': password_errors})
        else:
            return render(request, 'izdelki/edit_user.html', {'form_basic': basic_form,
                                                     'form_password': password_form, 'password_errors': password_errors })

@login_required
@require_POST
def password_change(request):
    password_form = PasswordChangeForm(request.user, request.POST)
    if password_form.is_valid():
        user = password_form.save()
        update_session_auth_hash(request, user)  # Important!
        return redirect('dashboard')
    else:
        request.session['password_errors'] = password_form.errors
        return redirect('edit_user')


@login_required
def dashboard(request):
    DASH_ITEMS_PER_PAGE = 6

    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})
    user = None
    user_profile = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)
        if user.is_company:
            user_profile = user.company_profile
        else:
            user_profile = user


    try:
        food = read_products()
    except:
        return HttpResponse("Prišlo je do napake pri povezavi s strežnikom. Prosim, osvežite stran in poskusite ponovno", status=500)

    filtered_food = []

    total_recipes = 0
    total_labels = 0
    for product in food.values():

        if not product['COMMERCIAL_NAME']: product['COMMERCIAL_NAME'] = product['NAME']
        if product['AUTHOR'] == str(user.id):  
            #not all products have the IS_RECIPE tag, so add it for those that are missing it
            if "IS_RECIPE" not in product:
                product["IS_RECIPE"] = True   

            if product["IS_RECIPE"]:
                total_recipes += 1
            else:
                total_labels += 1

            calculate_values(product)
            make_https_photo(product)
            product = round_component_values(product, is_product=True)
            filtered_food.append(product)
        
        #For testing pagination. Since the test user doesn't have a lot of products, just take all of them
        """
        else:
            filtered_food.append(product)
        """

    filtered_food.sort(key=lambda x: x['COMMERCIAL_NAME'])

    [components, other] = prepare_components()
    for c in components:
        #"Nasičene maščobe" is too long to fit into the product box, so shorten it.
        if c['name'].startswith("Nasi"):
            c['name'] = "nas. maščobe"

    paginator = view_utilities.FoodPaginator(filtered_food, DASH_ITEMS_PER_PAGE)
    page = request.GET.get('page')
    try:
        food_results = paginator.page(page)
    except PageNotAnInteger:
        food_results = paginator.page(1)
    except EmptyPage:
        food_results = paginator.page(paginator.num_pages)





    return render(request, 'izdelki/dashboard.html', {'user_profile' : user_profile, 'food': food_results, 
                                                      'components': components,
                                                      'total_labels': total_labels,
                                                      'total_recipes': total_recipes })

def search(request):
    DEFAULT_SORT = 'PRODUCER'
    """
    The number of results per page needs to be an even number to get
    the alternating row colors to line up correctly when loading new pages 
    It should also be larger than 20 results. 20 is the number that fits into one screen
    when we first load the results. 
    Since the infinitescroll plugin loads a new page when the bottom of the results table is visible,
    we don't want the bottom to be visible immediately (so that we don't instantly load another page)
    """
    SEARCH_RESULTS_PER_PAGE = 24

    #update the local json file: TODO: when?
    """
    api = API()
    api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
    api.download_food()
    api.logout()
    """

    view_utilities.set_sort_locale()

    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})

    user = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)

    #Filter results
    search_query = request.GET.get('searchQuery', '')
    empty_query = len(search_query) == 0
    try:
        food = read_products()
    except:
        return HttpResponse("Prišlo je do napake pri povezavi s strežnikom. Prosim, osvežite stran in poskusite ponovno", status=500)

    filtered_food = view_utilities.search_products(food,search_query, user)


    #Sort results
    sort_by = DEFAULT_SORT
    if request.GET.get('sort'):
        if len(filtered_food) > 0 and request.GET.get('sort') in filtered_food[0]:
            sort_by = request.GET.get('sort')

    #should the sorting be done in reverse order?
    reverse = request.GET.get('reverse')=='yes'
    view_utilities.sort_results(filtered_food, sort_by, reverse)

    #moved to search_products() to enable searching for label products
    #filtered_food = mark_label_products(filtered_food)
    

    paginator = Paginator(filtered_food, SEARCH_RESULTS_PER_PAGE)
    page = request.GET.get('page')



    try:
        food_results = paginator.page(page)
    except PageNotAnInteger:
        food_results = paginator.page(1)
    except EmptyPage:
        food_results = paginator.page(paginator.num_pages)
    

    return render(request, 'izdelki/search.html', {'search_query': search_query, 'food': food_results,
                                                   'sort' : sort_by, 'reverse' : reverse, 'empty_query': empty_query})

    

def detail(request, product_id):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})  
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})  
    
    user = None
    original_components_grams = {}
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)


    queried_product = find_product_by_id(product_id)
    #print("PRODUCT")
    print(queried_product)

    api = API()
    api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
    api.download_one_product(product_id)
    print("DOWNLOADED")
    print(queried_product)


    
    if queried_product is None:
        return render(request, 'izdelki/invalid_product.html')
    
    
    if not view_utilities.is_product_displayable(queried_product, user):
        return render(request, 'izdelki/invalid_product.html')


    #Save the original components before rounding. 
    #We'll use these when converting units to not lose precision when converting
    original_components_grams = json.dumps(queried_product["COMPONENTS"])

    #edit = user and queried_product['AUTHOR'] == str(user.id)
    edit = view_utilities.can_edit_product(user, queried_product)

    #In addition to basic components (Fat, Sugar, Carbohydrates, Proteins), users can also choose to add
    #extra components to their products (like Vitamins). This will get us a list of those extra components
    extra_components = get_extra_components(queried_product)
    make_https_photo(queried_product)
    
    [components, other] = prepare_components(extra_components)
    #create a list of just the component names
    component_names = [x.get("code", "") for x in components]

    finished_product = calculate_values(queried_product)
    
    #Calculate percentages needs to be called before convert_to_default_units,
    #Since we calculate percentages based on values in grams
    product_percentages = calculate_percentages(finished_product, components=component_names)
    finished_product = convert_to_default_units(finished_product, components)

    finished_product = find_product_diets_and_ingredients(finished_product)
    finished_product = round_component_values(finished_product, is_product=True)

    if not finished_product['COMMERCIAL_NAME']: finished_product['COMMERCIAL_NAME'] = finished_product['NAME']
    
    producer = finished_product.get('PRODUCER', None)


    
    try:
        company = CompanyUserProfile.objects.get(registration_number=producer) if producer and producer.isdigit() else None
    except:
        company = None

    """
    The producer will be one of:
    - Company ID number, if the product is a recipe added from our site
    - String for labels added from our site
    - String for all products added externally (which don't show on our site, so we can ignore them)

    """  
    is_recipe = finished_product.get("IS_RECIPE", True)
    if  is_recipe:
        if company:
            product_producer = company.company_name
        else:
            product_producer = None
    else:
        product_producer = finished_product.get("PRODUCER", None)

    #print (str(json.dumps(original_components_grams)))
    return render(request, 'izdelki/detail.html', {'product': finished_product, 'components': components,
                                                   'product_company': company, 'user': user,
                                                   'product_percentages': product_percentages,
                                                   'edit': edit,
                                                   'unit_change_codes':UNIT_CHANGE_CODES,
                                                   'product_producer': product_producer,
                                                   'original_components_grams': original_components_grams,
                                                   'is_recipe': is_recipe})


def add(request, product_id=None):
    original_components_grams = {}
    all_barcodes= get_all_barcodes()
    product = None
    login_form = BasicLoginForm()
    login_form.helper.form_action = build_url('login', params={'next': request.path})
    if product_id:
        product = find_product_by_id(product_id)

    user = None
    company_profile = None
    authenticated = True if request.user.is_authenticated else False
    barcode_prefix = ""
    if authenticated:
        user = BasicUser.objects.get(email=request.user)
        company_profile = user.company_profile
        if company_profile:
            barcode_prefix = company_profile.gs1_prefix

    
    if (product and not(view_utilities.is_product_from_user(product, user) or view_utilities.can_edit_product(user, product))
        or (not product and product_id)):
            return render(request, 'izdelki/invalid_product.html')
    

    [components, other_comp] = prepare_components()
    
    other_comp = json.dumps(other_comp)

    api = API()
    api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
    cooking_methods = api.get_cooking_methods()
    #print(cooking_methods)

    
    groups = api.get_groups()
    api.logout()


    is_from_label = False
    product_components = []
    extra_components = []
    if product:
        make_https_photo(product)
        original_components_grams = json.dumps(product["COMPONENTS"])
        extra_components = get_extra_components(product)
        [components_new, other] = prepare_components(extra_components)
        

        is_from_label = not product.get("IS_RECIPE", True)
        if is_from_label:
            #labels don't have an enval value, but prepare_components+convert will still put it there
            #so we need to delete it to not break other functions
            product = convert_to_default_units(product, components_new)
            product["COMPONENTS"].pop("ENVAL", None)
        
        product_components = product["COMPONENTS"]
        #Add in a salt value to display on the edit screen 
        NA_TO_SALT_ACTOR = 2.5
        #Some old products were added to the database without any ingredients,
        #which causes them to not have components, so they throw an error
        #at product_components.get("NA",0)
        #
        #We explicitly handle this special case here, even though it isn't possible
        #to add products without ingredients anymore
        if len(product_components) > 0:
            product_components["SALT"] = product_components.get("NA",0) * NA_TO_SALT_ACTOR
        
        product_components = round_component_values(product_components)
        product_edit = True
        request.session['edit_start'] = True
        try:
            recipe_form = RecipeForm(product_for_edit(product), 
                                 initial={'group': product.get('GROUP_ID')},
                                 group_choices=food_group_for_select(groups), barcode_prefix = barcode_prefix)
        except:
            return render(request, "izdelki/api_error.html")
    else:
        try:
            recipe_form = RecipeForm(group_choices=food_group_for_select(groups),
                                 barcode_prefix = barcode_prefix)
        except:
            return render(request, "izdelki/api_error.html")
        
        product_edit = False

    product = find_product_diets_and_ingredients(product) if product else None
    

    product_name = product.get('COMMERCIAL_NAME', "") if product else ""
    if not product_name:
        product_name = product.get('NAME', "") if product else ""
    product_method = product['COOKMETH'] if product else False
    product_allergens = product['DIETS'] if product else False
    product_ingredients = product.get('INGREDIENTS', False) if product else False
    if product_ingredients:
        product_ingredients = generate_tooltips(product_ingredients)
    product_serving = product['SERVING']['WEIGHT'] if product else 0
    product_mealsize = product['SERVING']['MEALSIZE'] if product else 0
    product_photo = product.get("PHOTO", False) if product else False
    product_producer = product.get("PRODUCER", None) if product else None




    return render(request, 'izdelki/add.html', {'components': components,
                                                'other_components': other_comp, 'allergens': allergens,
                                                'cooking_methods': cooking_methods, 'groups': groups,
                                                'user_profile': company_profile,
                                                'recipe_form': recipe_form, 'authenticated': json.dumps(authenticated),
                                                'product_method': json.dumps(product_method),
                                                'product_allergens': json.dumps(product_allergens),
                                                'product_ingredients': json.dumps(product_ingredients),
                                                'product_serving': product_serving,
                                                'product_mealsize': product_mealsize,
                                                'edit': authenticated,
                                                'barcode_prefix': barcode_prefix,
                                                'product_id': product_id,
                                                'product_name': product_name,
                                                'product_edit': product_edit,
                                                'is_from_label': is_from_label,
                                                'product_components': product_components,
                                                'product_photo': product_photo,
                                                'extra_components': extra_components,
                                                'product_components_correct_units': product_components,
                                                'unit_change_codes': UNIT_CHANGE_CODES,
                                                'product_producer' : product_producer,
                                                'original_components_grams': original_components_grams,
                                                'unit_change_codes': UNIT_CHANGE_CODES,
                                                'box_login_form': login_form})



def add_label(request, product_id=None):
    original_components_grams = {}
    all_barcodes= get_all_barcodes()
    product = None
    login_form = BasicLoginForm()
    login_form.helper.form_action = build_url('login', params={'next': request.path})
    if product_id:
        product = find_product_by_id(product_id)

    user = None
    company_profile = None
    authenticated = True if request.user.is_authenticated else False
    barcode_prefix = ""
    if authenticated:
        user = BasicUser.objects.get(email=request.user)
        company_profile = user.company_profile
        if company_profile:
            barcode_prefix = company_profile.gs1_prefix

    
    if (product and not(view_utilities.is_product_from_user(product, user) or view_utilities.can_edit_product(user, product))
        or (not product and product_id)):
            return render(request, 'izdelki/invalid_product.html')
    

    [components, other_comp] = prepare_components()
    
    other_comp = json.dumps(other_comp)

    api = API()
    api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
    cooking_methods = api.get_cooking_methods()
    #print(cooking_methods)

    
    groups = api.get_groups()
    api.logout()


    is_from_label = False
    product_components = []
    extra_components = []
    if product:
        make_https_photo(product)
        original_components_grams = json.dumps(product["COMPONENTS"])
        extra_components = get_extra_components(product)
        [components_new, other] = prepare_components(extra_components)
        

        is_from_label = not product.get("IS_RECIPE", True)
        if is_from_label:
            #labels don't have an enval value, but prepare_components+convert will still put it there
            #so we need to delete it to not break other functions
            product = convert_to_default_units(product, components_new)
            product["COMPONENTS"].pop("ENVAL", None)
        
        product_components = product["COMPONENTS"]
        #Add in a salt value to display on the edit screen 
        NA_TO_SALT_ACTOR = 2.5
        #Some old products were added to the database without any ingredients,
        #which causes them to not have components, so they throw an error
        #at product_components.get("NA",0)
        #
        #We explicitly handle this special case here, even though it isn't possible
        #to add products without ingredients anymore
        if len(product_components) > 0:
            product_components["SALT"] = product_components.get("NA",0) * NA_TO_SALT_ACTOR
        
        product_components = round_component_values(product_components)
        product_edit = True
        request.session['edit_start'] = True
        try:
            recipe_form = RecipeForm(product_for_edit(product), 
                                 initial={'group': product.get('GROUP_ID')},
                                 group_choices=food_group_for_select(groups), barcode_prefix = barcode_prefix)
        except:
            return render(request, "izdelki/api_error.html")
    else:
        try:
            recipe_form = RecipeForm(group_choices=food_group_for_select(groups),
                                 barcode_prefix = barcode_prefix)
        except:
            return render(request, "izdelki/api_error.html")
        
        product_edit = False

    product = find_product_diets_and_ingredients(product) if product else None
    

    product_name = product.get('COMMERCIAL_NAME', "") if product else ""
    if not product_name:
        product_name = product.get('NAME', "") if product else ""
    product_method = product['COOKMETH'] if product else False
    product_allergens = product['DIETS'] if product else False
    product_ingredients = product.get('INGREDIENTS', False) if product else False
    if product_ingredients:
        product_ingredients = generate_tooltips(product_ingredients)
    product_serving = product['SERVING']['WEIGHT'] if product else 0
    product_mealsize = product['SERVING']['MEALSIZE'] if product else 0
    product_photo = product.get("PHOTO", False) if product else False
    product_producer = product.get("PRODUCER", "") if product else ""




    return render(request, 'izdelki/add_label.html', {'components': components,
                                                'other_components': other_comp, 'allergens': allergens,
                                                'cooking_methods': cooking_methods, 'groups': groups,
                                                'user_profile': company_profile,
                                                'recipe_form': recipe_form, 'authenticated': json.dumps(authenticated),
                                                'product_method': json.dumps(product_method),
                                                'product_allergens': json.dumps(product_allergens),
                                                'product_ingredients': json.dumps(product_ingredients),
                                                'product_serving': product_serving,
                                                'product_mealsize': product_mealsize,
                                                'edit': authenticated,
                                                'barcode_prefix': barcode_prefix,
                                                'product_id': product_id,
                                                'product_name': product_name,
                                                'product_edit': product_edit,
                                                'is_from_label': is_from_label,
                                                'product_components': product_components,
                                                'product_photo': product_photo,
                                                'extra_components': extra_components,
                                                'product_components_correct_units': product_components,
                                                'unit_change_codes': UNIT_CHANGE_CODES,
                                                'product_producer' : product_producer,
                                                'original_components_grams': original_components_grams,
                                                'unit_change_codes': UNIT_CHANGE_CODES,
                                                'box_login_form': login_form})



def get_component_values(request):
    if request.method == 'GET':
        try:
            wanted_components = request.GET.get('components')
            ingredients = request.GET.get('ingredients')
            cook_method = request.GET.get('cook_method')
            group = request.GET.get('group')
            serv_size = request.GET.get('serv_size', '').replace(',', '')
            wanted_components, ingredients = json.loads(wanted_components), json.loads(ingredients)
            api = API()
            api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)

            all_components = api.calc_product(
                ingredients, cook_method, group, serv_size)
            values = {name: val for name, val in all_components.items() if name in wanted_components}
            # fill missing zero value components
            for cp in wanted_components:
                if cp not in values:
                    values[cp] = 0
            #get salt value fron the NA component
            values['salt']=values['na'] * 2.5
            #the NA key isn't needed anymore, so remove it
            #keeping it also breaks the calculate_percentages function
            values.pop('na')
            wanted_components.remove('na')
            percentages = calculate_percentages({'COMPONENTS': values}, wanted_components, lower=True)
            api.logout()
            return JsonResponse({'components': values, 'percentages': percentages})
        except Exception as e:
            return HttpResponse(repr(e), status=500)
    
    elif request.method == "OPTIONS":
        #Some browsers seem to send an OPTIONS request when doing a jquery ajax call
        #Try to fix it with this
        #https://stackoverflow.com/questions/1099787/jquery-ajax-post-sending-options-as-request-method-in-firefox
        response = HttpResponse()
        response['Access-Control-Allow-Origin'] = 'https://prehranskeoznacbe.si'
        response['Vary'] = 'Origin'
        response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
        response['Access-Control-Max-Age'] = 1000
        # note that '*' is not valid for Access-Control-Allow-Headers
        response['Access-Control-Allow-Headers'] = 'origin, x-csrftoken, content-type, accept, x-csrf-token'
        return response

    else:
        return HttpResponse("Method not allowed", status=405)



@login_required
def add_submit(request, product_id=None):
    logger = logging.getLogger(__name__)
    if request.method == 'POST':
        try:
            data = {}

            api = API()
            api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
            groups = api.get_groups()
            api.logout()

            barcode_prefix=""
            user = BasicUser.objects.get(email=request.user)
            company_profile = user.company_profile
            if company_profile:
                barcode_prefix = company_profile.gs1_prefix
            all_barcodes= get_all_barcodes()

            if product_id:
                product = find_product_by_id(product_id)
                if product is None:
                    return render(request, 'izdelki/invalid_product.html')
                current_product_barcode = product.get("BARCODE", None)
                #if we're editing, we also need to tell the form what the current products barcode is
                #so that we won't think the barcode already exists when the product is being added
                try:
                    form = RecipeForm(request.POST, group_choices=food_group_for_select(groups), 
                                  barcode_prefix=barcode_prefix, all_barcodes=all_barcodes,
                                  current_product_barcode=current_product_barcode)
                except:
                    return render(request, "izdelki/api_error.html")
            else:
                try:
                    form = RecipeForm(request.POST, group_choices=food_group_for_select(groups), 
                                  barcode_prefix=barcode_prefix, all_barcodes=all_barcodes)
                except:
                    return render(request, "izdelki/api_error.html")

            
            if not form.is_valid():
                user = None
                company_profile = None
                authenticated = True if request.user.is_authenticated else False
                if authenticated:
                    user = BasicUser.objects.get(email=request.user)
                    company_profile = user.company_profile
                return render(request, 'izdelki/add_product_info.html', {'recipe_form': form, 'user_profile':
                                                                         company_profile}, status=400)
            form_data = form.cleaned_data
            for k, v in request.POST.items():

                if 'csrf' in k:
                    continue

                if k in form_data:
                    v = form_data[k]

                if 'items' in k:
                    if 'weight' in k:
                        try:
                            v = float(v) if '.' in v or 'e' in v else int(v)
                        except:
                            v = 0
                    data[k] = v
                    continue

                if '[' in k and ']' in k:
                    v = request.POST.getlist(k)
                    k = k.replace('[', '')
                    k = k.replace(']', '')

                new_v = []
                if k in database_ints:
                    if type(v) == list:
                        for x in v:
                            if x: new_v.append(int(x))
                    else:
                        if v: new_v = [int(x) for x in v.split(',')]
                elif k in database_floats:
                    if type(v) == list:
                        for x in v:
                            if x: new_v.append(float(x) if '.' in x else int(x))
                    else:
                        if v:
                            v=v.replace(",",".")
                            new_v = float(v) if '.' in v or 'e' in v else int(v)
                if type(new_v) == list and len(new_v) == 0:
                    new_v = v
                data[k] = new_v

            if 'diets' in data:
                data['diets'] = ','.join([str(x) for x in data['diets']])
            
            # USER DATA
            if request.user.is_authenticated:
                user = BasicUser.objects.get(email=request.user)
                data['author'] = user.id
                if user.is_company:
                    data['producer'] = user.company_profile.registration_number  # TODO

            data['self_api'] = True
            data['is_public'] = 1 if data.get('is_public', 0) else 0

            if product_id:
                data['code'] = product_id


            api = API()
            api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
            code = api.add_product(data)

            #When the users add a product, they can choose to display extra components (such as vitamins, ...)
            #The API doesn't keep track of which components were added this way, so we store this information locally
            #
            #This let's us display these extra components when the user views a saved file
            extra_components = request.POST.get("ExtraComponents","").split(",")
            add_components_to_file(code, extra_components)
            
            if 'photo' in request.POST and request.POST['photo']:
                photo_bin = decodebytes(str.encode(
                    request.POST['photo'].split(',')[1]))
                
                if (not photo_valid(photo_bin)):
                    return HttpResponse("Strežnik ni mogel obdelati vaše slike. Prosim, poskusite z drugo sliko.", status=500)
            
                api.add_photo(code, photo_bin)

            product = api.download_one_product(code)
            if not product:
                return HttpResponse("Prišlo je do napake pri povezavi s strežnikom. Prosim, osvežite stran in poskusite ponovno", status=500)
            api.logout()

            add_product_to_file(product, product_id)

            return JsonResponse({'redirect': reverse('dashboard')})
        except Exception as e:
            raise e
            return HttpResponse("Prišlo je do napake pri povezavi s strežnikom. Prosim, osvežite stran in poskusite ponovno", status=500)


@login_required
def dashboard_submit(request):
    try:
        api = API()
        api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
        if 'code' in request.POST and request.POST['code']:
            code = request.POST['code']
        else:
            return HttpResponse("No code in image", status=500)

        if 'photo' in request.POST and request.POST['photo']:
            photo_bin = decodebytes(str.encode(
                                   request.POST['photo'].split(',')[1]))

            if (not photo_valid(photo_bin)):
                return HttpResponse("Bad photo", status=500)

            api.add_photo(code, photo_bin)
            
        else:
            return HttpResponse("No photo in image", status=500)
        
        product = api.download_one_product(code)
        api.logout()

        add_product_to_file(product, code)

        return JsonResponse({'redirect': reverse('dashboard')})
    except Exception as e:
        raise e
        return HttpResponse("Prišlo je do napake pri povezavi s strežnikom. Prosim, osvežite stran in poskusite ponovno", status=500)


@login_required
def delete(request):
    if request.method != "POST" or "product_id" not in request.POST:
        return HttpResponse("Bad request", status=500)
    product_id = request.POST["product_id"]


    user = BasicUser.objects.get(email=request.user)
    product = find_product_by_id(product_id)
    if not user or product['AUTHOR'] != str(user.id):
        return HttpResponse("Bad request", status=500)
    try:
        api = API()
        api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
        api.delete_product(product_id)
        api.logout()
    except Exception as e:
        raise e
        return HttpResponse("Prišlo je do napake pri povezavi s strežnikom. Prosim, osvežite stran in poskusite ponovno", status=500)
    #We also need to mark the product in the food.json file, else the site will keep displaying it
    mark_product_as_deleted(product_id)


    return JsonResponse({'redirect': reverse('dashboard')})



#Similar to add_submit, but with component values and without author id.
#If redirect is true, the view will redirect to the dashboard after a sucessfull submit.
#Otherwise, the browser will stay on the same page
@login_required
def add_submit_label(request, product_id=None, redirect = False):
    if request.method == 'POST':
        try:
            data = {}
            api = API()
            api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
            groups = api.get_groups()
            api.logout()

            barcode_prefix=""
            user = BasicUser.objects.get(email=request.user)
            company_profile = user.company_profile
            if company_profile:
                barcode_prefix = company_profile.gs1_prefix
            all_barcodes= get_all_barcodes()

            if product_id:
                product = find_product_by_id(product_id)
                if product is None:
                    return render(request, 'izdelki/invalid_product.html')
                current_product_barcode = product.get("BARCODE", None)
                #if we're editing, we also need to tell the form what the current products barcode is
                #so that we won't think the barcode already exists when the product is being added
                try:
                    form = RecipeForm(request.POST, group_choices=food_group_for_select(groups), 
                                  barcode_prefix=barcode_prefix, all_barcodes=all_barcodes,
                                  current_product_barcode=current_product_barcode)
                except:
                    return render(request, "izdelki/api_error.html")
            else:
                try:
                    form = RecipeForm(request.POST, group_choices=food_group_for_select(groups), 
                                  barcode_prefix=barcode_prefix, all_barcodes=all_barcodes)
                except:
                    return render(request, "izdelki/api_error.html")
           
            if not form.is_valid():
                user = None
                company_profile = None
                authenticated = True if request.user.is_authenticated else False
                if authenticated:
                    user = BasicUser.objects.get(email=request.user)
                    company_profile = user.company_profile
                #return render(request, 'izdelki/add_product_info.html', {'recipe_form': form, 'user_profile':
                #                                                         company_profile}, status=500)
                #print(form.errors)
                return JsonResponse(form.errors, status=500)


            form_data = form.cleaned_data
            
            for k, v in request.POST.items():
                if 'csrf' in k:
                    continue

                if k in form_data:
                    v = form_data[k]

                if 'components' in k:
                    if 'weight' in k:
                        try:
                            v = float(v) if '.' in v or 'e' in v else int(v)
                        except:
                            v = 0
                    data[k] = v
                    continue

                if '[' in k and ']' in k:
                    v = request.POST.getlist(k)
                    k = k.replace('[', '')
                    k = k.replace(']', '')

                new_v = []
                if k in database_ints:
                    if type(v) == list:
                        for x in v:
                            if x: new_v.append(int(x))
                    else:
                        if v: new_v = [int(x) for x in v.split(',')]
                elif k in database_floats:
                    if type(v) == list:
                        for x in v:
                            if x: new_v.append(float(x) if '.' in x else int(x))
                    else:
                        if v: new_v = float(v) if '.' in v or 'e' in v else int(v)
                if type(new_v) == list and len(new_v) == 0:
                    new_v = v
                data[k] = new_v

            data['self_api'] = True
            data['is_public'] = 1 if data.get('is_public', 0) else 0
            if 'diets' in data:
                data['diets'] = ','.join([str(x) for x in data['diets']])
            
            if len(data['name']) == 0:
                data['name'] = data['commercial_name']

            if request.user.is_authenticated:
                user = BasicUser.objects.get(email=request.user)
                data['author'] = user.id

            if product_id:
                data['code'] = product_id

            data['status'] = "entered"

            api = API()
            api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
            print("ADDING PRODUCT")
            print(data)
            code = api.add_product_from_label(data)
            print(code)

            product = api.download_one_product(code)
            print(product)

            if not product:
                return JsonResponse({'server': ["Prišlo je do napake pri komunikaciji s strežnikom."]}, status=500)

            product_name = product.get('COMMERCIAL_NAME', None)
            if not product_name:
                product_name = product.get('NAME', '')
            api.logout()

            add_product_to_file(product, product_id)

            #product_id will be true when the user is editing an existing product
            #in that case, we just want to redirect back to the dashboard
            #
            #Else, the user is adding a new product from label as a part of a bigger recipe
            #In that case, we need to return the product's code and name so that it can
            #be displayed in the add product interface
            if product_id or redirect:
                return JsonResponse({'redirect': reverse('dashboard')})
            else:    
                #return the product's name and id so that they can be added on the client side
                return JsonResponse({'product_code' : code, 'product_name': product_name})
            
        except Exception as e:
            raise e
            return HttpResponse("Prišlo je do napake pri povezavi s strežnikom. Prosim, osvežite stran in poskusite ponovno", status=500)


def about(request):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})
    user = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)
    return render(request, 'izdelki/about.html', {'user': user})

def help(request):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})
    user = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)
    return render(request, 'izdelki/about_pages/help.html', {'user': user})

def about(request):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})
    user = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)
    return render(request, 'izdelki/about_pages/about.html', {'user': user})

def contact(request):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})
    user = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)
    return render(request, 'izdelki/about_pages/contact.html', {'user': user})

def terms_of_use(request):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})
    user = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)
    return render(request, 'izdelki/about_pages/terms_of_use.html', {'user': user})


def link_callback(uri, rel):
    """Convert HTML URIs to absolute system paths so xhtml2pdf can access those resources
    
    This is needed because the xhtml2pdf library only takes absolute file paths
    But Django uses relative paths to refer to files
    Necessary if we want to link to fonts/other resources from the pdf file

    merges sRoot  /home/userX/static/ with uri static/resource
    to do that, it needs to remove the static/ part from the uri to get:
    /home/userX/static/resource
    
    Arguments:
        uri {string} -- The file path to convert
        rel {} -- Unused, but must be defined for the PDF library to work
    
    Returns:
        string -- The new path that can be used by the PDF library
    
    Raises:
        Exception -- The requested file doesn't exits
    """
    # use short variable names
    sUrl = settings.STATIC_URL      # Typically /static/
    sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/

    # convert URIs to absolute system paths
    if uri.startswith(sUrl):

        #change / to \ in windows
        WINDOWS_OS_NAME="nt"
        if os.name==WINDOWS_OS_NAME:
            uri = uri.replace('/', '\\')
            sUrl=sUrl.replace('/', '\\')
            sRoot=sRoot.replace('/', '\\')
        path = os.path.join(sRoot, uri.replace(sUrl,""))

    #

    # make sure that file exists
    if not os.path.isfile(path):
            raise Exception(
                    'File not found: %s. You may need to run collectstatic even on development builds' % \
                    (uri))
    return path




#using the xhtml2pdf library, convert a html template to pdf
def get_pdf(request, product_id=""):
    mimetypes.add_type('application/font-ttf', '.ttf')
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})  
    
    if request.POST:
        data = {}
        pdf_filename=request.POST.get("commercial_name", "export").replace(" ", "_")
        if pdf_filename == "": pdf_filename = "export"
        api = API()
        api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
        groups = api.get_groups()
        api.logout()

        # PRODUCT DATA
        # print(request.POST)
        try:
            form = RecipeForm(request.POST, group_choices=food_group_for_select(groups))
        except:
            return render(request, "izdelki/api_error.html")
        form.is_valid()
        form_data = form.cleaned_data
        #any field that doesn't pass validation will be put here
        #this includes for example non numeric product weight
        invalid_fields = form.errors.keys()
        components = {}
        for k, v in request.POST.items():
            
            if k in invalid_fields:
                #skip this field, since it has an error
                #we simply won't display it
                continue

            if 'csrf' in k:
                continue

            if 'mealsize' in k:
                if len(v) == 0:
                    #If we don't have a mealsize, set it to a default
                    data['SERVING'] =  {'MEALSIZE': 100}
                    continue
                else:
                    v_dot = v.replace(',','.')
                    data['SERVING'] =  {'MEALSIZE': int(float(v_dot))}
                    continue

            if k in form_data:
                v = form_data[k]

            if 'items' in k:
                if 'weight' in k:
                    try:
                        v = float(v) if '.' in v or 'e' in v else int(v)
                    except:
                        v=0
                data[k] = v
                continue

            if 'components' in k:
                result = re.search('\[(.*)\]', k)
                key = result.group(1)
                if 'enval' in key:
                    continue
                
                components[key.upper()]=float(v)
                continue

            new_v = []
            if k in database_ints:
                if type(v) == list:
                    for x in v:
                        if x: new_v.append(int(x))
                else:
                    if v: new_v = [int(x) for x in v.split(',')]
            elif k in database_floats:
                if type(v) == list:
                    for x in v:
                        if x: new_v.append(float(x) if '.' in x else int(x))
                else:
                    if v: new_v = float(v) if '.' in v or 'e' in v else int(v)
            if type(new_v) == list and len(new_v) == 0:
                new_v = v
            data[k] = new_v
        if 'diets' in data:
            data['diets'] = ','.join([str(x) for x in data['diets']])

        product = data
        product['COMPONENTS'] = components


        #usually, we get extra components from get_extra_components()
        #however, that only works for products that have been saved to the database 
        #(because that's when we save their extra components)
        #
        #Here, we instead need to get the extra components from the POST data.
        extra_components = components.keys()
        if 'SERVING' not in product:
            product['SERVING'] = {'MEALSIZE' : 0}
        product = {k.upper():v for k,v in product.items()} 
    
    

    else:
        pdf_filename = product_id
        product = find_product_by_id(product_id)
        extra_components = get_extra_components(product)

        user = None
        if request.user.is_authenticated:
            user = BasicUser.objects.get(email=request.user)

        if product is None:
            return render(request, 'izdelki/invalid_product.html')

        if not ((user and product['AUTHOR'] == str(user.id)) or (
               product['AUTHOR'] and product['AUTHOR'].isdigit() and product['IS_PUBLIC'])):
            return render(request, 'izdelki/invalid_product.html')
    
    [components, other] = prepare_components(extra_components)

    if 'SALT' in product['COMPONENTS']:
        salt = product['COMPONENTS']['SALT']
        finished_product = calculate_values(product)
        finished_product['COMPONENTS']['SALT'] = salt
    else:
        finished_product = calculate_values(product)
    finished_product = find_product_diets_and_ingredients(finished_product)

    if 'COMMERCIAL_NAME' not in finished_product and 'NAME' in finished_product:
        finished_product['COMMERCIAL_NAME'] = finished_product['NAME']

    if 'NAME' in finished_product and len(product['NAME']) == 0: 
        finished_product['NAME'] = finished_product['COMMERCIAL_NAME']

    #percentage before conversion
    component_names = [x.get("code", "") for x in components]
    product_percentages = calculate_percentages(finished_product, components=component_names)

    finished_product = convert_to_default_units(finished_product, components)
    #rounding after conversion
    finished_product = round_component_values(finished_product, is_product=True)
    portion_components = calculate_portion(finished_product)
    portion_components = round_component_values(portion_components, multiple_enval=False)
    context = {'product': finished_product, 'components':components, 'product_percentages': product_percentages,
                         'portion_components': portion_components}
               
    template = get_template('izdelki/export_pdf.html')
    html  = template.render(context)

    #create a "file" in memory, without writing to disk
    file = BytesIO()
    pisaStatus = pisa.CreatePDF(html, dest=file,
            link_callback = link_callback)

    # Return PDF document through a Django HTTP response
    pdf = file.getvalue()
    file.close()
    response = HttpResponse(pdf, content_type='application/pdf')

    #format filename from unicode into ascii 
    #(since HTML headers can't transfer python unidoce strings)
    #č->c, š->s, ...
    pdf_filename=str(unicodedata.normalize('NFKD', pdf_filename).encode('ascii','ignore'), "ascii")
    #make the pdf show up as a download instead of just opening a new page, if needed
    response['Content-Disposition'] = 'attachment; filename=' + pdf_filename + ".pdf"
    return response



def get_xls(request, product_id=""):
    mimetypes.add_type('application/font-ttf', '.ttf')
    product = find_product_by_id(product_id)

    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})  
    user = None
    if request.user.is_authenticated:
        user = BasicUser.objects.get(email=request.user)

    if request.POST:
        if user is None:
            return render(request, 'izdelki/invalid_product.html')
        data = {}
        excel_filename=request.POST.get("commercial_name", "export").replace(" ", "_")
        if excel_filename == "": excel_filename = "export"
        api = API()
        api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
        groups = api.get_groups()
        api.logout()

        # PRODUCT DATA
        # print(request.POST)
        try:
            form = RecipeForm(request.POST, group_choices=food_group_for_select(groups))
        except:
            return render(request, "izdelki/api_error.html")
        form.is_valid()
        form_data = form.cleaned_data
        invalid_fields = form.errors.keys()
        components = {}
        for k, v in request.POST.items():
            if k in invalid_fields:
                continue
            if 'csrf' in k:
                continue

            if 'mealsize' in k:
                if len(v) == 0:
                    #If we don't have a mealsize, set it to a default
                    data['SERVING'] =  {'MEALSIZE': 100}
                    continue
                else:
                    v_dot = v.replace(',','.')
                    data['SERVING'] =  {'MEALSIZE': int(float(v_dot))}
                    continue

            if k in form_data:
                v = form_data[k]

            if 'items' in k:
                if 'weight' in k:
                    try:
                        v = float(v) if '.' in v or 'e' in v else int(v)
                    except:
                        v=0
                data[k] = v
                continue

            if 'components' in k:
                result = re.search('\[(.*)\]', k)
                key = result.group(1)
                if 'enval' in key:
                    continue
                
                components[key.upper()]=float(v)
                continue

            new_v = []
            if k in database_ints:
                if type(v) == list:
                    for x in v:
                        if x: new_v.append(int(x))
                else:
                    if v: new_v = [int(x) for x in v.split(',')]
           
            elif k in database_floats:
                if type(v) == list:
                    for x in v:
                        if x: new_v.append(float(x) if '.' in x else int(x))
                else:
                    if v: new_v = float(v) if '.' in v or 'e' in v else int(v)
            if type(new_v) == list and len(new_v) == 0:
                new_v = v
            data[k] = new_v
        if 'diets' in data:
            data['diets'] = ','.join([str(x) for x in data['diets']])

        product = data
        #usually, we get extra components from get_extra_components()
        #however, that only works for products that have been saved to the database 
        #(because that's when we save their extra components)
        #
        #Here, we instead need to get the extra components from the POST data.
        extra_components = components.keys()
        product['COMPONENTS'] = components
        if 'SERVING' not in product:
            product['SERVING'] = {'MEALSIZE' : 0}
        product = {k.upper():v for k,v in product.items()} 
        

    else:
        if product is None or user is None:
            return render(request, 'izdelki/invalid_product.html')
        
        if not ((user and product['AUTHOR'] == str(user.id)) or (
            product['AUTHOR'] and product['AUTHOR'].isdigit() and product['IS_PUBLIC'])):
            return render(request, 'izdelki/invalid_product.html')
        product = find_product_by_id(product_id)
        extra_components = get_extra_components(product)
        excel_filename = product_id
    
    xls = write_product_to_xls(product, extra_components)
    #xslx content type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    response = HttpResponse(xls.getvalue(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    #file extension needs to be .xlsx, nothing else. Otherwise, libre office can't open it and excel gives a warning
    
    #format filename from unicode into ascii 
    #(since HTML headers can't transfer python unidoce strings)
    #č->c, š->s, ...
    excel_filename=str(unicodedata.normalize('NFKD', excel_filename).encode('ascii','ignore'), "ascii")
    
    response['Content-Disposition'] = 'attachment; filename="' + excel_filename + '.xlsx"'
    return response


def get_product_components(request):
    products = read_products().values()
    json = create_select2_product_json(products)
    return JsonResponse(json)


def check_company_id(request):
    company_id = request.POST["company_id"]
    return JsonResponse(AJPES_check_company_id(company_id))


def register_done(request):
    login_form = BasicLoginForm()
    #login_form.helper.form_action = build_url('login', params={'next': request.path})
    login_form.helper.form_action = build_url('login', params={'next': '/dashboard'})
    user = None
    return render(request, 'izdelki/register_done.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = BasicUser.objects.get(id=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return redirect('dashboard')
    else:
        return HttpResponse('Povezava ni veljavna.')    


@user_passes_test(lambda u: u.is_superuser)
def database_redownload(request):
    api = API()
    api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
    api.download_food()
    api.logout()
    return redirect('dashboard')


@csrf_exempt
def api_search(request):
    if request.body:
      request_data= json.loads(request.body.decode('utf-8'))
    else:
      request_data = request.GET

    api_url = "http://bazil.si/api/search/"

    api_key = request.META.get("HTTP_AUTHORIZATION", None)
    
    headers = {"Authorization": api_key}
    response = requests.get(api_url, params=request_data, headers=headers)
    return JsonResponse(response.json(), safe=False)


@csrf_exempt
def api_header(request):
    if request.body:
      request_data= json.loads(request.body.decode('utf-8'))
    else:
      request_data = request.GET

    api_url = "http://bazil.si/api/search/"

    api_key = request.META.get("HTTP_AUTHORIZATION", None)
    
    headers = {"Authorization": api_key}
    return HttpResponse(api_key)
    #response = requests.get(api_url, params=request_data, headers=headers)
    #return JsonResponse(response.json(), safe=False)

@csrf_exempt
def api_test(request):
    api_url = "http://bazil.si/api/search/"
    params = {"code":"24014328"}
    headers = {"Authorization": "Bearer 1J0i8MmV2BQWJrgcVnGU"}
    response = requests.get(api_url, params=params, headers=headers)
    return JsonResponse(response.json(), safe=False)

@csrf_exempt
def api_search_test(request):
    api_url = "http://bazil.si/api/search/"
    if request.body:
      request_data= json.loads(request.body.decode('utf-8'))
    else:
      request_data = request.GET
    headers = {"Authorization": "Bearer 1J0i8MmV2BQWJrgcVnGU"}
    response = requests.get(api_url, params=request_data, headers=headers)
    return JsonResponse(response.json(), safe=False)

@csrf_exempt
def api_search_alt_auth(request):
    api_url = "http://bazil.si/api/search/"
    if request.body:
      request_data= json.loads(request.body.decode('utf-8'))
    else:
      request_data = request.GET
    token = request_data.get("token", None)
    if not token:
        return JsonResponse({'error': 'No token found'})
    headers = {"Authorization": "Bearer " + str(token)}
    response = requests.get(api_url, params=request_data, headers=headers)
    return JsonResponse(response.json(), safe=False)
