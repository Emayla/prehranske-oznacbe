from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

from izdelki.setup import status_strings

register = template.Library()


@register.filter
def get_item(dictionary, key):
    if type(dictionary) != dict:
        return 
    item = dictionary.get(key)
    if key == 'ENVAL' and type(item) == list:
        return item[1]
    return item


@register.filter
def place_dots(list, i):
    i = int(i)
    if i < 1: return False
    return list[i] - list[i-1] > 1

@register.filter
def truncate_email(email):
    return email.split('@')[0]

"""
#Get varaible from a dict
#https://stackoverflow.com/questions/8000022/django-template-how-to-look-up-a-dictionary-value-with-a-variable/8000091
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)
"""

#Get varaible from a dict
#https://stackoverflow.com/questions/8000022/django-template-how-to-look-up-a-dictionary-value-with-a-variable/8000091
@register.filter
def get_dict_item(dict, key):
    return dict.get(key)


#Removes trailing zeroes from floats
#1,00 -> 1
#1,1400 -> 1,14
#This will be called on strings that are using , as a decimal seperator
@register.filter
def trim_trailing_zeroes(string):
    return (string.rstrip('0').rstrip(',') if ',' in string else string)


@register.filter(needs_autoescape=True)
def get_status_string(status,  autoescape=True):
    """Converts the database status string (entered, confirmed, unconfirmed) into a human readable form
    
    Decorators:
        register.filter
    
    Arguments:
        status {string} -- The status string to convert
    
    Returns:
        string -- The converted string
    """

    status_string = status_strings.get(status, status_strings['None'])
    return mark_safe(status_string)


