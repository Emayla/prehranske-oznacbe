from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UserChangeForm
from django.core.exceptions import ValidationError
from django.urls import reverse

from izdelki.models import BasicUser, CompanyUserProfile
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, HTML
from crispy_forms.bootstrap import StrictButton, InlineRadios

from izdelki.utilities import build_url


import requests, re

common_field_errors = {'required': 'To polje je obvezno.', 'invalid': 'Vsebina tega polja ni veljavna.'}


class BasicLoginForm(AuthenticationForm):
    error_messages = {
        'invalid_login':
            "Vnesite pravilno kombinacijo e-naslova in gesla. Bodite pozorni na male in velike črke.",
        'inactive': "Uporabniški račun ni aktiven.",
    }

    class Meta:
        model = BasicUser

    def __init__(self, request=None, *args, **kwargs):
        super(BasicLoginForm, self).__init__(request, *args, **kwargs)
        self.fields['password'].label = 'Geslo'

        for k, field in self.fields.items():
            field.error_messages = common_field_errors

        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.attrs = {'novalidate': ''}
        self.helper.form_action = reverse('login')
        self.helper.layout = Layout(
                'username', 'password',
                StrictButton('Prijava', type='submit', css_class='btn btn-primary btn-login-form-login'),
                HTML("""<div class="login-link-container"> <p class = "register-link-container"><a class="register-link" href="{% url 'register' %}" role="button">Registracija</a></p>"""),
                HTML("""<p class = "reset-password-link-container"><a class="register-link" href="{% url 'password_reset' %}" role="button">Pozabljeno geslo</a></p></div>"""),
        )


class HorizontalLoginForm(BasicLoginForm):
    def __init__(self, request=None, *args, **kwargs):
        super(HorizontalLoginForm, self).__init__(request, *args, **kwargs)
        self.helper.layout = Layout(
                'username', 'password',
                Div(StrictButton('Prijava', type='input', css_class='btn-default btn-primary horizontal-login-button'), css_class=''),
                Div(
                    HTML("""<p class="register-link-container"><a class="register-link" href="{% url 'register' %}">Registracija</a></p>"""),
                    HTML("""<p class="reset-password-link-container"><a class="register-link" id="password-reset-btn" href="{% url 'password_reset' %}">Pozabljeno geslo</a></p>"""),
                    css_class='')
        )


class BasicUserForm(UserCreationForm):
    error_messages = {
        'password_mismatch': "Gesli se ne ujemata.",
    }

    msg_translations = {
        "This password is entirely numeric.": "vnešeno geslo je sestavljeno samo iz številk.",
        "This password is too short.":        "Geslo je prekratko.",
        "This password is too common.":       "vnešeno geslo je prepogosto.",
        "The password is too similar.":       "vnešeno geslo je preveč podobno vašim osebnim podatkom.",
        "Geslo je eno izmed preveč pogosto uporabljanih.": "vnešeno geslo je prepogosto.",
        "Vaše geslo je sestavljeno samo iz števk.": "vnešeno geslo je sestavljeno samo iz številk.",
        "To geslo je prekratko.":             "Geslo je prekratko."

    }

    
    
    user_type_choices = (('physical', 'Potrošnik'), ('business', 'Ponudnik (podjetje, pridelovalec)'))
    user_type = forms.ChoiceField(label='Tip uporabnika', widget=forms.RadioSelect, choices=user_type_choices)

    class Meta:
        model = BasicUser
        fields = ['email', 'first_name', 'last_name', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        user_type = 'physical'
        if 'user_type' in kwargs:
            user_type = kwargs['user_type']
            kwargs.pop('user_type', None)
        super(BasicUserForm, self).__init__(*args, **kwargs)
        self.initial['user_type'] = user_type
        self.fields['password1'].label = 'Geslo'
        self.fields['password1'].help_text = 'Geslo ne sme biti podobno vašim ostalim osebnim podatkom, ne sme biti ' \
                                             'preveč pogosto ali sestavljeno le iz številk, ter mora vsebovati vsaj 8 ' \
                                             'znakov.'
        self.fields['password2'].label = 'Ponovi geslo'
        self.fields['password2'].help_text = 'Ponovno vpišite isto geslo kot zgoraj.'

        for k, field in self.fields.items():
            field.error_messages = common_field_errors

    def clean_password2(self):
        try:
            return super(BasicUserForm, self).clean_password2()
        except ValidationError as e:
            if not getattr(e, 'code', None):
                new_e = []
                for msg in e:
                    msg = msg.split('.')[0] + '.'
                    msg = "The password is too similar." if 'too similar' in msg else msg
                    print(msg)
                    new_e.append(self.msg_translations[msg])
                raise ValidationError(new_e)
            else:
                raise ValidationError(self.error_messages[e.code])

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    # helper.form_method = 'post'
    # helper.form_action = 'register'  # this gets set to reverse('register')
    helper.form_tag = False  # for rendering two forms together; no <form action ="" ...> is generated
    helper.disable_csrf = True  # for rendering two forms together
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-10'
    helper.layout = Layout(
            InlineRadios('user_type'),
            'email',
            Div(
                    'first_name', 'last_name',
                    css_class='physical-only'
            ),
            'password1', 'password2',
    )

        
class PrivacyForm(forms.Form):

    checkbox_1_label = """
            Skladno s Splošno uredbo o varstvu podatkov (GDPR) dovoljujem zbiranje zgoraj navedenih osebnih podatkov izključno za namene preverjanja identitete uporabnika znotraj aplikacije 
            <a href = "/terms" target='_blank' onclick="window.open('/terms', 
                         'Pogoji Uporabe', 
                         'width=800,height=500, scrollbars=yes'); return false; ">Prehranske označbe</a>.
            """

    checkbox_2_label = """
        Seznanjen sem s tem, da imam skladno s Splošno uredbo o varstvu podatkov (GDPR) pravico, da
        <ul>
            <li>zahtevam dostop do osebnih podatkov,</li>
            <li>zahtevam popravek ali izbris osebnih podatkov,</li>
            <li>lahko soglasje kadarkoli prekličem z obvestilom po elektronski pošti preko naslova <a href="mailto:info@prehranskeoznacbe.si">info@prehranskeoznacbe.si</a>,</li>
            <li>imam pravico pri Informacijskem pooblaščencu vložiti pritožbo.</li>
        </ul>
    """

    checkbox_3_label = """
        Seznanjen sem, da
        <ul>
            <li>bo upravljalec navedene osebne podatke obdeloval izključno za preverjanje identitete uporabnika znotraj aplikacije Prehranske označbe,</li>
            <li>na podlagi posredovanih podatkov o podjetju poteka preverjanje identitete preko AJPES spletne storitve PRS,</li>
            <li>se osebni podatki hranijo do preklica.</li>
        </ul>
    """

    privacy_1 = forms.BooleanField(label=checkbox_1_label)
    privacy_2 = forms.BooleanField(label=checkbox_2_label)
    privacy_3 = forms.BooleanField(label=checkbox_3_label)

    helper = FormHelper()
    helper.form_tag = False  # for rendering two forms together; no <form action ="" ...> is generated
    helper.disable_csrf = True  # for rendering two forms together
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-10'
    helper.layout = Layout(
        Div(
                'privacy_1', "privacy_2", "privacy_3",
                css_class="privacy-checkboxes"
        )
    )



class EditUserForm(forms.ModelForm):
    class Meta:
        model = BasicUser
        fields =['email', 'first_name', 'last_name']

    def __init__(self, *args, **kwargs):
        user_type = 'physical'
        if 'user_type' in kwargs:
            user_type = kwargs['user_type']
            kwargs.pop('user_type', None)
        super(EditUserForm, self).__init__(*args, **kwargs)
        self.fields['email'].help_text = ''
        #self.initial['user_type'] = user_type
        for k, field in self.fields.items():
            field.error_messages = common_field_errors

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    # helper.form_method = 'post'
    # helper.form_action = 'register'  # this gets set to reverse('register')
    helper.form_tag = False  # for rendering two forms together; no <form action ="" ...> is generated
    helper.disable_csrf = True  # for rendering two forms together
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-10'
    #helper.layout = Layout(
    #        #Remove the user type choice
    #        InlineRadios('user_type'),
    #        Div(
    #                'first_name', 'last_name',
    #                css_class='physical-only'
    #        ),
    #)




class EditUserEmailOnlyForm(forms.ModelForm):
    class Meta:
        model = BasicUser
        fields =['email']

    def __init__(self, *args, **kwargs):
        user_type = 'physical'
        if 'user_type' in kwargs:
            user_type = kwargs['user_type']
            kwargs.pop('user_type', None)
        super(EditUserEmailOnlyForm, self).__init__(*args, **kwargs)
        self.fields['email'].help_text = ''
        #self.initial['user_type'] = user_type
        for k, field in self.fields.items():
            field.error_messages = common_field_errors

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.form_tag = False  # for rendering two forms together; no <form action ="" ...> is generated
    helper.disable_csrf = True  # for rendering two forms together
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-10'


class CompanyUserProfileForm(forms.ModelForm):
    class Meta:
        model = CompanyUserProfile
        fields = [ 'registration_number', 'company_name', 'address', 'phone', 'website', 'contact', 'gs1_prefix']


    def __init__(self, *args, **kwargs):
        super(CompanyUserProfileForm, self).__init__(*args, **kwargs)

        for field in self.fields.values():
            field.error_messages = common_field_errors

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.form_tag = False  # for rendering two forms together; no <form action ="" ...> is generated
    helper.disable_csrf = True  # for rendering two forms together
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-10'

    def clean(self):
        cleaned_data = super(CompanyUserProfileForm, self).clean()
        cc_registrationNumber = cleaned_data.get("registration_number")

        try:
            response = checkParams(cc_registrationNumber)
            if  (response == False):
                msg="Podjetje s to matično številko ne obstaja/ni zastopnik!"
                self.add_error('registration_number', msg)
            else:
                print(response)
        except Exception:
            pass


class EditCompanyForm(forms.ModelForm):
    class Meta:
        model = CompanyUserProfile
        fields = ['registration_number', 'company_name', 'address', 'phone', 'website', 'contact', 'gs1_prefix']

    def __init__(self, *args, **kwargs):
        super(CompanyUserProfileForm, self).__init__(*args, **kwargs)

        for field in self.fields.values():
            field.error_messages = common_field_errors

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.form_tag = False  # for rendering two forms together; no <form action ="" ...> is generated
    helper.disable_csrf = True  # for rendering two forms together
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-10'




def checkParams(registrationNumber):
    print("CHECKING PARAMETERS")
    url="https://www.ajpes.si/wsPrsInfo/PrsInfo.asmx"
    #headers = {'content-type': 'application/soap+xml'}
    headers = {'content-type': 'text/xml'}
    body ="""<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                    <soap:Body>
                        <PrsDataFind xmlns="http://www.ajpes.si/wsPrs/PrsInfo">
                            <sNaziv/>
                            <sMaticna>""" + str(registrationNumber) + """</sMaticna>
                            <sDavcna/>
                            <sNaslov/>
                            <sHisnaStevilka/>
                            <sNaselje/>
                            <sObcina/>
                            <sPosta/>
                            <sDejavnost/>
                            <sSektor/>
                            <sOblika/>
                            <iTip>1</iTip>
                            <iMaxRec>20</iMaxRec>
                            <Ident>
                                <string>prsinfo_ijs_prehoz</string>
                                <string>Xhj7_mBNb</string>
                                <string>PRS_SN_E</string>
                            </Ident>
                        </PrsDataFind>
                    </soap:Body>
                </soap:Envelope>
    """

    response = requests.post(url,data=body,headers=headers)

    result = str(response.content,"utf-8")
    print(result)

    #Ali je zastopnik (str)
    preverizastopnika = (re.findall(r'<PrsDataFindResult>(.*?)</PrsDataFindResult>', result))[0]


    if preverizastopnika != "true":
        #podjetje s to matično številko ni zastopnik
        return (False)


    #Dobimo še:
    #Kratko_ime
    #Ulica
    #Posta
    #Podenota
    #
    #Primer:
    #<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/
    # xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    # <soap:Body><PrsDataFindResponse xmlns="http://www.ajpes.si/wsPrs/PrsInfo">
    # <PrsDataFindResult>true</PrsDataFindResult><sRetVal>
    # &lt;PrsFind xmlns="http://www.ajpes.si/xml_sheme/prs-info/PrsDataFind"
    #  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;
    #  PrsData&gt;
    #  &lt;Popolno_ime&gt;ORGANIZACIJA KULTURNIH PRIREDITEV, ANDREJ HOFER s.p.&lt;/Popolno_ime&gt;
    #  &lt;Kratko_ime&gt;ANDREJ HOFER s.p.&lt;/Kratko_ime&gt;
    #  &lt;Maticna&gt;3559173000&lt;/Maticna&gt;
    #  &lt;Ulica&gt;Šlandrov trg 020&lt;/Ulica&gt;
    #  &lt;Posta&gt;3310 Žalec&lt;/Posta&gt;
    #  &lt;Podenota&gt;000&lt;/Podenota&gt;
    #  &lt;/PrsData&gt;&lt;/PrsFind&gt;</sRetVal></PrsDataFindResponse></soap:Body></soap:Envelope>
    fullName = (re.findall(r'Popolno_ime&gt;(.*?)/Popolno_ime&gt;', result))[0][:-4]
    #print(fullName)
    return True