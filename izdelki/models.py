from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.core.validators import RegexValidator, URLValidator

# USER RELATED MODELS


class BasicUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The e-mail must be set.')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class CompanyUserProfile(models.Model):
    company_name = models.CharField(_('naziv'), max_length=200)
    registration_number = models.CharField(
            _('matična številka'),
            max_length=7,
            unique=True,
            validators=[
                RegexValidator(
                        regex=r'^\d{7}$',
                        message=_('Matična številka mora biti dolžine 7-mestna številka.'),
                        code=_('invalid_registration_number')
                ),
            ],
            error_messages={'unique': 'Podjetje s to matično številko že obstaja.'},
    )
    address = models.CharField(
            _('sedež'),
            max_length=200,
    )
    # city = models.CharField(
    #         _('kra'),
    #         max_length=50,
    #         validators=[
    #             RegexValidator(
    #                     regex=r'^[a-žA-Ž ]+$',
    #                     message=_('Kraj mora biti sestavljen iz črk in presledkov.'),
    #                     code=_('invalid_city')
    #             ),
    #         ]
    # )
    phone = models.CharField(
            _('telefon'),
            max_length=12,
            validators=[
                RegexValidator(
                        regex=r'^\+?\d{9,12}$',
                        message=_('Telefonska številka mora biti sestavljena le iz števk (in po želji znaka +) - vpišite jo '
                                  'torej brez presledkov in znaka -. Če vpisujete številko stacionarnega telefona, '
                                  'morate na začetek napisati lokalno kodo vašega področja, npr. 01 za širše '
                                  'ljubljansko območje.'),
                        code=_('invalid_phone')
                ),
            ],
            blank=True
    )
    website = models.CharField(_('spletna stran'), max_length=100, blank=True,
                               validators=[
                                   URLValidator(
                                           message=_('Naslov spletne strani se mora začeti s \'http://\' ali \'https://\'.'),
                                           code=_('invalid_website')
                                   )
                               ])
    contact = models.CharField(_('kontaktna oseba'), max_length=80, blank=True)
    
    gs1_prefix = models.CharField(
            _('GS1 Predpona črtne kode'),
            max_length=11, 
            validators = [
                RegexValidator(
                        regex=r'\d{3,12}$',
                        message=_('GS1 predpona mora biti sestavljena samo iz 3- do 12-mestnih številk.'),
                        code=_('invalid_prefix'))
            ],
            blank=True,
            error_messages={'unique':"To GS1 predpono že uporablja drug račun."},
            #We need null if we want the unique constraint
            #Otherwise we can only have one user withoug a prefix
            null=True,
            unique = True


    )
     
    def __str__(self):
        return self.company_name

    class Meta:
        verbose_name = _('nosilec dejavnosti')
        verbose_name_plural = _('nosilci dejavnosti')


class BasicUser(AbstractBaseUser, PermissionsMixin):
    # TODO is activate (for email confirmation)
    # TODO: Default to false, activate with email
    #Needed for password reset
    is_active = models.BooleanField(default=False)
    # password = models.CharField(_('geslo'), max_length=128)
    email = models.EmailField(_('e-naslov'), unique=True,
                              help_text=_('Vnesite pravilen e-naslov, s katerim se boste registrirali in prijavili.'),
                              error_messages= {'unique': 'Uporabnik s tem e-naslovom že obstaja.'})
    first_name = models.CharField(_('ime'), max_length=40, blank=True)
    last_name = models.CharField(_('priimek'), max_length=40, blank=True)
    date_joined = models.DateTimeField(_('datum registracije'), auto_now_add=True)
    is_staff = models.BooleanField(_('dostop do Django admin'), default=False,
                                   help_text=_('Pove, ali se lahko uporabnik prijavi v Django admin.'))
    is_company = models.BooleanField(_('podjetje'), default=False)
    is_inspector = models.BooleanField(_('inšpektor'), default=False)

    company_profile = models.OneToOneField(CompanyUserProfile, null=True, blank=True, on_delete=models.CASCADE,
                                           related_name='company_user')

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = BasicUserManager()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _('uporabnik')
        verbose_name_plural = _('uporabniki')

    def get_full_name(self):
        if self.first_name or self.last_name:
            return self.first_name + ' ' + self.last_name
        return self.email

    def get_short_name(self):
        if self.first_name:
            return self.first_name
        split_email = self.email.split('@')
        return split_email[0]

    def email_user(self, subject, message, from_email=None, **kwargs):
        # TODO implement
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def clean(self):
        super(BasicUser, self).clean()
        self.email = self.__class__.objects.normalize_email(self.email)


BasicUser._meta.get_field('is_superuser').verbose_name = _('administrator strani')
BasicUser._meta.get_field('is_superuser').help_text = _('Tak uporabnik ima vsa dovoljenja in ima torej tudi dostop do '
                                                        'Django admina.')
BasicUser._meta.get_field('groups').verbose_name = _('skupine')
BasicUser._meta.get_field('groups').help_text = _('Skupine, ki jim pripada uporabnik. Uporabnik ima vsa dovoljenja, '
                                                  'ki jih imajo skupine, del katerih je.')
BasicUser._meta.get_field('user_permissions').verbose_name = _('dovoljenja')
BasicUser._meta.get_field('user_permissions').help_text = _('Dovoljenja, ki so specifična za tega uporabnika.')





class Product(models.Model):
    #this field will hold the complete json string of a product
    json = models.CharField(max_length=100000)
    code = models.CharField(max_length=100000, default="")

