import locale
import os
from izdelki.models import CompanyUserProfile
from django.core.paginator import PageNotAnInteger, EmptyPage

STATUS_DEFAULT = "entered"
STATUS_CONFIRMED = "confirmed"


def set_sort_locale():
    """Sets the locale needed for sorting search results

    Different languages sort letters differently. If we want correct sorting, we need to set a proper locale.
    This will ensure č gets sorted immediately after c (and not after z as it does in the English locale), and
    other similar cases.

    Since Windows and Linux use different locale names, we need to check for the OS the server is running on
    """
    LINUX_OS_NAME="posix"
    LINUX_SI_LOCALE = "sl_SI.UTF-8"
    WINDOWS_SI_LOCALE = "sl-SI"
    try:
        #linux
        if os.name=="posix":
            locale.setlocale(locale.LC_ALL, LINUX_SI_LOCALE)
        #windows
        else:
            locale.setlocale(locale.LC_ALL, WINDOWS_SI_LOCALE)
    except:
        #fall back to default locale if the above aren't supported
        #the sorting will be wrong, but the server won't crash
        locale.setlocale(locale.LC_ALL, "")


def is_product_public_and_confirmed(product):
    """Checks if the product is public and has been verified on the database

    Only some products should be displayed to all users (including those not logged in).
    If we want to display a product to everyone, the following must be true
    1. The product was uploaded from our web app. We check this by checking if the author is a number
       This works so far (since no other apps use numbers for authors), but might change in the future.
       In that case, we might need to prefix the author IDs with some unique string
    2. The product was marked as public during upload
    3. If the product is a label (not a recipe), it also needs to be verified on the database level,
       by setting its STATUS field to "confirmed". By default, products added from the webapp have
    the status of "entered"


    """
    is_from_our_app = product['AUTHOR'] and product['AUTHOR'].isdigit()
    is_public = product['IS_PUBLIC']
    
    is_verified = True
    is_recipe = product.get('IS_RECIPE', True)
    if not is_recipe:
        status = product.get('STATUS', STATUS_DEFAULT)
        if status != STATUS_CONFIRMED:
            is_verified  = False 
    
    return  (is_from_our_app and is_public and is_verified)


def is_product_from_user(product, user):
    """Checks if a given product belongs to the given user
    """
    return (user and product['AUTHOR'] == str(user.id))


def is_product_displayable(product, user):
    """Checks if the user is allowed to view a product

    A user can view a product if the product is public, or if the product was uploaded by the user
    """
    return is_product_public_and_confirmed(product) or is_product_from_user(product, user) or can_edit_product(user, product)


def shorten_group_name(group):
    """Shortens the input group_name for nicer display in the search results
    
    The group name is shortened by removing any text in parenthesis
    
    Arguments:
        group {string} -- The group name to shorten
    """ 
    if group:
        parenth_position = group.find('(')
        if parenth_position > 0:
            #-1 to cut off the space too
            group = group[:parenth_position-1]
    return group

def _sort_helper(x,key):
    """This function is passed to the python sort() function to properly sort search results
    
    some products don't contain all of their information. For example, they are missing 
    information about their producer, barcode, ...
    This can break sorting, since Python3 throws an exception whenever it tries to compare a NoneType (missing information) to anything else
    Fix: https://stackoverflow.com/questions/18411560/python-sort-list-with-none-at-the-end,
         https://stackoverflow.com/questions/30976124/sort-when-values-are-none-or-empty-strings-python
    

    Keys:
   
    Arguments:
        x {product} -- the element being compared
        key {string} -- the key to sort by. One of:
                        COMMERCIAL_NAME, PRODUCER, GROUP_NAME, BARCODE         
    """

    #Some product names have leading spaces that need to be removed for sorting to work
    string = x[key].lstrip() if x[key]!=None else None
    #First sort those whose key is None, than those whose key is empty
    #End then the normal strings
    #locale.strxfrm allows sorting by a locale specific alphabet (ie Slovenian)
    localized_string= locale.strxfrm(string) if string is not None else None
    return (x[key] is None, x[key] == "", localized_string)

def sort_results(results, sort_by, reverse):
    """Sorts the search results
    
    Arguments:
        results {array of products} -- The products to sort
        sort_by {string} -- the key to sort by. One of: COMMERCIAL_NAME, PRODUCER, GROUP_NAME, BARCODE
        reverse {boolean} -- Should the sorting be done in reverse order (ž first) 
    """
    results.sort(key=lambda x: _sort_helper(x,sort_by), reverse=reverse)


def search_products(products, search_query, user):
    """Returns the products whose names match the search query
    
    In addition, it also does some processing for nicer results display: it finds the
    name of the producer, and shortens group names so that they fit into the search result table
    
    Arguments:
        products {array of products} -- The products to search
        search_query {string} -- The text we are searching for
        user {user} -- The currently logged in user
    
    Returns:
        array of products -- The filtered and processed array of products, 
                             ready to be displayed in the search results table
    """
    if len(search_query) == 0:
        #Empty search strings (this includes the default search screen) shoudn't return any products
        return []
    filtered_food = []
    search_q = search_query.split()

    for product in products.values():
        #if is_product_displayable(product, user):
        if is_product_public_and_confirmed(product):
            if not product['COMMERCIAL_NAME']: product['COMMERCIAL_NAME'] = product['NAME']
            is_recipe = product.get("IS_RECIPE", True)
            for q in search_q:
                name = product['COMMERCIAL_NAME'].replace(',', '').split()
                barcode = product.get("BARCODE", "")
                if any([q.lower() in part.lower() for part in name]):
                    pass
                elif barcode and q.lower() in barcode:
                    pass
                else:
                    break
            else:
                #the else statement executes whenever the above for loop doesn't break:
                #so whenever we found a product that matches the search query
                producer = product.get('PRODUCER', None)
                if producer and producer.isdigit():
                    try:
                        product['PRODUCER'] = CompanyUserProfile.objects.get(registration_number=producer).company_name
                    except:
                        product['PRODUCER'] = None
                    if not product['PRODUCER']: product['PRODUCER'] = None
                
                product['GROUP_NAME'] = shorten_group_name(product['GROUP_NAME'])
                filtered_food.append(product)
    return filtered_food
    

def can_edit_product(user, product):
    """Checks if the given user has edit privileges for the given product
    
    A user can edit a specific product if:
        1. He is the product's author
        2. The user is a company, and the product has a barcode that begins with the companies prefix
    
    Arguments:
        user {User} -- The user to check permission for
        product {Product} -- The product to check 
    """

    if not user:
        #Users that aren't logged in can't edit anything
        return False
    
    if product['AUTHOR'] == str(user.id):
        return True

    product_barcode = product.get("BARCODE", None)
    barcode_prefix = None
    company_profile = user.company_profile
    if company_profile:
        barcode_prefix = company_profile.gs1_prefix

    if product_barcode and barcode_prefix and product_barcode.startswith(barcode_prefix):
        return True

    return False


class FoodIterator(object):
    def __init__(self, food, has_next, next_page):
        self.food = food
        self.has_next = has_next
        self.next_page = next_page
    
    def __iter__(self):
        return iter(self.food)

    def has_next(self):
        return self.has_next

    def next_page_number(self):
        return self.next_page

class FoodPaginator(object):
    """A custom paginator that gives product pages suited for the new dashboard layout
    
    Since the new dashboard layout is split into two columns (recipes + ingredients), we can't simply
    paginate a list of products anymore. This is because we would like each page to contain an even split of 
    labels/recipes, so that we get nice columns.

    Instead, this class performs pagination on recipes and labels seperately, then reutrns items_per_page number
    of both types of products so that we always get nicely balanced columns.
    """


    def __init__(self, items, items_per_page):
        self.products_labels = []
        self.products_recipes = []
        for product in items:
            if product.get("IS_RECIPE", True):
                self.products_recipes.append(product)
            else:
                self.products_labels.append(product)
        self.ITEMS_PER_PAGE = items_per_page
        recipe_pages = len(self.products_recipes) / items_per_page 
        label_pages = len(self.products_labels) / items_per_page 
        self.num_pages = max(recipe_pages,label_pages) + 1

    def page(self,page):
        try:
            page = int(page)
        except:
            raise PageNotAnInteger("Page isn't an integer")
        if (page > self.num_pages) or (page < 0):
            raise EmptyPage("Page is empty or doesn't exist")

        #Pages start from 1, not 0
        index_to_start_from = (page-1)*self.ITEMS_PER_PAGE
        last_index_recipes = index_to_start_from + self.ITEMS_PER_PAGE
        last_index_labels = index_to_start_from + self.ITEMS_PER_PAGE
        recipes = self.products_recipes[index_to_start_from:last_index_recipes]
        labels = self.products_labels[index_to_start_from:last_index_labels]
        products_to_return = recipes + labels


        has_recipes_next = len(self.products_recipes) > last_index_recipes
        has_labels_next = len (self.products_labels) >  last_index_labels 
        has_next = has_labels_next or has_recipes_next
        next_page = page + 1


        results = FoodIterator(products_to_return, has_next, next_page)
        return results