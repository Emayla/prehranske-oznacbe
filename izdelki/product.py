﻿import izdelki.setup as setup
from izdelki.utilities import find_product_by_id, cook_method_GtoM, read_product_components
from decimal import Decimal, ROUND_HALF_UP, getcontext
from collections import OrderedDict

#excel
import xlsxwriter
from io import BytesIO


def calculate_energy_value(component_weights, lower=False):
    """Calculates the energy value based on weights of components given by a dict."""
    enval = [0, 0]
    for c in setup.enval_calculation['necessary']:
        c_low = c.lower()
        conversion_factors = setup.enval_calculation.get(c, [0, 0])  # first value is for kJ, second for kcal
        enval[0] += component_weights.get(c if not lower else c_low, 0) * conversion_factors[0]  # in kJ
        enval[1] += component_weights.get(c if not lower else c_low, 0) * conversion_factors[1]  # in kcal
    return enval


def calculate_values(product):
    """Calculates all the necessary values (energy value, salt) and adds it to the product."""
    # energy value calc
    if len(product['COMPONENTS']) == 0:
        return product

    # salt calc
    salt = product['COMPONENTS'].get('NA', 0) * 2.5

    # saving to product
    product['COMPONENTS']['ENVAL'] = calculate_energy_value(product['COMPONENTS'])
    product['COMPONENTS']['SALT'] = salt

    return product

def make_https_photo(product):
    """If the products photo doesn't already start with https, rewrite the url so that it does

    
    Arguments:
        product {} -- Th eproduct whose photo we should rewrite
    """
    HTTPS_PREFIX = "https://" 
    photo_url = product.get("PHOTO", None)
    if not photo_url:
        return

    if photo_url.startswith(HTTPS_PREFIX):
        return
    else:
        #the non https url looks like http://opkp.si/...
        #first, cut off anyting before "opkp", then prefix it with https
        photo_url = photo_url[photo_url.find("opkp"):]
        photo_url = HTTPS_PREFIX + photo_url
        product["PHOTO"] = photo_url
        return


def calculate_percentages(product, components=False, lower=False):
    """Calculates the percentage of the recommended daily intake value for a specific product, for the component
    specified (if any), otherwise for all the necessary components."""
    percentages = {}

    if len(product['COMPONENTS']) == 0:
        return percentages

    components = setup.food_label['necessary'] if not components else components
    for c in components:
        if lower: c_low = c.lower()
        if c.upper() == 'ENVAL':
            recommended_intakes = setup.daily_intake_percent_calculation[c.upper()]  # first value is in kJ, second in kcal
            values = product['COMPONENTS'].get((c if not lower else c_low), [0,0]) # first value is in kJ, second in kcal
            percentages[(c if not lower else c_low)] = 100 * values[1] / recommended_intakes[1]  # calculation is in kcal
        else:
            #Some components the client can request don't have recommended daily values. for example:
            #enkrat nenasičene maščobne kisline  (FAMS)
            #večkrat nenasičene maščobne kisline (FAMU)
            #škrob (STARCH)
            #What should we do in this case? Use the parent value(FAT for FAMS, CHOT for STARCH and POLY)?
            #or do they have special values?
            if c.upper() in setup.daily_intake_percent_calculation:
                daily = setup.daily_intake_percent_calculation[c.upper()]
            else:
                if c.upper() in ['FAMS', 'FAPU']: 
                    daily = setup.daily_intake_percent_calculation['FAT']
                if c.upper() in ['POLY', 'STARCH']:
                    daily = setup.daily_intake_percent_calculation['CHOT']
            if type(daily) == list:
                daily = daily[0] * (10**daily[1])
            percentages[(c if not lower else c_low)] = 100 * product['COMPONENTS'].get((c if not lower else c_low),
                                                                                       0) / daily
    return percentages


def find_product_diets_and_ingredients(product):
    """For a product with given allergen ids and ingredient ids returns allergen and ingredient names."""
    if 'INGREDIENTS' in product:
        ingredient_names = []
        for ing in product['INGREDIENTS']:
            fing = find_product_by_id(ing['CODE'])
            if fing:
                ingredient_names.append({'NAME': fing['NAME'], 'CODE': ing['CODE'], 'WEIGHT': ing['WEIGHT']})
        product['INGREDIENTS'] = ingredient_names
    if len(product['DIETS']) > 0:
        allergen_names = []
        for diet in product['DIETS']:
            for all in setup.allergens:
                if "CODE" in diet:
                    if all['id'] == diet['CODE']:
                        allergen_names.append({'NAME': all['name'], 'CODE': diet['CODE']})
                else:
                    #Product['DIETS'] is just a list of codes
                    #(Used in printing pdfs and )
                    if all['id'] == diet:
                        allergen_names.append({'NAME': all['name'], 'CODE': diet})
        product['DIETS'] = allergen_names
    return product


def prepare_components(extra_components=[]):
    basic_components = []
    other_components = []
    previous_comp = ''
    for c in setup.food_label['layout']:
        if c != 'VIT':
            comp_dict = {'code': c}
            comp_dict = {**comp_dict, **setup.component_detail.get(c, {})}
            if c in setup.food_label['necessary'] or c in extra_components:
                basic_components.append(comp_dict)
            else:
                comp_dict['before'] = previous_comp
                other_components.append(comp_dict)
            previous_comp = c
        for c_finer in setup.food_label['layout'][c]:
            comp_dict = {'code': c_finer, 'child': c != 'VIT'}
            comp_dict = {**comp_dict, **setup.component_detail.get(c_finer, {})}
            if c_finer in setup.food_label['necessary'] or c_finer in extra_components:
                basic_components.append(comp_dict)
            else:
                comp_dict['before'] = previous_comp
                other_components.append(comp_dict)
            previous_comp = c_finer

    return basic_components, other_components


def get_retention_factor(comp, ing, cook_method, api):
    """Tries to first find a specific retention factor for the ingredient ing and component comp using cook_method,
    then tries to find a generic retention factor. If all else fails, returns 0.8 retention factor if a cooking
    method is used."""
    specific_factors = api.get_specific_retention_factor(comp, ing['GROUP_ID'], cook_method)
    for fact in specific_factors:
        if fact['PART'] == 'T' and not fact['WHOLE'] and not fact['SOAK'] and not fact['BREADED']:
            return fact['RETFACT']

    family = ''
    group = ing['GROUP_ID']
    cook_method_M = cook_method_GtoM(cook_method, api.get_cooking_methods())
    if 43 <= group <= 66:
        if not (cook_method_M & {'M016', 'M005', 'M015', 'M007'}):
            family = 'II'
        else: family = 'V'
    elif 8 <= group <= 18:
        if not (cook_method_M & {'M021', 'M002', 'M112', 'M212', 'M003', 'M013', 'M103', 'M203'}):
            family = 'III'
        else:
            family = 'VI'
    elif 30 <= group <= 41:
        family = 'IV'

    if family:
        generic_factor = api.get_generic_retention_factor(comp, family)
        if generic_factor:
            return generic_factor[0].get('RETFACT', None)

    factor = 1
    if 'M000' in cook_method_M:
        factor = 0.8
    return factor


def calculate_recipe_values(ingredients, components, cook_method, api):
    """Calculates all component values from list components based on the ingredients listed in ingredients. Also
    takes into account a 0.8 retention factor based on cook_method."""
    values = {}
    for component in components:
        if component == 'enval':
            values[component] = [0, 0]
        else:
            values[component] = 0

    total_weight = 0
    for ingredient in ingredients:
        weight = ingredient['weight'].replace(',', '.')
        if weight == '':
            continue
        weight = float(weight) if '.' in weight else int(weight)
        total_weight += weight
        product = find_product_by_id(ingredient['code'])
        product = calculate_values(product)
        for component in components:
            if component == 'enval':
                product_component_value = product['COMPONENTS'].get(component.upper(), [0,0])
                values[component][0] += product_component_value[0] * weight / 100
                values[component][1] += product_component_value[1] * weight / 100
            else:
                # factor = get_retention_factor(component, product, cook_method, api)
                values[component] += product['COMPONENTS'].get(component.upper(), 0) * weight / 100

    factor = 1
    if cook_method:
        if cook_method != 'G0003' and cook_method != 'G0001':
            factor = 0.8

    if total_weight == 0:
        return values

    # Component values are currently calculated per total_weight. We want to calculate the values per 100g (and take
    # a retention factor into account, if applicable).
    for comp in values:
        if comp == 'enval':
            # # we re-do energy value calculation because of the retention factors
            # values[comp] = calculate_energy_value(values, True)

            values[comp][0] = values[comp][0] / total_weight * 100 * factor
            values[comp][1] = values[comp][1] / total_weight * 100 * factor
        else:
            values[comp] = values[comp] / total_weight * 100 * factor

    return values

#Rounds all nutrient values according to rounding rules 
#Takes either a product or just a dictionary of component values
#The second parameter specifies which
def round_component_values(components, is_product=False, multiple_enval=True):
    """
    use the decimal class for exact rounding instead of float
    since correct rounding is legaly defined, we need exact control over it
    normal float round rounds 0.5 to nearest even number (1.5 -> 2, 2.5 -> 2, 3.5 - > 4)
    instead, we always round 0.5 up
    """
    getcontext().rounding=ROUND_HALF_UP
    if is_product:
        product = components
        #get salt and energy value if it hasn't been calculated yet
        product = calculate_values(product)
        components = product['COMPONENTS']

    if len(components) == 0:
        if is_product:
            return product
        else:
            return components
    for comp, value in components.items():
        if comp.lower() == 'enval':
            #some things only use a single enval value (just kcal)
            #For example, PDF export
            if multiple_enval:
                rounded0 = int(round(Decimal(value[0]),0))
                rounded1 = int(round(Decimal(value[1]),0))
                
                if is_product:
                    product['COMPONENTS'][comp] = [rounded0, rounded1]
                else:
                    components[comp] = [rounded0, rounded1]
            else:
                rounded = int(round(Decimal(value),0))
                
                if is_product:
                    product['COMPONENTS'][comp] = rounded
                else:
                    components[comp] = rounded

        elif comp.lower() in ['fat', 'chot', 'sugar', 'prot', 'fibt', 'starch']:
            if value <= 0.5:
                rounded = 0
            elif value < 10 :
                rounded = float(round(Decimal(value),1))
            else:
                rounded = int(round(Decimal(value),0))
            
            if is_product:
                product['COMPONENTS'][comp] = rounded
            else:
                components[comp] = rounded
        
        elif comp.lower() in ['fasat', 'fams', 'fapu']:
            if value <= 0.1:
                rounded = 0
            elif value < 10 :
                rounded = float(round(Decimal(value),1))
            else:
                rounded = int(round(Decimal(value),0))
            
            if is_product:
                product['COMPONENTS'][comp] = rounded
            else:
                components[comp] = rounded
        
        elif comp.lower() == 'salt':
            if value <= 0.0125:
                rounded = 0
            elif value < 1 :
                rounded = float(round(Decimal(value),2))
            else:
                rounded = float(round(Decimal(value),1))
            
            if is_product:
                product['COMPONENTS'][comp] = rounded
            else:
                components[comp] = rounded

        #the rest are just vitamins
        elif comp.lower() in ['vita','folacid', 'ca', 'chd', 'p', 'mg', 'id', 'k']:
            rounded = float(round(Decimal(value),3))
            if is_product:
                product['COMPONENTS'][comp] = rounded
            else:
                components[comp] = rounded

        else:
            rounded = float(round(Decimal(value),2))
            if is_product:
                product['COMPONENTS'][comp] = rounded
            else:
                components[comp] = rounded


    
    if is_product:
        return product
    else:
        return components



#Excel export helpers

#writes a single component to the specified row and column
def write_component_to_xls(worksheet, code, val, val_portion, unit, row, col):
    if code == "ENVAL":
        value = val[1]
    else:
        value = val

    worksheet.write(row, col, str(value) + " " + unit)
    worksheet.write(row, col + 1, str(val_portion) + " " + unit)

#Writes all component values to the excel file
def write_components_to_xls(worksheet, comp_values, product_percentages, portion, portions, components, other_components):
    row_c=0
    #ofset the component info a bit
    col_c=3
    worksheet.write(row_c, col_c, "HRANILNE VREDNOSTI")
    worksheet.write(row_c, col_c + 1, "Na 100g")
    worksheet.write(row_c, col_c + 2, "Na porcijo ({}g)".format(portion))
    worksheet.write(row_c, col_c + 3, "% PV na 2000 kcal")
    row_c+=1

    for comp in components:
        if comp['code'] in comp_values:
            worksheet.write(row_c, col_c, comp['name'])
            write_component_to_xls(worksheet, comp['code'],comp_values[comp['code']], 
                portions[comp['code']], comp['def_unit'], row_c, col_c + 1)
            worksheet.write(row_c, col_c + 3, str(round(product_percentages[comp['code']],1)) + "%")
            row_c+=1

    row_c+=3
    worksheet.write(row_c, col_c, "OSTALE HRANILNE VREDNOSTI")
    worksheet.write(row_c, col_c + 1, "Na 100g")
    worksheet.write(row_c, col_c + 2, "Na porcijo ({}g)".format(portion))
    worksheet.write(row_c, col_c + 3, "% PV na 2000 kcal")
    row_c+=1  
    
    for comp in other_components:
        if comp['code'] in comp_values:
            if comp_values[comp['code']] == 0:
                continue
            worksheet.write(row_c, col_c, comp['name'])
            write_component_to_xls(worksheet, comp['code'],comp_values[comp['code']], 
                portions[comp['code']], comp['def_unit'],row_c, col_c + 1)
            worksheet.write(row_c, col_c + 3, str(round(product_percentages[comp['code']],1)) + "%")
            row_c+=1


#gives a list of corect column widths for the excel worksheet
#format: [(colmun, width)]
def get_xls_column_widths():
    return [(0,20),
            (1,80),
            (2, 5),
            (3,35),
            (4,16),
            (5,16),
            (6,16)]

#sets the column widths of the worksheet columns to their proper values
def format_xls_column_widths(worksheet, widths):
    for col, width in widths:
        #the function takes (start_column, end_column, width)
        #since our widths are per column, the first two parameters are the same
        worksheet.set_column(col, col, width)



#takes a product and writes it to an excel file
def write_product_to_xls (product, extra_components = []):
    XLS_FIELD_TRANSLATIONS = {
        "COMMERCIAL_NAME": "Prodajno ime",
        "GROUP_NAME": "Skupina živila",
        "COUNTRY": "Država porekla",
        "NETOWEIGHT": "Neto količina (g)",
        "BARCODE": "Črtna koda",
        "NAME": "Naziv živila"
    }

    XLS_FIELD_ORDER = ["COMMERCIAL_NAME", "NAME", "COUNTRY", "NETOWEIGHT", "BARCODE"]

    
    product = OrderedDict(sorted(product.items(), key= lambda k: XLS_FIELD_ORDER.index(k[0]) if k[0] in XLS_FIELD_ORDER else 100))
    #get names instead of codes for allergens and ingredients
    #TODO: do we want names or codes
    find_product_diets_and_ingredients(product)

    #keep excel file in memory without writing temporary files
    output = BytesIO()
    workbook = xlsxwriter.Workbook(output, {'in_memory': True})
    worksheet = workbook.add_worksheet()
    format_xls_column_widths(worksheet, get_xls_column_widths())
    row = 0
    col = 0
    
    #first, write component info
    [components, other] = prepare_components(extra_components)
    component_names = [x.get("code", "") for x in components]
    component_names_extra = [x.get("code", "") for x in other]
    component_names = component_names + component_names_extra
    
    product = calculate_values(product)
    product_percentages = calculate_percentages(product, component_names)
    product = convert_to_default_units(product, components)
    product = round_component_values(product, is_product=True)

    portions = calculate_portion(product)
    portions = round_component_values(portions, multiple_enval=False)
    portion_size = round(product['SERVING']['MEALSIZE'], 1)
    write_components_to_xls(worksheet, product["COMPONENTS"], product_percentages,
                            portion_size, portions, components, other) 

    #Then write all other fields
    #loop through the product, which is now an ordered dict
    #this ensures we will always get fields in their correct positions
    for k,v in product.items():      
        if "components" in k.lower() :
            #we already processed components above, so skip this part
            continue
        else:
            if k not in XLS_FIELD_TRANSLATIONS or v is None :
                continue
            title = XLS_FIELD_TRANSLATIONS[k]
            worksheet.write(row, col, title)
            worksheet.write(row, col + 1, str(v))
            row+=1

    workbook.close()
    return output


def calculate_portion(product):
    serving=product['SERVING']['MEALSIZE']
    serving_mult = serving / 100.0
    serving_components={}
    for k,v in product['COMPONENTS'].items():
        if k == 'ENVAL': v=v[1]
        serving_components[k]=v*serving_mult

    return serving_components



def get_extra_components(product):
    """Find additional components that need to be displayed for this product
    
    In addition to basic components, users can also add additional components (such as vitamins) to a product.
    This function will find what the extra components added by theuser are, and return them.

    There are two different kids of products: Recipes and Labels. 
    In Labels, the user enters components manualy. This means that the API will only have information
    on the components entered by the user, so we simply display all components returned by the server.

    Recipes are harder. Here, the user enters ingredients, and the database server calculates the components.
    When we get the product from the API, it will return all calculated components, not just the ones the user wanted.
    To handle this, we save all extra ingredients to a local file/db, and reference it when the user wants to view a product.
    or now, a JSON file should suffice, since it's fast enough even when we browse our entire DB.
    
    Arguments:
        product {Dictionary} -- A Dictionary containing the data of the product we want to check
    """

    extra_components = []
    
    # Early products don't have the IS_RECIPE flag (it was added in later), so we can't tell what they are
    # We will assume they are recipes, otherwise we would always display most possible components
    # (since the ones that aren't labels always contain all nonzero components)
    from_label = not product.get("IS_RECIPE", True)
    if from_label and "COMPONENTS" in product:
        extra_components = list(product["COMPONENTS"].keys())
    else:
        extra_components = find_extra_recipe_components(product.get("CODE", ""))

    return extra_components


def find_extra_recipe_components(product_id):
    """Finds a product's extra components from a local file
    
    Used by :func:`~izdelki.get_extra_components`
    
    Arguments:
        product_id {string} -- ID of the product we are searching for
    """  

    all_extra_components = read_product_components()
    extra_components = all_extra_components.get(product_id,[])
    return extra_components


def convert_to_default_units(product, components):
    """Converts component values from grams to their default units (g, mg or mcg)
    
    When we receive product info from the API, the component values of this product are always expressed in grams
    When we display them, we display them in different units: every component has a defined "default unit"
    that is used for display

    This function takes in a product and converts all components from grams to their defualt units

    Arguments:
        product {Dictionary} -- The product we are converting. Needs to have a key "COMPONENTS"
        components {List} -- A list of components. Every component is a dictionary with at least two keys:
                            "code" and "def_unit"
    """ 
    if "COMPONENTS" not in product:
        return product
    if len(product["COMPONENTS"])==0:
        return product

    MG_MULTIPLIER = 1000
    MCG_MULTIPLIER = 1000000    

    component_values = product["COMPONENTS"]
    for component in components:
        current_code = component.get("code", "")
        current_value = component_values.get(current_code, 0)
        default_unit = component.get("def_unit", "g")

        if default_unit == "mg":
            new_value = current_value * MG_MULTIPLIER
        elif default_unit == "mcg":
            new_value = current_value * MCG_MULTIPLIER
        else:
            new_value=current_value
        product["COMPONENTS"][current_code] = new_value

    return product  
