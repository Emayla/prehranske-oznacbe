﻿import json
from izdelki.setup import food_path, components_path
from izdelki.models import Product
from izdelki.tokens import account_activation_token
# from setup import food_path

from django.core.urlresolvers import reverse
from django.http import QueryDict
from django.template import defaultfilters as filters

from django.core.mail import send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.templatetags.static import static

#from PIL import Image
#from PIL import ImageOps
from io import BytesIO

import requests, re

#One of:
#"FILE" - store products in a json file. Has problems with simultaneous access
#"DB"   - store products in a DB. Most complicated, slow on creating, fast on write
#"API"  - always get food from the API. Simplest, slowest.
FOOD_STORE_BACKEND = "DB"

def read_products(whole=False):
    """Read food products from JSON file. If whole is set to True, also returns the count (returns the whole file)."""
    
    if FOOD_STORE_BACKEND == "FILE":
        #File based storage: Problems reading and writing at the same time
        with open(food_path) as food_file:
            food = json.load(food_file)
        
            # filter out all deleted items
            # make a list() to allow deletion from dictionary while iterating through it
            for code, product in list(food['items'].items()):
                if 'deleted' in product:
                    food['items'].pop(code)
            if not whole: food = food['items']
            return food



    if FOOD_STORE_BACKEND == "DB":
        #DB based storage
        products = Product.objects.all()
        product_list = {}
        num=0
        for product in products:
            num+=1
            product_json=json.loads(product.json)
            if not 'deleted' in product_json:
                product_list[product.code] = product_json

        if not whole:
            return product_list
        else:
            return {'count':num, 'items':product_list}



    if FOOD_STORE_BACKEND == "API":
        #Just access the API every time
        #Simplest, even slower
        from izdelki.api import API
        from django.conf import settings
        api = API()
        api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
        food = api.download_food_local()
        api.logout()
        for code, product in list(food['items'].items()):
            if 'deleted' in product:
                food['items'].pop(code)
        if not whole:
            return food['items']
        else:
            return food



def json_to_database():
    """Convert the food json file to database
    """
    #first, clear all old products
    Product.objects.all().delete()
    #first, read the json file
    with open(food_path) as food_file:
        food = json.load(food_file)
    
        # filter out all deleted items
        # make a list() to allow deletion from dictionary while iterating through it
        products = []
        for code, product in list(food['items'].items()):
            if not 'deleted' in product:
                products.append(Product(code=code, json=json.dumps(product, ensure_ascii=False)))
        Product.objects.bulk_create(products)

        


def read_product_components():
    """Reads the component file to find extra components added to a product
    
    Returns:
        {Dictionary} -- A dictionary of the form {'product_id': [extra components]}
    """
    try:
        with open(components_path) as components_file:
            components = json.load(components_file)
            return components
    except IOError:
        return {}


def write_products(products):
    """Write given food products to JSON file."""

    if FOOD_STORE_BACKEND == "FILE":
        with open(food_path, 'w') as food_file:
            json.dump(products, food_file)

    if FOOD_STORE_BACKEND == "DB":
        with open(food_path, 'w') as food_file:
            json.dump(products, food_file)
        json_to_database()

    if FOOD_STORE_BACKEND == "API":
        #Don't need to save
        pass

def write_product_components(products):
    """Writes to the extra component file
    
    Arguments:
        products {Dictionary} -- Products and components to write, in the form [{'product_id': [extra components]}]
    """
    try:
        with open(components_path, 'w') as components_file:
            json.dump(products, components_file)
    except IOError:
        return 


def build_dict_products(products):
    """Builds a dictionary from given product with key=CODE and value=product."""
    items_dict = {}
    for product in products['items']:
        items_dict[product['CODE']] = product
    products['items'] = items_dict
    return products


def count_products():
    """Returns the number of products currently in database."""
    if FOOD_STORE_BACKEND == "FILE":
        with open(food_path) as food_file:
            food = json.load(food_file)
        return food['count']

    if FOOD_STORE_BACKEND == "DB":
        return Product.objects.all().count()

    if FOOD_STORE_BACKEND == "API":
        from izdelki.api import API
        from django.conf import settings
        api = API()
        api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
        food = api.download_food_local()
        api.logout()
        return len(list(food['items'].items()))

def count_producers():
    """Returns the number of unique producers in the database"""

    if FOOD_STORE_BACKEND == "FILE":
        print("FILE")
        with open(food_path) as food_file:
            food = json.load(food_file)

        producers = set()
        for code, item in food['items'].items():
            producers.add(item.get("PRODUCER", None))
        return len(producers)

    if FOOD_STORE_BACKEND == "DB":
        print("DB")
        producers = set()
        food =  Product.objects.all()
        for item in food:
            product_json=json.loads(item.json)
            producers.add(product_json.get("PRODUCER", None))
        return len(producers)


    if FOOD_STORE_BACKEND == "API":
        print("API")
        from izdelki.api import API
        from django.conf import settings
        api = API()
        api.login(username=settings.API_USERNAME, password=settings.API_PASSWORD)
        food = api.download_food_local()
        producers = set()
        for code, item in food['items'].items():
            producers.add(item.get("PRODUCER", None))
        return len(producers)



def find_product_by_id(product_id):
    """Returns the product given by id."""
    products = read_products()
    queried_product = products.get(product_id, None)
    return queried_product

def find_extra_components_by_id(product_id):
    """Returns the product given by id."""
    products = read_product_components()
    components = products.get(product_id, [])
    return components

#TOO: Fix: takes too long, breaks the read_products function
def add_product_to_file(product, count_add=False):
    """Adds a newly downloaded product to the existing products saved in the JSON file."""

    if FOOD_STORE_BACKEND == "FILE":
        products = read_products(True)
        # products['items'].append(product)
        products['items'].update({product['CODE']: product})
        if count_add:
            products['count'] += 1
        write_products(products)

    if FOOD_STORE_BACKEND == "DB":
        print ("UPDATING")
        print (product)
        # The product is of the form {CODE: DATA}, so the code is also the only key of the dict
        code = product['CODE']
        new_product, created = Product.objects.update_or_create(code=code, defaults = {'json':json.dumps(product, ensure_ascii=False)})
        print(new_product)

    if FOOD_STORE_BACKEND == "API":
        #Don't need to save
        pass


def add_components_to_file(product_id, components):
    """Adds product components to the JSON file.
    
    When the users add a product, they can choose to display extra components (such as vitamins, ...)
    The API doesn't keep track of which components were added this way, so we store this information locally
    
    This let's us display these extra components when the user views a saved file
    """
    products = read_product_components()
    products.update({product_id: components})
    write_product_components(products)


def cook_method_GtoM(wanted_method, all_cook_methods):
    """Returns the Mxxx cook methods for given Gxxx method."""
    for method in all_cook_methods:
        if method['CODE'] == wanted_method:
            return set([m['CODE'] for m in method['PREP_METHODS']])


def food_group_for_select(food_groups):
    """Returns 2-tuples for food groups, 1st element if food group CODE, 2nd is food group NAME. Used in Select
    form widget."""
    new_food_group = [(group['CODE'], group['NAME']) for group in food_groups]
    return new_food_group


def product_for_edit(product):
    """Returns product dict with lowercase keys, which can populate the RecipeForm."""
    product = {k.lower(): v for k, v in product.items()}
    product['group'] = product.get('group_id',0)
    product['neto_weight'] = product.get('netoweight',0)
    if 'netoweight' in product:
        del product['netoweight']
    return product


def mark_product_as_deleted(product_id):
    """Sets a flag in the food.json file that indicates the specific product has been
    deleted and shouldn't be shown to users
    """
    try:
        products = read_products(whole = True)
        products['items'][product_id]['deleted'] = True
        write_products(products)
        return True
    except:
        return False


def get_all_barcodes():
    products = read_products()
    barcodes = []
    for product in products.values():
        if (product['BARCODE'] and product['COUNTRY']):
            barcodes.append((product['BARCODE'], product['COUNTRY'].lower()))
    return barcodes

# URL helpers

def build_url(*args, **kwargs):
    params = kwargs.pop('params', {})
    url = reverse(*args, **kwargs)
    if not params: return url

    query_dict = QueryDict('', mutable=True)
    for k, v in params.items():
        if type(v) is list: query_dict.setlist(k, v)
        else: query_dict[k] = v

    return url + '?' + query_dict.urlencode()



def photo_valid(photo_bin, MAX_SIZE=10000000):
    """ 
   try:
        #first, check file size so we don't open large files
        if len(photo_bin) > MAX_SIZE:
            return False

        #now we can try opening the image
        image = Image.open(BytesIO(photo_bin))
        #verify does basic checks on the header
        image.verify()
        #The image needs to be reopened after verify() for it to work properly
        #(this is the libraries limitation)
        image = Image.open(BytesIO(photo_bin))
        #load attempts to actually load the image into memory and will catch more errors
        #this can be slow, so it can be removed if we don't need complete image validation
        image.load()
        

        #if both of these don't throw exceptions, we have a valid photo
        return True
    except:
        return False
    """
    return True

#Cuts off the image to the specified size/stretcheds it if it's too small
#Currently not used, since the picture is instead resized on the client,
#It's left here in case it will be needed at some point
def resize_photo(photo_bin, size=(260,260)):
#    image = Image.open(BytesIO(photo_bin))
#    format = image.format
#    image_resized = ImageOps.fit(image,size)
#    image_bytes = BytesIO()
#    image_resized.save(image_bytes,format)
#    return image_bytes.getvalue()
    return photo_bin


def mark_label_products(products):
    """
    Adds a label (Oznacba) to all products that were added from a label
    This is used on the sort page to differentiate receipes and raw ingredients

    TODO: Take this into account if we ever shorten long names in the search results
    """

    LABEL = "(označba)"
    for product in products:
        if product['CODE'].startswith("IND"):
            product['NAME'] = product['NAME'] + " " + LABEL
            product['COMMERCIAL_NAME'] = product['COMMERCIAL_NAME'] + " " + LABEL

    return products


def filter_select2_component (component):
    """
    Takes a component string and returns the equivalent of doing
    {{component|floatformat:"0"|rjust:"3"}}
    inside a django template
    """
    return filters.rjust(filters.floatformat(component,0),3)

def create_select2_product_json(products):
    """Creates a json file containing basic information on all ingredients
    
    This JSON is sent to the client to be used by the ingredient searchbox used when adding a new product

    Arguments:
        products {list} -- A list of all products to convert to JSON
    """
    json = []
    for product in products:

        type_image = ""

        name = product["COMMERCIAL_NAME"]
        favscore = product.get("FAVSCORE", 0)
        if not name:
            name = product["NAME"]

        is_name_allcaps = name == name.upper()
        
        #if product.get("IS_RECIPE", False):
        if is_name_allcaps:
            type_image = static('izdelki/resources/img/search-result-label.png')
        else:
            type_image = static('izdelki/resources/img/search-result-recipe.png')

        #Shorten name to prevent overflow
        #This only applies to the list of added ingredients
        MAX_ALLOWED_NAME_LENGTH = 50
        """
        if len(name) > MAX_ALLOWED_NAME_LENGTH:
            name = name[0:MAX_ALLOWED_NAME_LENGTH]
            name += "..."
        """
        #Some products don't have component information 
        #and just return an empty list instead of an empty dict
        #Since the list doesn't have a get method, we can't use that to set a default
        #and need to hardcode it
        if len(product["COMPONENTS"]) > 0:
            p_chot = filter_select2_component(product["COMPONENTS"].get("CHOT",0))
            p_prot = filter_select2_component(product["COMPONENTS"].get("PROT",0))
            p_fat  = filter_select2_component(product["COMPONENTS"].get("FAT",0))
        else:
            p_chot = filter_select2_component(0)
            p_prot = filter_select2_component(0)
            p_fat  = filter_select2_component(0)

        components_html = """ <span class="searchbox-components align-right-ingredient-table" >
                              <span class="searchbox-component-OH" data-toggle="tooltip" data-placement="top" title="Ogljikovi hidrati">
                                OH</span>:<span class="preserve-whitespace">{}g  </span>
                              <span class="searchbox-component-B" data-toggle="tooltip" data-placement="top" title="Beljakovine">
                                B</span>:<span class="preserve-whitespace">{}g  </span>
                              <span class="searchbox-component-M" data-toggle="tooltip" data-placement="top" title="Maščobe">
                                M</span>:<span class="preserve-whitespace">{}g </span>
                             </span> """.format(p_chot,p_prot,p_fat)

        tooltip = """<div class ='tooltip-container-left'>Ogljikovi hidrati:</div> <div class=tooltip-container-right>{}g</div> <br/>
                     <div class ='tooltip-container-left'>Beljakovine:</div>       <div class=tooltip-container-right>{}g</div> <br/>
                     <div class ='tooltip-container-left'>Maščobe:</div>           <div class=tooltip-container-right>{}g</div> <br/>
        
        """.format(p_chot,p_prot,p_fat )   

        json.append({"id":product["CODE"], "text": name, "components": components_html, "barcode": product.get("BARCODE",""), "tooltip":tooltip, "typeImage": type_image, "favscore": favscore})
    return {"results": json}

def generate_tooltips (ingredients):
    """Takes in a list of ingredients and adds tooltips to them
    
    These tooltips are used on the add screen to display nutrient values to users

    Arguments:
        ingredients {list} -- A list of ingredients. Every ingredient is a dictionary containing at least the key CODE
    """
    products = read_products()
    for ingredient in ingredients:
        code = ingredient["CODE"]
        product = products[code]

        if len(product["COMPONENTS"]) > 0:
            p_chot = filter_select2_component(product["COMPONENTS"].get("CHOT",0))
            p_prot = filter_select2_component(product["COMPONENTS"].get("PROT",0))
            p_fat  = filter_select2_component(product["COMPONENTS"].get("FAT",0))
        else:
            p_chot = filter_select2_component(0)
            p_prot = filter_select2_component(0)
            p_fat  = filter_select2_component(0)



        #tooltip = "OH: {}g   B: {}g   M: {}g".format(p_chot,p_prot,p_fat)
        tooltip = """<div class ='tooltip-container-left'>Ogljikovi hidrati:</div> <div class=tooltip-container-right>{}g</div> <br/>
                     <div class ='tooltip-container-left'>Beljakovine:</div>       <div class=tooltip-container-right>{}g</div> <br/>
                     <div class ='tooltip-container-left'>Maščobe:</div>           <div class=tooltip-container-right>{}g</div> <br/>
        
        """.format(p_chot,p_prot,p_fat )   
        ingredient["TOOLTIP"] = tooltip

    return ingredients


def AJPES_check_company_id(company_id):
    """Uses the AJPES web API to check if a given ID is correct. 

    If it is, returns the name and address of the company. Else, it returns an error
    
    Arguments:
        company_id {string} -- A 7 digit ID of the company
    """ 

    print("CHECKING PARAMETERS")
    url="https://www.ajpes.si/wsPrsInfo/PrsInfo.asmx"
    #headers = {'content-type': 'application/soap+xml'}
    headers = {'content-type': 'text/xml'}
    body ="""<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                    <soap:Body>
                        <PrsDataFind xmlns="http://www.ajpes.si/wsPrs/PrsInfo">
                            <sNaziv/>
                            <sMaticna>""" + str(company_id) + """</sMaticna>
                            <sDavcna/>
                            <sNaslov/>
                            <sHisnaStevilka/>
                            <sNaselje/>
                            <sObcina/>
                            <sPosta/>
                            <sDejavnost/>
                            <sSektor/>
                            <sOblika/>
                            <iTip>1</iTip>
                            <iMaxRec>20</iMaxRec>
                            <Ident>
                                <string>prsinfo_ijs_prehoz</string>
                                <string>Xhj7_mBNb</string>
                                <string>PRS_SN_E</string>
                            </Ident>
                        </PrsDataFind>
                    </soap:Body>
                </soap:Envelope>
    """

    response = requests.post(url,data=body,headers=headers)

    result = str(response.content,"utf-8")
    print(result)

    #Ali je zastopnik (str)
    preverizastopnika = (re.findall(r'<PrsDataFindResult>(.*?)</PrsDataFindResult>', result))[0]


    if preverizastopnika != "true":
        #podjetje s to matično številko ne obstaja
        return ({"success": False})


    #Dobimo še:
    #Kratko_ime
    #Ulica
    #Posta
    #Podenota
    #
    #Primer:
    #<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/
    # xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    # <soap:Body><PrsDataFindResponse xmlns="http://www.ajpes.si/wsPrs/PrsInfo">
    # <PrsDataFindResult>true</PrsDataFindResult><sRetVal>
    # &lt;PrsFind xmlns="http://www.ajpes.si/xml_sheme/prs-info/PrsDataFind"
    #  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;&lt;
    #  PrsData&gt;
    #  &lt;Popolno_ime&gt;ORGANIZACIJA KULTURNIH PRIREDITEV, ANDREJ HOFER s.p.&lt;/Popolno_ime&gt;
    #  &lt;Kratko_ime&gt;ANDREJ HOFER s.p.&lt;/Kratko_ime&gt;
    #  &lt;Maticna&gt;3559173000&lt;/Maticna&gt;
    #  &lt;Ulica&gt;Šlandrov trg 020&lt;/Ulica&gt;
    #  &lt;Posta&gt;3310 Žalec&lt;/Posta&gt;
    #  &lt;Podenota&gt;000&lt;/Podenota&gt;
    #  &lt;/PrsData&gt;&lt;/PrsFind&gt;</sRetVal></PrsDataFindResponse></soap:Body></soap:Envelope>
    full_name = (re.findall(r'Popolno_ime&gt;(.*?)/Popolno_ime&gt;', result))[0][:-4]
    address = (re.findall(r'Ulica&gt;(.*?)/Ulica&gt;', result))[0][:-4]
    
    return {"success": True, "name": full_name, "address": address}


def send_register_confirm_email(user, to_email, request):
    mail_subject = 'Pozdravljeni v spletni aplikaciji Prehranske Označbe'
    current_site = get_current_site(request)
    email_body = render_to_string('izdelki/register_confirm_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid':urlsafe_base64_encode(force_bytes(user.id)),
                    'token':account_activation_token.make_token(user),
                })

    try:    
        send_mail(
            mail_subject,
            email_body,
            'Prehranske Označbe <info@prehranskeoznacbe.si>',
            [to_email],
            fail_silently=False,
        )
    except smtplib.SMTPException:
        logger.info("Couldn't send email")





def get_unit_change_components():
    """Returns a list of all component codes that should have a unit select button in the component table
    
    These components are all extra components that don't have the default unit of 'g', as well as SALT and ENVAL
    """ 
    from izdelki.product import prepare_components
    [base_components, extra_components] = prepare_components()

    unit_change_components = ["SALT", "ENVAL"]
    for component in extra_components:
        if component["def_unit"] != "g":
            unit_change_components.append(component["code"])

    return unit_change_components 