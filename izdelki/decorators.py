from django.http import HttpResponse


def anonymous_required(f):
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponse('Ste že prijavljeni!')
        else:
            return f(request, *args, **kwargs)
    return wrap
