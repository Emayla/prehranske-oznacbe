from prehranske_oznacbe.settings.base import *


DEBUG = False
ALLOWED_HOSTS = ['172.20.8.41', 'izidor.ijs.si','prehranskeoznacbe.si', 'www.prehranskeoznacbe.si', '194.249.231.190']
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = 'DENY'
