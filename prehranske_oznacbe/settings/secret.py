import os


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('OZNACBE_SECRET_KEY')

API_USERNAME = os.getenv('OZNACBE_API_USERNAME')
API_PASSWORD = os.getenv('OZNACBE_API_PASSWORD')

EMAIL_USERNAME = os.getenv('OZNACBE_EMAIL_USERNAME')
EMAIL_PASSWORD = os.getenv('OZNACBE_EMAIL_PASSWORD')
